"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import unittest

import openapi_client
from openapi_client.api.aligntimeseries_api import AligntimeseriesApi  # noqa: E501


class TestAligntimeseriesApi(unittest.TestCase):
    """AligntimeseriesApi unit test stubs"""

    def setUp(self):
        self.api = AligntimeseriesApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_find_align_corrections(self):
        """Test case for find_align_corrections

        Retrieves alignment corrections, covering a range in time  # noqa: E501
        """
        pass

    def test_find_align_corrections_ids(self):
        """Test case for find_align_corrections_ids

        Retrieves the list of possible element IDs, for a given alignment tag  # noqa: E501
        """
        pass

    def test_find_align_sagittas(self):
        """Test case for find_align_sagittas

        Retrieves alignment sagittas, covering a range in time  # noqa: E501
        """
        pass

    def test_find_align_sagittas_ids(self):
        """Test case for find_align_sagittas_ids

        Retrieves the list of possible chamber IDs, for a given alignment tag  # noqa: E501
        """
        pass

    def test_find_align_sensor_pulls(self):
        """Test case for find_align_sensor_pulls

        Retrieves a list of sensor pulls, covering a range in time  # noqa: E501
        """
        pass

    def test_find_align_sensor_pulls_ids(self):
        """Test case for find_align_sensor_pulls_ids

        Retrieves the list of possible sensor pull IDs, for a given alignment tag  # noqa: E501
        """
        pass

    def test_find_align_sensor_residuals(self):
        """Test case for find_align_sensor_residuals

        Retrieves a list of sensor residuals, covering a range in time  # noqa: E501
        """
        pass

    def test_find_align_sensor_residuals_ids(self):
        """Test case for find_align_sensor_residuals_ids

        Retrieves the list of possible sensor residual IDs, for a given alignment tag  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
