"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.aligniov_base_dto import AligniovBaseDto
globals()['AligniovBaseDto'] = AligniovBaseDto
from openapi_client.model.align_sagittas_dto import AlignSagittasDto


class TestAlignSagittasDto(unittest.TestCase):
    """AlignSagittasDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlignSagittasDto(self):
        """Test AlignSagittasDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlignSagittasDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
