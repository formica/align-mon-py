"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.al_d_dto import AlDDto
from openapi_client.model.al_d_set_dto_all_of import AlDSetDtoAllOf
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
globals()['AlDDto'] = AlDDto
globals()['AlDSetDtoAllOf'] = AlDSetDtoAllOf
globals()['AlignMonBaseResponse'] = AlignMonBaseResponse
from openapi_client.model.al_d_set_dto import AlDSetDto


class TestAlDSetDto(unittest.TestCase):
    """AlDSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlDSetDto(self):
        """Test AlDSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlDSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
