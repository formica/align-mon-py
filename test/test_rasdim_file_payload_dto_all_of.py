"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.oltestlines_dto import OltestlinesDto
globals()['OltestlinesDto'] = OltestlinesDto
from openapi_client.model.rasdim_file_payload_dto_all_of import RasdimFilePayloadDtoAllOf


class TestRasdimFilePayloadDtoAllOf(unittest.TestCase):
    """RasdimFilePayloadDtoAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testRasdimFilePayloadDtoAllOf(self):
        """Test RasdimFilePayloadDtoAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = RasdimFilePayloadDtoAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
