"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.cool_config_dto import CoolConfigDto
globals()['CoolConfigDto'] = CoolConfigDto
from openapi_client.model.align_cool_tag_dto import AlignCoolTagDto


class TestAlignCoolTagDto(unittest.TestCase):
    """AlignCoolTagDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlignCoolTagDto(self):
        """Test AlignCoolTagDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlignCoolTagDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
