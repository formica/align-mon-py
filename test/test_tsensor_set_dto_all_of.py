"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.tsensor_dto import TsensorDto
globals()['TsensorDto'] = TsensorDto
from openapi_client.model.tsensor_set_dto_all_of import TsensorSetDtoAllOf


class TestTsensorSetDtoAllOf(unittest.TestCase):
    """TsensorSetDtoAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTsensorSetDtoAllOf(self):
        """Test TsensorSetDtoAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = TsensorSetDtoAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
