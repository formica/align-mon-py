"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.align_fit_bad_sensors_dto import AlignFitBadSensorsDto
globals()['AlignFitBadSensorsDto'] = AlignFitBadSensorsDto
from openapi_client.model.align_fit_bad_sensors_set_dto_all_of import AlignFitBadSensorsSetDtoAllOf


class TestAlignFitBadSensorsSetDtoAllOf(unittest.TestCase):
    """AlignFitBadSensorsSetDtoAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlignFitBadSensorsSetDtoAllOf(self):
        """Test AlignFitBadSensorsSetDtoAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlignFitBadSensorsSetDtoAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
