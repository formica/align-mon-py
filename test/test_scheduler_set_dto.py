"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
from openapi_client.model.scheduler_dto import SchedulerDto
from openapi_client.model.scheduler_set_dto_all_of import SchedulerSetDtoAllOf
globals()['AlignMonBaseResponse'] = AlignMonBaseResponse
globals()['SchedulerDto'] = SchedulerDto
globals()['SchedulerSetDtoAllOf'] = SchedulerSetDtoAllOf
from openapi_client.model.scheduler_set_dto import SchedulerSetDto


class TestSchedulerSetDto(unittest.TestCase):
    """SchedulerSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testSchedulerSetDto(self):
        """Test SchedulerSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = SchedulerSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
