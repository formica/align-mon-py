"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.interval_maker_tag_dto import IntervalMakerTagDto
globals()['IntervalMakerTagDto'] = IntervalMakerTagDto
from openapi_client.model.interval_maker_tag_set_dto_all_of import IntervalMakerTagSetDtoAllOf


class TestIntervalMakerTagSetDtoAllOf(unittest.TestCase):
    """IntervalMakerTagSetDtoAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testIntervalMakerTagSetDtoAllOf(self):
        """Test IntervalMakerTagSetDtoAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = IntervalMakerTagSetDtoAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
