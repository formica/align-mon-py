"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.align_reco_element_conf_dto import AlignRecoElementConfDto
globals()['AlignRecoElementConfDto'] = AlignRecoElementConfDto
from openapi_client.model.align_reco_element_conf_set_dto_all_of import AlignRecoElementConfSetDtoAllOf


class TestAlignRecoElementConfSetDtoAllOf(unittest.TestCase):
    """AlignRecoElementConfSetDtoAllOf unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlignRecoElementConfSetDtoAllOf(self):
        """Test AlignRecoElementConfSetDtoAllOf"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlignRecoElementConfSetDtoAllOf()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
