"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.dynamic_interval_maker_conf_dto_all_of import DynamicIntervalMakerConfDtoAllOf
from openapi_client.model.interval_maker_conf_dto import IntervalMakerConfDto
globals()['DynamicIntervalMakerConfDtoAllOf'] = DynamicIntervalMakerConfDtoAllOf
globals()['IntervalMakerConfDto'] = IntervalMakerConfDto
from openapi_client.model.dynamic_stable_iovs_interval_maker_conf_dto import DynamicStableIovsIntervalMakerConfDto


class TestDynamicStableIovsIntervalMakerConfDto(unittest.TestCase):
    """DynamicStableIovsIntervalMakerConfDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testDynamicStableIovsIntervalMakerConfDto(self):
        """Test DynamicStableIovsIntervalMakerConfDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = DynamicStableIovsIntervalMakerConfDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
