"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.ol_incident_dto import OlIncidentDto
from openapi_client.model.oltestlines_dto import OltestlinesDto
from openapi_client.model.opticallines_base_dto import OpticallinesBaseDto
from openapi_client.model.opticallines_dto_all_of import OpticallinesDtoAllOf
globals()['OlIncidentDto'] = OlIncidentDto
globals()['OltestlinesDto'] = OltestlinesDto
globals()['OpticallinesBaseDto'] = OpticallinesBaseDto
globals()['OpticallinesDtoAllOf'] = OpticallinesDtoAllOf
from openapi_client.model.opticallines_dto import OpticallinesDto


class TestOpticallinesDto(unittest.TestCase):
    """OpticallinesDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOpticallinesDto(self):
        """Test OpticallinesDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = OpticallinesDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
