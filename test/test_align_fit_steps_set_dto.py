"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import sys
import unittest

import openapi_client
from openapi_client.model.align_fit_steps_dto import AlignFitStepsDto
from openapi_client.model.align_fit_steps_set_dto_all_of import AlignFitStepsSetDtoAllOf
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
globals()['AlignFitStepsDto'] = AlignFitStepsDto
globals()['AlignFitStepsSetDtoAllOf'] = AlignFitStepsSetDtoAllOf
globals()['AlignMonBaseResponse'] = AlignMonBaseResponse
from openapi_client.model.align_fit_steps_set_dto import AlignFitStepsSetDto


class TestAlignFitStepsSetDto(unittest.TestCase):
    """AlignFitStepsSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testAlignFitStepsSetDto(self):
        """Test AlignFitStepsSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = AlignFitStepsSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
