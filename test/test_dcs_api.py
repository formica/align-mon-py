"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import unittest

import openapi_client
from openapi_client.api.dcs_api import DcsApi  # noqa: E501


class TestDcsApi(unittest.TestCase):
    """DcsApi unit test stubs"""

    def setUp(self):
        self.api = DcsApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_get_average_current_for_align_iov(self):
        """Test case for get_average_current_for_align_iov

        Retrieves the average current for an IOV  # noqa: E501
        """
        pass

    def test_get_bfieldcurrent_history(self):
        """Test case for get_bfieldcurrent_history

        Retrieves magnet current history  # noqa: E501
        """
        pass

    def test_get_tsdata_history(self):
        """Test case for get_tsdata_history

        Retrieves T sensor measurement history  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
