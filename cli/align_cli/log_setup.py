import logging

def setup_logger(level):

    log = logging.getLogger("aligncli")
    log.setLevel(level)

    handler = logging.StreamHandler()
    logfmt = "%(levelname)s:%(name)s: %(message)s"
    handler.setFormatter(logging.Formatter(logfmt))
    log.addHandler(handler)

    return log
