"""
 Keycloak Token tool.

 Request Keycloak server for user/app tokens, to set "Authorization" headers.

 @author: maxime.bocquier@cea.fr, andrea.formica@cern.ch

-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
1) 'client credential flow': a pipeline has access through docker secret to
dedicated keycloak 'confidential' :
- client_id
- client_secret

FSC: To get an access token we must POST to keycloak with a payload like:
    payload = {
        "client_id": "ZeConfidentialClientId",
        "client_secret": "ZeConfidentialClientSecret",
        "grant_type": "client_credentials",
        "scope": "https://fsc.svom.api"
    }

CERN: 'exchange token' : a client who cannot keep a secret request an exchange token to then ask access
  to the API.
    payload = {"client_id": client_id,
        "grant_type": "password",
        "scope": "openid",
        "username" : username,
        "password": password
    }

CERN: 'api access' , the provided token will contain both the access and refresh token
    payload = {
        "client_id": client_id,
        "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
        "subject_token": token,
        "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
        "audience": SERVICE_KC_TARGET_ID
    }

Outside FSC: 'Direct Access Grant': the user is authenticated and its credentials are used in app/
pipeline. Dedicated keycloak 'public' clients. Tokens:
    payload = {
        "client_id": "ZePublicClientId",
        "username": "username",
        "password": "whatever"
        "grant_type": "password",
        "scope": "https://fsc.svom.api"
    }

Then, to use the access token:
    header = {"Authorization": "Bearer XXX.YYY.ZZZ"}

-------------------------------------------------------------------------------------
ENV/secret:
    - KC_TYPE = "pipeline" -> defines docker secret to use (only inside FSC)
        - pipeline_kc_client_id
        - pipeline_kc_client_secret

    (outside FSC -> locally, docker secret not set, not available)
    - KC_USERNAME -> username
    - KC_PASSWORD -> password
    - KC_CLIENT_ID -> public kc client_id
-------------------------------------------------------------------------------------
"""
import os
import time
import requests
import logging
from align_cli.log_setup import setup_logger
import aiohttp
import asyncio
import getpass
import json
import attrs

from .utils import power_delay


log = logging.getLogger("aligncli.kc_tokens")


AUTH_TENANT = "auth.cern.ch/auth/realms/cern"
AUTH_TOKEN_ENDPOINT = "/protocol/openid-connect/token"
AUTH_CLIENT_TOKEN_ENDPOINT = "/api-access/token"
AUTH_URL = "https://{}{}".format(
    AUTH_TENANT,
    AUTH_TOKEN_ENDPOINT,
)
AUTH_API_URL = "https://{}{}".format(
    AUTH_TENANT,
    AUTH_CLIENT_TOKEN_ENDPOINT,
)
AUTH_HEADER = {"Content-Type": "application/x-www-form-urlencoded"}

SERVICE_KC_CLIENT_ID = 'align-mon-cli-client'
SERVICE_KC_TARGET_ID = 'atlas-muon-align-mon-server'


def store_payload(filename='.client-exchange-token.json', pyld=None):
    if os.path.exists(filename):
        log.info(f'File {filename} already exists...do not overwrite')
        return
    with open(filename, 'w') as f:
        json.dump(pyld, f)
    f.close()


def read_payload(filename='.client-exchange-token.json'):
    p = None
    if not os.path.exists(filename):
        log.info(f'File {filename} does not exists...')
        return p
    with open(filename, 'r') as f:
        pyld = f.read()
        p = json.loads(pyld)
    f.close()
    return p


def get_token_file_time(path=None):
    # Both the variables would contain time
    # elapsed since EPOCH in float
    ti_c = os.path.getctime(path)
    ti_m = os.path.getmtime(path)

    # Converting the time in seconds to a timestamp
    c_ti = time.ctime(ti_c)
    m_ti = time.ctime(ti_m)

    log.info(
        f"The file located at the path {path} was "
        f"created at {c_ti} and was last modified at {m_ti}")
    return ti_c, ti_m, c_ti, m_ti


def get_username_password():
    # Ask for user/pass
    username = os.environ.get('KC_USERNAME')
    password = os.environ.get('KC_PASSWORD')
    if username is None:
        print('User: ')
        username = input()
        password = getpass.getpass()
    return username, password


def get_subject_token():
    # Check from file
    tok = read_payload()
    if tok is not None:
        return tok['access_token']
    else:
        log.warning(f'There is no access token exchange file...you should generate one')
    return None


@attrs.define
class Tokens:
    """Tokens are objects returned by a Keycloak request for access tokens."""

    access_token: str
    expires_in: int
    refresh_expires_in: int
    refresh_token: str
    token_type: str
    not_before_policy: int
    session_state: str
    scope: str

    @classmethod
    def from_dict(cls, my_dict) -> "Tokens":
        """Instanciate from dictionnary"""
        return cls(
            my_dict.get("access_token", ""),
            my_dict.get("expires_in", 0),
            my_dict.get("refresh_expires_in", 0),
            my_dict.get("refresh_token", ""),
            my_dict.get("token_type", ""),
            my_dict.get("not-before-policy", 0),
            my_dict.get("session_state", ""),
            my_dict.get("scope", ""),
        )

    def to_dict(self):
        return {
            'access_token': self.access_token,
            'expires_in': self.expires_in,
            'refresh_expires_in': self.refresh_expires_in,
            'refresh_token': self.refresh_token,
            'token_type': self.token_type,
            'not-before-policy': self.not_before_policy,
            'session_state': self.session_state,
            'scope': self.scope
        }


@attrs.define
class TokensExpirations:
    """The TokensExpirations Class to register the tokens expirations when
    requesting or refreshing one.
    """

    expires_at: int
    refresh_expires_at: int
    expires_at_threshold: int

    @classmethod
    def from_tokens(cls, tokens, expires_at_threshold=10, timestamp=None) -> None:
        """Instanciate from tokens after request"""
        timestamp = time.time() if timestamp is None else timestamp
        exp_at = int(timestamp) + tokens.expires_in - expires_at_threshold
        ref_expires_at = int(timestamp) + tokens.refresh_expires_in - expires_at_threshold
        return cls(
            exp_at,
            ref_expires_at,
            expires_at_threshold,
        )

    @classmethod
    def from_dict(cls, my_dict):
        """Instanciate from dict"""
        return cls(
            my_dict.get("expires_at", 0),
            my_dict.get("refresh_expires_at", 0),
            my_dict.get("expires_at_threshold", 10),
        )

    def access_is_expired(self) -> bool:
        """Return True if access token is expired"""
        return time.time() > self.expires_at

    def refresh_is_expired(self) -> bool:
        """Return True if refresh token is expired"""
        return time.time() > self.refresh_expires_at


@attrs.define
class TokensWithExpirationsDates:
    """To gather tokens and their expirations date if provided by external clients. The timestamp
    for expirations dates must be set when fetching the tokens, not after."""

    tokens: Tokens
    expirations: TokensExpirations

    @classmethod
    def from_dict(cls, my_dict):
        """Instanciate from dict"""
        return cls(
            Tokens.from_dict(my_dict.get("tokens", {})),
            TokensExpirations.from_dict(my_dict.get("expirations", {})),
        )

    @classmethod
    def from_dict_resp(cls, my_dict, expires_at_threshold=10, timestamp=None):
        """Instanciate from request json response (dict)"""
        tokens = Tokens.from_dict(my_dict)
        return cls(
            tokens,
            TokensExpirations.from_tokens(tokens, expires_at_threshold, timestamp),
        )

    def access_is_expired(self):
        """To check that inner access token is expired"""
        return self.expirations.access_is_expired()

    def refresh_is_expired(self):
        """To check that inner refresh token is expired"""
        return self.expirations.refresh_is_expired()


@attrs.define
class RetryParams:
    """RetryParams class to gather fields to manage request session"""

    max_tries: int
    backoff_factor: int


@attrs.define
class ClientCredentialsPayload:
    """in terms of OAuth2
    The Client Credentials grant type is used by clients to obtain an access token
    outside of the context of a user.

    This is typically used by clients to access resources about
    themselves rather than to access a user's resources.
    """

    client_id: str
    client_secret: str
    grant_type: str
    requested_token_type: str
    audience: str
    #    subject_token: str
    scope: str = "openid"

    @classmethod
    def from_config(cls, config) -> "ClientCredentialsPayload":
        """Instanciate from config"""
        return cls(config.client_id, config.client_secret, config.grant_type,
                   config.requested_token_type, config.audience)
        # this was also taken in old code...do not think it is needed, config.subject_token)


@attrs.define
class ExchangeCredentialsPayload:
    """in terms of OAuth2
    The Exchange Credentials grant type is used by clients to obtain an access token
    from an existing exchange token.
    """

    client_id: str
    subject_token: str
    scope: str = "openid"
    grant_type: str = "urn:ietf:params:oauth:grant-type:token-exchange"
    #    grant_type: str = "client_credentials"
    requested_token_type: str = "urn:ietf:params:oauth:token-type:refresh_token"
    audience: str = SERVICE_KC_TARGET_ID

    @classmethod
    def from_config(cls, config) -> "ExchangeCredentialsPayload":
        """Instanciate from config"""
        return cls(config.client_id, config.subject_token)


@attrs.define
class ResourceOwnerPasswordCredentialsPayload:
    """in terms of OAuth2
    The resource owner password credentials grant workflow allows for the exchanging
    of the user name and password of a user for an access token.

    When using the resource owner password credentials grant, the user provides the credentials
    (user name and password) directly to the application.
    The application then uses the credentials to obtain an access token from the service.
    """

    client_id: str
    username: str
    password: str
    scope: str = "openid"
    grant_type: str = "password"

    @classmethod
    def from_config(cls, config) -> "ResourceOwnerPasswordCredentialsPayload":
        """Instanciate from config"""
        return cls(config.client_id, config.username, config.password)


@attrs.define
class RefreshPayload:
    """A RefreshPayload instance is used when refreshing a token to Keycloak."""

    client_id: str
    refresh_token: str
    grant_type: str = "urn:ietf:params:oauth:grant-type:token-exchange"

    @classmethod
    def from_config_tokens(cls, config, tokens) -> "RefreshPayload":
        """Instanciate from config and tokens (to get the refresh token)"""
        return cls(config.client_id, tokens.refresh_token)


@attrs.define
class Payload:
    """Abstract ?"""

    @classmethod
    def from_config(cls, config):
        """Instanciate from config"""
        if config.subject_token is not None:
            log.info(f'Create an exchange credential payload')
            return ExchangeCredentialsPayload.from_config(config)

        if config.username is not None:
            log.info(f'Create a user/pass credential payload')
            return ResourceOwnerPasswordCredentialsPayload.from_config(config)
        log.info(f'Create a client credential payload')

        return ClientCredentialsPayload.from_config(config)


@attrs.define
class AuthorizationConfig:
    """This class stores all params used to configure security."""

    kc_type: str
    kc_client_id: str
    kc_client_secret: str
    kc_auth_url: str
    kc_target_id: str

    @classmethod
    def from_env(cls) -> "AuthorizationConfig":
        """Depending on properties file, create a configuration to get correct params.
        ENV:
            - KC_TYPE: "exchange", "api", "secret"
                -> retrieve docker secrets associated with client_id/client_secret
            - KC_CLIENT_ID
                -> either a public client id or a protected one
            - KC_CLIENT_SECRET
                -> mandatory with protected KC_CLIENT_ID
        """
        properties = {}
        if os.path.isfile('./config/authentication.json'):
            with open('./config/authentication.json') as json_file:
                prop_dic = json.load(json_file)
                for key in prop_dic.keys():
                    log.info(f'Update property from file for key {key}')
                    properties[key] = prop_dic[key]

        env_kc_type = 'exchange'
        if 'KC_TYPE' in properties.keys():
            env_kc_type = properties['KC_TYPE']
        log.info("    -- kc_type: %s", env_kc_type)

        env_kc_client_id = SERVICE_KC_CLIENT_ID
        if 'KC_CLIENT_ID' in properties.keys():
            env_kc_client_id = properties['KC_CLIENT_ID']
        log.info("    -- kc_client_id: %s", env_kc_client_id)

        env_kc_client_secret = None
        if 'KC_CLIENT_SECRET' in properties.keys():
            env_kc_client_secret = properties['KC_CLIENT_SECRET']
        if env_kc_client_secret is None and env_kc_type != 'exchange':
            log.warning(f'Cannot have None secret with kc_type {env_kc_type}')

        env_kc_auth_url = AUTH_URL
        if env_kc_type == 'secret':
            env_kc_auth_url = AUTH_API_URL
        log.info(f'The URL for keycloak is : {env_kc_auth_url}')

        env_kc_target_id = SERVICE_KC_TARGET_ID

        return cls(
            env_kc_type, env_kc_client_id, env_kc_client_secret, env_kc_auth_url, env_kc_target_id
        )


@attrs.define
class Config:
    """This class stores all params used in keycloak token requests payloads."""

    type: str
    client_id: str
    client_secret: str
    username: str
    password: str
    subject_token: str
    grant_type: str
    requested_token_type: str
    audience: str

    @classmethod
    def from_auth_config(cls, auth_config) -> "Config":
        """Depending on auth_config var, create a payload to be POSTed to keycloak server, to get tokens.
        ENV:
            - KC_TYPE: "pipeline", "ic"
                -> retrieve docker secrets associated with client_id/client_secret
            - KC_CLIENT_ID
                -> outside fsc, public keycloak client id, dedicated to authenticate users
            - KC_USERNAME
                -> mandatory with KC_CLIENT_ID
            - KC_PASSWORD
                -> mandatory with KC_CLIENT_ID
        At least KC_TYPE or KC_CLIENT_ID must be set! If both, KC_CLIENT_ID is highest priority.

        Inside FSC, in a docker compose, only KC_TYPE should be set.
        """
        env_kc_type = auth_config.kc_type
        log.info("    -- kc_type: %s", env_kc_type)

        env_kc_client_id = auth_config.kc_client_id
        log.info("    -- kc_client_id: %s", env_kc_client_id)

        env_kc_client_secret = auth_config.kc_client_secret
        if env_kc_client_secret is None:
            log.warning("No secret provided: adopt token exchange config ?")

        if not env_kc_type and not env_kc_client_id:
            log.error("You must at least set one of these properties: KC_TYPE, KC_CLIENT_ID.")
            raise ValueError

        env_grant_type = "urn:ietf:params:oauth:grant-type:token-exchange"
        env_requested_token_type = "urn:ietf:params:oauth:token-type:refresh_token"
        if env_kc_type == 'secret':
            env_grant_type = "client_credentials"
            env_requested_token_type = None
        env_audience = auth_config.kc_target_id

        env_kc_username = None
        env_kc_password = None
        env_kc_subjtok = None

        log.info(f'The configuration is of type : {env_kc_type}')

        if 'exchange' in env_kc_type:
            # Check first if an existing file is present, in that case it will try to use that file
            # Here we should simply verify file existence
            if os.path.exists('.client-exchange-token.json'):
                log.info(f'A token exchange seems to exists...try to use the file later on')
            else:
                # this is a configuration for an exchange token request
                # we should then ask for username and password (command line or env variables)
                env_kc_username, env_kc_password = get_username_password()

        if 'api' in env_kc_type:
            env_kc_subjtok = get_subject_token()

        if 'secret' in env_kc_type:
            if env_kc_client_secret is None:
                log.error(
                    "You must set property: KC_CLIENT_SECRET with the KC_CLIENT_ID"
                )
                raise ValueError

        return cls(
            env_kc_type, env_kc_client_id, env_kc_client_secret, env_kc_username, env_kc_password,
            env_kc_subjtok, env_grant_type, env_requested_token_type, env_audience
        )


class KcExchangeTokens:
    """A class to request exchange tokens from Keycloak auth server. An access token expires
    after a validity time. Meanwhile, no new requests are made to Keycloak.
    - if env var are not properly set, __init__ will raise ValueError.
    - if tokens requests fail -> format_header returns {},
        else {"Authorization": "Bearer XXX.YYY.ZZZ"}
    - if tokens are passed in init, these tokens will be used instead of requesting keycloak, except
    if the tokens is not valid anymore.
    """

    def __init__(
            self,
            max_tries=1,
            backoff_factor=1,
            expires_at_threshold=10,
            tokens=None,
            auth_config=None
    ):
        """The init method"""
        # we want config ok even if tokens are not None, to be able to fetch tokens if expired
        log.info(f'Init a KcExchangeTokens')
        self.config = Config.from_auth_config(auth_config=auth_config)
        self.retry_params = RetryParams(max_tries, backoff_factor)
        self.expires_at_threshold = expires_at_threshold
        if os.path.exists('.client-exchange-token.json'):
            if tokens is None:
                tokens_dic = read_payload('.client-exchange-token.json')
                t_c, t_m, c_t, m_t = get_token_file_time(path='.client-exchange-token.json')
                tokens = TokensWithExpirationsDates.from_dict_resp(tokens_dic,
                                                                   expires_at_threshold=
                                                                   self.expires_at_threshold,
                                                                   timestamp=t_c)
        if tokens is None:
            self.tokens_with_expirations_dates = None
        else:
            assert isinstance(tokens, TokensWithExpirationsDates)
            self.tokens_with_expirations_dates = tokens

    def access_is_expired(self):
        """Calls tokens_expirations access is expired -> True if so"""
        return self.tokens_with_expirations_dates.access_is_expired()

    def refresh_is_expired(self):
        """Calls tokens_expirations refresh is expired -> True if so"""
        return self.tokens_with_expirations_dates.refresh_is_expired()

    def get_exchange_token(self):
        log.info(f'Get or generate exchange token')
        if self.tokens_with_expirations_dates is None:
            log.info(f'No token available...request a new one')
            self._request_tokens()
            store_payload('.client-exchange-token.json',
                          self.tokens_with_expirations_dates.tokens.to_dict())
            return
        if self.access_is_expired():
            log.warning(f'We should refresh this token')
            self.request_or_refresh()
        if self.refresh_is_expired():
            log.warning(f'We should get a new token...remove old cache')
            os.remove('.client-exchange-token.json')

    def request_or_refresh(self):
        """request or refresh tokens depending on expiration"""

        if self.tokens_with_expirations_dates is not None:
            if self.access_is_expired():
                if not self.refresh_is_expired():
                    # Never true when no refresh token -> refresh_expires_in = 0 in zis caze.
                    self._refresh_tokens()
                else:
                    self._request_tokens()
        else:
            self._request_tokens()

    def _refresh_tokens(
            self,
    ):
        """https://stackoverflow.com/questions/38986005/what-is-the-purpose-of-a-refresh-token
        Refresh the access token using the refresh token. Refresh token is only exchanged
        with authorization server.
        """
        log.info(" -- Auth: refresh access token")

        # Request to refresh an access token
        tried = 0
        response = None
        while tried < self.retry_params.max_tries:
            tried += 1
            token_url = AUTH_URL
            log.info(f'Get refresh token from url {token_url}')
            response = requests.post(
                token_url,
                data=attrs.asdict(
                    RefreshPayload.from_config_tokens(
                        self.config, self.tokens_with_expirations_dates.tokens
                    )
                ),
                headers=AUTH_HEADER,
            )

            if response.status_code == 200:
                break

            if tried >= self.retry_params.max_tries:
                log.warning(" -- Auth: -- [refresh] status: %s", response.status_code)
                log.error(
                    " -- Auth: -- [refresh] max tries exceeded (%s) with: %s",
                    self.retry_params.max_tries,
                    response.text,
                )
                log.error(" -- Auth: -- [refresh] failing to get tokens, aborting!")
                break

            delay = power_delay(tried, self.retry_params.backoff_factor)
            log.warning(
                " -- Auth: -- [refresh] failed: %s. Trying again in %ss",
                response.status_code,
                delay,
            )
            time.sleep(delay)

        try:
            self.tokens_with_expirations_dates = (
                TokensWithExpirationsDates.from_dict_resp(
                    response.json(), self.expires_at_threshold
                )
            )
        except Exception as err:
            log.error(" -- Auth: [refresh] error to get json response: %s", err)

    def _request_tokens(
            self,
    ):
        """POST request to keycloak server for tokens."""
        log.info(" -- Auth: get exchange access token")

        # Request to get an access token
        tried = 0
        response = None

        while tried < self.retry_params.max_tries:
            tried += 1
            token_url = AUTH_URL
            payload_data = attrs.asdict(Payload.from_config(self.config))
            log.info(f'Sending request with {payload_data} and {AUTH_HEADER} to {token_url}')
            response = requests.post(
                token_url,
                data=payload_data,
                headers=AUTH_HEADER,
            )

            if response.status_code == 200:
                log.info(f'Exchange Token Received response: {response}')
                break

            if tried >= self.retry_params.max_tries:
                log.warning(" -- Auth: -- status: %s", response.status_code)
                log.error(
                    " -- Auth: -- max tries exceeded (%s) with: %s",
                    self.retry_params.max_tries,
                    response.text,
                )
                log.error(" -- Auth: -- failing to get tokens, aborting!")
                break

            delay = power_delay(tried, self.retry_params.backoff_factor)
            log.warning(
                " -- Auth: -- failed: %s. Trying again in %ss",
                response.status_code,
                delay,
            )
            time.sleep(delay)

        try:
            self.tokens_with_expirations_dates = (
                TokensWithExpirationsDates.from_dict_resp(
                    response.json(), self.expires_at_threshold
                )
            )
        except Exception as err:
            log.error(" -- Auth: error to get json response: %s", err)


class KcApiTokens:
    """A class to request users/app tokens from Keycloak auth server. An access token expires
    after a validity time. Meanwhile, no new requests are made to Keycloak.
    - if env var are not properly set, __init__ will raise ValueError.
    - if tokens requests fail -> format_header returns {},
        else {"Authorization": "Bearer XXX.YYY.ZZZ"}
    - if tokens are passed in init, these tokens will be used instead of requesting keycloak, except
    if the tokens is not valid anymore.
    """

    def __init__(
            self,
            max_tries=2,
            backoff_factor=1,
            expires_at_threshold=10,
            loop=None,
            tokens=None,
            auth_config=None
    ):
        """The init method"""
        # we want config ok even if tokens are not None, to be able to fetch tokens if expired
        log.info(f'Init a KcApiTokens')
        if auth_config.kc_type is None:
            auth_config.kc_type = "api"

        self._auth_config = auth_config
        self.config = Config.from_auth_config(auth_config=auth_config)
        self.retry_params = RetryParams(max_tries, backoff_factor)
        self.expires_at_threshold = expires_at_threshold
        if os.path.exists('.client-api-token.json'):
            if tokens is None:
                tokens_dic = read_payload('.client-api-token.json')
                t_c, t_m, c_t, m_t = get_token_file_time(path='.client-api-token.json')
                tokens = TokensWithExpirationsDates.from_dict_resp(tokens_dic,
                                                                   expires_at_threshold=
                                                                   self.expires_at_threshold,
                                                                   timestamp=t_c)

        if tokens is None:
            self.tokens_with_expirations_dates = None
        else:
            assert isinstance(tokens, TokensWithExpirationsDates)
            self.tokens_with_expirations_dates = tokens

        self.loop = loop

    def access_is_expired(self):
        """Calls tokens_expirations access is expired -> True if so"""
        return self.tokens_with_expirations_dates.access_is_expired()

    def refresh_is_expired(self):
        """Calls tokens_expirations refresh is expired -> True if so"""
        return self.tokens_with_expirations_dates.refresh_is_expired()

    def _request_tokens(
            self,
    ):
        """POST request to keycloak server for tokens."""
        log.info(" -- Auth: get access token")

        # Request to get an access token
        tried = 0
        response = None
        payload_data = attrs.asdict(Payload.from_config(self.config))
        token_url = self._auth_config.kc_auth_url
        if payload_data['grant_type'] == 'client_credentials':
            payload_data.pop('requested_token_type')
            payload_data.pop('scope')
        while tried < self.retry_params.max_tries:
            tried += 1
            log.info(f'Sending request with {payload_data} and {AUTH_HEADER} using url {token_url}')
            response = requests.post(
                token_url,
                data=payload_data,
                headers=AUTH_HEADER,
            )

            if response.status_code == 200:
                break

            if tried >= self.retry_params.max_tries:
                log.warning(" -- Auth: -- status: %s", response.status_code)
                log.error(
                    " -- Auth: -- max tries exceeded (%s) with: %s",
                    self.retry_params.max_tries,
                    response.text,
                )
                log.error(" -- Auth: -- failing to get tokens, aborting!")
                break

            delay = power_delay(tried, self.retry_params.backoff_factor)
            log.warning(
                " -- Auth: -- failed: %s. Trying again in %ss",
                response.status_code,
                delay,
            )
            time.sleep(delay)

        try:
            self.tokens_with_expirations_dates = (
                TokensWithExpirationsDates.from_dict_resp(
                    response.json(), self.expires_at_threshold
                )
            )
        except Exception as err:
            log.error(" -- Auth: error to get json response: %s", err)

    def _refresh_tokens(
            self,
    ):
        """https://stackoverflow.com/questions/38986005/what-is-the-purpose-of-a-refresh-token
        Refresh the access token using the refresh token. Refresh token is only exchanged
        with authorization server.
        """
        log.info(" -- Auth: refresh access token")

        # Request to refresh an access token
        tried = 0
        response = None
        while tried < self.retry_params.max_tries:
            tried += 1
            response = requests.post(
                AUTH_URL,
                data=attrs.asdict(
                    RefreshPayload.from_config_tokens(
                        self.config, self.tokens_with_expirations_dates.tokens
                    )
                ),
                headers=AUTH_HEADER,
            )

            if response.status_code == 200:
                break

            if tried >= self.retry_params.max_tries:
                log.warning(" -- Auth: -- [refresh] status: %s", response.status_code)
                log.error(
                    " -- Auth: -- [refresh] max tries exceeded (%s) with: %s",
                    self.retry_params.max_tries,
                    response.text,
                )
                log.error(" -- Auth: -- [refresh] failing to get tokens, aborting!")
                break

            delay = power_delay(tried, self.retry_params.backoff_factor)
            log.warning(
                " -- Auth: -- [refresh] failed: %s. Trying again in %ss",
                response.status_code,
                delay,
            )
            time.sleep(delay)

        try:
            self.tokens_with_expirations_dates = (
                TokensWithExpirationsDates.from_dict_resp(
                    response.json(), self.expires_at_threshold
                )
            )
        except Exception as err:
            log.error(" -- Auth: [refresh] error to get json response: %s", err)

    def request_or_refresh(self):
        """request or refresh tokens depending on expiration"""

        if self.tokens_with_expirations_dates is not None:
            if self.access_is_expired():
                if not self.refresh_is_expired():
                    # Never true when no refresh token -> refresh_expires_in = 0 in zis caze.
                    self._refresh_tokens()
                else:
                    self._request_tokens()
        else:
            self._request_tokens()

    def format_header(
            self,
    ):
        """If tokens, format the Authorization header."""
        header = {}
        if self.tokens_with_expirations_dates:
            header = {
                "Authorization": "{} {}".format(
                    self.tokens_with_expirations_dates.tokens.token_type,
                    self.tokens_with_expirations_dates.tokens.access_token,
                ),
            }
        else:
            log.warning("-- Auth: no Authorization header set.")

        return header

    def get_access_token(self):
        return self.tokens_with_expirations_dates.tokens.access_token

    def get_authorization_header(
            self,
    ):
        """Request for token if expired, and return the Authorization header."""

        self.request_or_refresh()
        return self.format_header()
