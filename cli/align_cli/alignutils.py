"""
Created on Nov 24, 2017

@author: formica
"""
from datetime import datetime
from beautifultable import BeautifulTable
from termcolor import colored
import logging
from align_cli.log_setup import setup_logger

log = logging.getLogger("aligncli.alignutils")

# log.info('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__,
#                                                                          str(__package__)))

# this should be finished eventually
# ecolsfieldsdicheader = {
#     'id': {'key': '{typ:4s}', 'val': 'Typ', 'field': '{typ:4s}', 'type': 'str'},
#     'jff': {'key': '{jff:3s}', 'val': 'Jff', 'field': '{jff:-3d}', 'type': 'int'},
#     'jzz': {'key': '{jzz:3s}', 'val': 'Jzz', 'field': '{jzz:-4d}', 'type': 'int'},
#     'job': {'key': '{job:3s}', 'val': 'Job', 'field': '{job:-3d}', 'type': 'int'},
#     'svalue': {'key': '{svalue:7s}', 'val': 'S', 'field': '{svalue:.6f}', 'type': 'float'},
#     'zvalue': {'key': '{zvalue:7s}', 'val': 'Z', 'field': '{zvalue:.6f}', 'type': 'float'},
#     'tvalue': {'key': '{tvalue:7s}', 'val': 'T', 'field': '{tvalue:.6f}', 'type': 'float'},
#     'tsv': {'key': '{tsv:7s}', 'val': 'ts', 'field': '{tsv:.6f}', 'type': 'float'},
#     'tzv': {'key': '{tzv:7s}', 'val': 'tz', 'field': '{tzv:.6f}', 'type': 'float'},
#     'ttv': {'key': '{ttv:7s}', 'val': 'tt', 'field': '{ttv:.6f}', 'type': 'float'},
#     'bz': {'key': '{bz:7s}', 'val': 'BZ', 'field': '{bz:.6f}', 'type': 'float'},
#     'bp': {'key': '{bp:7s}', 'val': 'BP', 'field': '{bp:.6f}', 'type': 'float'},
#     'bn': {'key': '{bn:7s}', 'val': 'BN', 'field': '{bn:.6f}', 'type': 'float'},
#     'sp': {'key': '{sp:7s}', 'val': 'SP', 'field': '{sp:.6f}', 'type': 'float'},
#     'sn': {'key': '{sn:7s}', 'val': 'SN', 'field': '{sn:.6f}', 'type': 'float'},
#     'tw': {'key': '{tw:7s}', 'val': 'TW', 'field': '{tw:.6f}', 'type': 'float'},
#     'pg': {'key': '{pg:7s}', 'val': 'PG', 'field': '{pg:.6f}', 'type': 'float'},
#     'tr': {'key': '{tr:7s}', 'val': 'TR', 'field': '{tr:.6f}', 'type': 'float'},
#     'eg': {'key': '{eg:7s}', 'val': 'EG', 'field': '{eg:.6f}', 'type': 'float'},
#     'ep': {'key': '{ep:7s}', 'val': 'EP', 'field': '{ep:.6f}', 'type': 'float'},
#     'en': {'key': '{en:7s}', 'val': 'EN', 'field': '{en:.6f}', 'type': 'float'},
#     'xatlas': {'key': '{xatlas:7s}', 'val': 'XATLAS', 'field': '{xatlas:.3f}', 'type': 'float'},
#     'yatlas': {'key': '{yatlas:7s}', 'val': 'YATLAS', 'field': '{yatlas:.3f}', 'type': 'float'},
#     'hwElement': {'key': '{hwElement:10s}', 'val': 'Element', 'field': '{hwElement:10s}',
#                   'type': 'str'},
# }
pullsfieldsdicheader = {
    'olname': {'key': '{olname:25s}', 'val': 'Name', 'field': '{olname:25s}', 'type': 'str'},
    'iovid': {'key': '{iovid:20s}', 'val': 'Iov', 'field': '{iovid:10d}', 'type': 'long'},
    'xPull': {'key': '{xPull:10s}', 'val': 'X Pull', 'field': '{xPull:10f}', 'type': 'float'},
    'yPull': {'key': '{yPull:10s}', 'val': 'Y Pull', 'field': '{yPull:10f}', 'type': 'float'},
    'zPull': {'key': '{zPull:10s}', 'val': 'Z Pull', 'field': '{zPull:10f}', 'type': 'float'},
}
sagittasfieldsdicheader = {
    'sagId': {'key': '{sagId:10s}', 'val': 'Id', 'field': '{sagId:25s}', 'type': 'long'},
    'xtrack': {'key': '{xtrack:10s}', 'val': 'X track', 'field': '{xtrack:10f}', 'type': 'float'},
    'ytrack': {'key': '{ytrack:10s}', 'val': 'Y track', 'field': '{ytrack:10f}', 'type': 'float'},
    'ztrack': {'key': '{ztrack:10s}', 'val': 'Z track', 'field': '{ztrack:10f}', 'type': 'float'},
    'uxtrack': {'key': '{uxtrack:10s}', 'val': 'UX track', 'field': '{uxtrack:10f}',
                'type': 'float'},
    'uytrack': {'key': '{uytrack:10s}', 'val': 'UY track', 'field': '{uytrack:10f}',
                'type': 'float'},
    'uztrack': {'key': '{uztrack:10s}', 'val': 'UZ track', 'field': '{uztrack:10f}',
                'type': 'float'},
    'bm': {'key': '{bm:10s}', 'val': 'BM', 'field': '{bm:10s}', 'type': 'str'},
    'bi': {'key': '{bi:10s}', 'val': 'BI', 'field': '{bi:10s}', 'type': 'str'},
    'bo': {'key': '{bo:10s}', 'val': 'BO', 'field': '{bo:10s}', 'type': 'str'},
}
stepsfieldsdicheader = {
    'fitId': {'key': '{fitId:10s}', 'val': 'Id', 'field': '{fitId:10d}', 'type': 'long'},
    'chiSquareOvDof': {'key': '{chiSquareOvDof:10s}', 'val': 'Chi2/DoF',
                       'field': '{chiSquareOvDof:10f}',
                       'type': 'float'},
    'chiSquare': {'key': '{chiSquare:10s}', 'val': 'Chi2', 'field': '{chiSquare:10f}',
                  'type': 'float'},
    'dof': {'key': '{dof:7s}', 'val': 'DoF', 'field': '{dof:7d}', 'type': 'int'},
    'prob': {'key': '{prob:8s}', 'val': 'Prob', 'field': '{prob:8f}', 'type': 'float'},
    'sequenceNumber': {'key': '{sequenceNumber:5s}', 'val': 'Seq', 'field': '{sequenceNumber:5d}',
                       'type': 'int'},
    'fitDescription': {'key': '{fitDescription:30s}', 'val': 'Description',
                       'field': '{fitDescription:30s}',
                       'type': 'str'},
    'nIterations': {'key': '{nIterations:6s}', 'val': 'NIter', 'field': '{nIterations:6d}',
                    'type': 'int'},
    'exitCode': {'key': '{exitCode:10s}', 'val': 'ExCode', 'field': '{exitCode:5d}', 'type': 'int'},
    'branch': {'key': '{branch:10s}', 'val': 'Branch', 'field': '{branch:10s}', 'type': 'str'},
    'revision': {'key': '{revision:10s}', 'val': 'Revision', 'field': '{revision:10s}',
                 'type': 'str'},
}
optlinefieldsdicheader = {
    'olname': {'key': '{olname:25s}', 'val': 'Name', 'field': '{olname:25s}', 'type': 'str'},
    'oltype': {'key': '{oltype:20s}', 'val': 'Type', 'field': '{oltype:20s}', 'type': 'str'},
    'olstatus': {'key': '{olstatus:20s}', 'val': 'Status', 'field': '{olstatus:20s}',
                 'type': 'str'},
}
incidentsdicheader = {
    'ol_name': {'key': '{ol_name:25s}', 'val': 'Name', 'field': '{ol_name:25s}', 'type': 'str'},
    'incident_category': {'key': '{incident_category:15s}', 'val': 'Category', 'field': '{incident_category:25s}', 'type': 'str'},
    'incident_date': {'key': '{incident_date:20s}', 'val': 'Date', 'field': '{incident_date:20s}', 'type': 'str'},
    'last_comment': {'key': '{last_comment:50s}', 'val': 'Comment', 'field': '{last_comment:50s}', 'type': 'str'},
}

corrfieldsdicheaderamdb = {
    'corrId': {'key': '{corrId:10s}', 'val': 'Id', 'field': '{corrId:10d}', 'type': 'int'},
    'corrDescription': {'key': '{corrDescription:4s}', 'val': 'Desc',
                        'field': '{corrDescription:4s}', 'type': 'str'},
    'typ': {'key': '{typ:4s}', 'val': 'Typ', 'field': '{typ:4s}', 'type': 'str'},
    'jff': {'key': '{jff:3s}', 'val': 'Jff', 'field': '{jff:-3d}', 'type': 'int'},
    'jzz': {'key': '{jzz:3s}', 'val': 'Jzz', 'field': '{jzz:-4d}', 'type': 'int'},
    'job': {'key': '{job:3s}', 'val': 'Job', 'field': '{job:-3d}', 'type': 'int'},
    'svalue': {'key': '{svalue:7s}', 'val': 'S', 'field': '{svalue:-7.3f}', 'type': 'float'},
    'zvalue': {'key': '{zvalue:7s}', 'val': 'Z', 'field': '{zvalue:-7.3f}', 'type': 'float'},
    'tvalue': {'key': '{tvalue:7s}', 'val': 'T', 'field': '{tvalue:-7.3f}', 'type': 'float'},
    'tsv': {'key': '{tsv:7s}', 'val': 'ts', 'field': '{tsv:-9.6f}', 'type': 'float'},
    'tzv': {'key': '{tzv:7s}', 'val': 'tz', 'field': '{tzv:-9.6f}', 'type': 'float'},
    'ttv': {'key': '{ttv:7s}', 'val': 'tt', 'field': '{ttv:-9.6f}', 'type': 'float'},
    'bz': {'key': '{bz:7s}', 'val': 'BZ', 'field': '{bz:-9.6f}', 'type': 'float'},
    'bp': {'key': '{bp:7s}', 'val': 'BP', 'field': '{bp:-9.6f}', 'type': 'float'},
    'bn': {'key': '{bn:7s}', 'val': 'BN', 'field': '{bn:-9.6f}', 'type': 'float'},
    'sp': {'key': '{sp:7s}', 'val': 'SP', 'field': '{sp:-9.6f}', 'type': 'float'},
    'sn': {'key': '{sn:7s}', 'val': 'SN', 'field': '{sn:-9.6f}', 'type': 'float'},
    'tw': {'key': '{tw:7s}', 'val': 'TW', 'field': '{tw:-9.6f}', 'type': 'float'},
    'pg': {'key': '{pg:7s}', 'val': 'PG', 'field': '{pg:-9.6f}', 'type': 'float'},
    'tr': {'key': '{tr:7s}', 'val': 'TR', 'field': '{tr:-9.6f}', 'type': 'float'},
    'eg': {'key': '{eg:7s}', 'val': 'EG', 'field': '{eg:-9.6f}', 'type': 'float'},
    'ep': {'key': '{ep:7s}', 'val': 'EP', 'field': '{ep:-9.6f}', 'type': 'float'},
    'en': {'key': '{en:7s}', 'val': 'EN', 'field': '{en:-9.6f}', 'type': 'float'},
    'xatlas': {'key': '{xatlas:7s}', 'val': 'XATLAS', 'field': '{xatlas:.3f}', 'type': 'float',
               'default': -999.},
    'yatlas': {'key': '{yatlas:7s}', 'val': 'YATLAS', 'field': '{yatlas:.3f}', 'type': 'float',
               'default': -999.},
    'hwElement': {'key': '{hwElement:10s}', 'val': 'Element', 'field': '{hwElement:10s}',
                  'type': 'str'},
}

corrfieldsdicheader = {
    'typ': {'key': '{typ:4s}', 'val': 'Typ', 'field': '{typ:4s}', 'type': 'str'},
    'jff': {'key': '{jff:3s}', 'val': 'Jff', 'field': '{jff:-3d}', 'type': 'int'},
    'jzz': {'key': '{jzz:3s}', 'val': 'Jzz', 'field': '{jzz:-4d}', 'type': 'int'},
    'job': {'key': '{job:3s}', 'val': 'Job', 'field': '{job:-3d}', 'type': 'int'},
    'svalue': {'key': '{svalue:7s}', 'val': 'S', 'field': '{svalue:.6f}', 'type': 'float'},
    'zvalue': {'key': '{zvalue:7s}', 'val': 'Z', 'field': '{zvalue:.6f}', 'type': 'float'},
    'tvalue': {'key': '{tvalue:7s}', 'val': 'T', 'field': '{tvalue:.6f}', 'type': 'float'},
    'tsv': {'key': '{tsv:7s}', 'val': 'ts', 'field': '{tsv:.6f}', 'type': 'float'},
    'tzv': {'key': '{tzv:7s}', 'val': 'tz', 'field': '{tzv:.6f}', 'type': 'float'},
    'ttv': {'key': '{ttv:7s}', 'val': 'tt', 'field': '{ttv:.6f}', 'type': 'float'},
    'bz': {'key': '{bz:7s}', 'val': 'BZ', 'field': '{bz:.6f}', 'type': 'float'},
    'bp': {'key': '{bp:7s}', 'val': 'BP', 'field': '{bp:.6f}', 'type': 'float'},
    'bn': {'key': '{bn:7s}', 'val': 'BN', 'field': '{bn:.6f}', 'type': 'float'},
    'sp': {'key': '{sp:7s}', 'val': 'SP', 'field': '{sp:.6f}', 'type': 'float'},
    'sn': {'key': '{sn:7s}', 'val': 'SN', 'field': '{sn:.6f}', 'type': 'float'},
    'tw': {'key': '{tw:7s}', 'val': 'TW', 'field': '{tw:.6f}', 'type': 'float'},
    'pg': {'key': '{pg:7s}', 'val': 'PG', 'field': '{pg:.6f}', 'type': 'float'},
    'tr': {'key': '{tr:7s}', 'val': 'TR', 'field': '{tr:.6f}', 'type': 'float'},
    'eg': {'key': '{eg:7s}', 'val': 'EG', 'field': '{eg:.6f}', 'type': 'float'},
    'ep': {'key': '{ep:7s}', 'val': 'EP', 'field': '{ep:.6f}', 'type': 'float'},
    'en': {'key': '{en:7s}', 'val': 'EN', 'field': '{en:.6f}', 'type': 'float'},
    'xatlas': {'key': '{xatlas:7s}', 'val': 'XATLAS', 'field': '{xatlas:.3f}', 'type': 'float'},
    'yatlas': {'key': '{yatlas:7s}', 'val': 'YATLAS', 'field': '{yatlas:.3f}', 'type': 'float'},
    'hwElement': {'key': '{hwElement:10s}', 'val': 'Element', 'field': '{hwElement:10s}',
                  'type': 'str'},
}

iovfieldsdicheader = {
    'iov_id': {'key': '{iov_id:10s}', 'val': 'Id', 'field': '{iov_id:10d}', 'type': 'int'},
    'since_t': {'key': '{since_t:30s}', 'val': 'Since', 'field': '{since_t:30s}', 'type': 'str'},
    'till_t': {'key': '{till_t:30s}', 'val': 'Until', 'field': '{till_t:30s}', 'type': 'str'},
    'fit_status': {'key': '{fit_status:3s}', 'val': 'Fit', 'field': '{fit_status:3d}', 'type': 'int'},
    'reco_status': {'key': '{reco_status:20s}', 'val': 'Cool', 'field': '{reco_status:20s}',
                   'type': 'str'},
    'iov_comment': {'key': '{iov_comment:20s}', 'val': 'Comment', 'field': '{iov_comment:20s}',
                   'type': 'str'},
}

imiovfieldsdicheader = {
    'iovId': {'key': '{iovId:10s}', 'val': 'Id', 'field': '{iovId:10d}', 'type': 'int'},
    'since': {'key': '{since:30s}', 'val': 'Since', 'field': '{since:30s}', 'type': 'str'},
    'till': {'key': '{till:30s}', 'val': 'Until', 'field': '{till:30s}', 'type': 'str'},
    'fitStatus': {'key': '{fitStatus:3s}', 'val': 'Fit', 'field': '{fitStatus:3d}', 'type': 'int'},
    'coolStatus': {'key': '{coolStatus:20s}', 'val': 'Cool', 'field': '{coolStatus:20s}',
                   'type': 'str'},
    'iovComment': {'key': '{iovComment:20s}', 'val': 'Comment', 'field': '{iovComment:20s}',
                   'type': 'str'},
}

tagfieldsdicheader = {
    'tag_id': {'key': '{tag_id:10s}', 'val': 'TagId', 'field': '{tag_id:10d}', 'type': 'long'},
    'iov_tag': {'key': '{iov_tag:25s}', 'val': 'Name', 'field': '{iov_tag:25s}', 'type': 'str'},
    'tag_type': {'key': '{tag_type:10s}', 'val': 'Type', 'field': '{tag_type:10s}', 'type': 'str'},
    'tag_insertion': {'key': '{tag_insertion:30s}', 'val': 'Insertion',
                      'field': '{tag_insertion:30s}', 'type': 'str'},
    'tag_description': {'key': '{tag_description:50s}', 'val': 'Description',
                        'field': '{tag_description:50s}',
                        'type': 'str'},
    'tag_author': {'key': '{tag_author:15s}', 'val': 'Author', 'field': '{tag_author:15s}',
                   'type': 'str'},
    'prev_id': {'key': '{prev_id:10s}', 'val': 'PrevId', 'field': '{prev_id:10d}', 'type': 'long'},
    'status': {'key': '{status:10s}', 'val': 'Status', 'field': '{status:10s}', 'type': 'str'},
}
imtagfieldsdicheader = {
    'tagId': {'key': '{tagId:10s}', 'val': 'TagId', 'field': '{tagId:10d}', 'type': 'long'},
    'tagName': {'key': '{tagName:25s}', 'val': 'Name', 'field': '{tagName:25s}', 'type': 'str'},
    'tagDescription': {'key': '{tagDescription:50s}', 'val': 'Description',
                       'field': '{tagDescription:50s}',
                       'type': 'str'},
    'tagInsertion': {'key': '{tagInsertion:30s}', 'val': 'Insertion', 'field': '{tagInsertion:30s}',
                     'type': 'str'},
}
imiovfieldsdicheader = {
    'id': {'key': '{id:10s}', 'val': 'Id', 'field': '{id:10d}', 'type': 'int'},
    'since': {'key': '{since:30s}', 'val': 'Since', 'field': '{since:30s}', 'type': 'str'},
    'till': {'key': '{till:30s}', 'val': 'Until', 'field': '{till:30s}', 'type': 'str'},
    'isFloating': {'key': '{isFloating:10s}', 'val': 'Floating', 'field': '{isFloating:10s}',
                   'type': 'bool'},
    'isStable': {'key': '{isStable:10s}', 'val': 'Stable', 'field': '{isStable:10s}',
                 'type': 'bool'},
}

imconfigfieldsdicheader = {
    'tag': {'key': '{tag:25s}', 'val': 'Name', 'field': '{tag:25s}', 'type': 'str'},
    'tagDescription': {'key': '{tagDescription:50s}', 'val': 'Description',
                       'field': '{tagDescription:50s}',
                       'type': 'str'},
    'saveToDB': {'key': '{saveToDB:6s}', 'val': 'DB store', 'field': '{saveToDB:6s}',
                 'type': 'bool'},
    'periodMinutes': {'key': '{periodMinutes:12s}', 'val': 'Periodicity',
                      'field': '{periodMinutes:12d}',
                      'type': 'int'},
    'iovRange': {'key': '{iovRange:10s}', 'val': 'Range', 'field': '{iovRange:10d}',
                 'type': 'int'},
    'iovMinLength': {'key': '{iovMinLength:10s}', 'val': 'MinLen', 'field': '{iovMinLength:10d}',
                     'type': 'int'},
    'magCurrentCut': {'key': '{magCurrentCut:10s}', 'val': 'MagCurrCut',
                      'field': '{magCurrentCut:10.2f}',
                      'type': 'float'},
}

cooltagfieldsdicheader = {
    'cool_tag': {'key': '{cool_tag:25s}', 'val': 'CoolTag', 'field': '{cool_tag:25s}',
                 'type': 'str'},
    'align_tag': {'key': '{coolTag:25s}', 'val': 'AlignTag', 'field': '{align_tag:25s}', 'type': 'str'},
    'tag_type': {'key': '{tag_type:10s}', 'val': 'Type', 'field': '{tag_type:10s}', 'type': 'str'},
    'tag_comment': {'key': '{tag_comment:30s}', 'val': 'Comment', 'field': '{tag_comment:30s}',
                   'type': 'str'},
    'do_upload_on_reco': {'key': '{do_upload_on_reco:5s}', 'val': 'upload',
                       'field': '{do_upload_on_reco:5s}', 'type': 'str'},
    'cherrypy_config': {'key': '{cherrypy_config:25s}', 'val': 'CherryPy',
                       'field': '{cherrypy_config:25s}',
                       'type': 'str'},
    'coollock': {'key': '{coollock:10s}', 'val': 'Lock', 'field': '{coollock:10s}', 'type': 'str'},
}


def server_print(srvdata, format=[]):
    if srvdata is None:
        print('Cannot find results to print')
        return
    size = srvdata['size']
    dataarr = []
    print(f'Found data list of size {size}')
    if 'resources' in srvdata:
        print(f'Retrieved {size} lines')
        dataarr = srvdata['resources']
    # if (srvdata['format'] == 'AlignMonBaseResponse'):
    #     prettyprint(format, ecolsfieldsdicheader, dataarr)
    if (srvdata['format'] == 'AligntagSetDto'):
        # dprint(format, tagfieldsdicheader, dataarr)
        prettyprint(format, tagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'OpticallinesSetDto'):
        prettyprint(format, optlinefieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlBSetDto'):
        prettyprint(format, optlinefieldsdicheader, dataarr)
    elif (srvdata['format'] == 'OpticallinesSetDtoProblems'):
        incidents = []
        for x in dataarr:
            if len(x['ol_incidents']) > 0:
                incidents = incidents + x['ol_incidents']
        print(f'Print incidents array: {incidents}')
        prettyprint([], incidentsdicheader, incidents, color='red')
    elif (srvdata['format'] == 'AligniovSetDto'):
        prettyprint(format, iovfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AligncorrectionsSetDto'):
        prettyprint(format, corrfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignFitPullsSetDto'):
        prettyprint(format, pullsfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignFitStepsSetDto'):
        prettyprint(format, stepsfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignCoolTagSetDto'):
        prettyprint(format, cooltagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'AlignSagittasSetDto'):
        dprint(format, sagittasfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerTagSetDto'):
        dprint(format, imtagfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerConfigurationSetDto'):
        dprint(format, imconfigfieldsdicheader, dataarr)
    elif (srvdata['format'] == 'IntervalMakerIovSetDto'):
        dprint(format, imiovfieldsdicheader, dataarr)
    else:
        print(srvdata)


def dprint(format, headerdic, cdata):
    if len(format) == 0:
        format = headerdic.keys()
    headerfmtstr = ' '.join([headerdic[k]['key'] for k in format])
    # print(f'The format string is {headerfmtstr}')
    headic = {}
    for k in format:
        headic[k] = headerdic[k]['val']
        # print(f'Using format {headic[k]} for {k}')
    print(headerfmtstr.format(**headic))
    # print('Use format %s' % format)
    fmtstr = ' '.join([headerdic[k]['field'] for k in format])
    # print('Format string %s'%fmtstr)
    for obj in cdata:
        xt = obj.to_dict()
        adic = {}
        for k in format:
            adic[k] = parse_dictionary(k, xt, headerdic)
            ##print(f'dump {k} {xt[k]} {headerdic[k]["type"]}')

        print(fmtstr.format(**adic))


def parse_dictionary(k, xt, headerdic):
    val = None
    if k not in xt or xt[k] is None:
        if headerdic[k]['type'] in ['int', 'long', 'float']:  # True
            xt[k] = -1
        else:
            xt[k] = ' - '
    if headerdic[k]['type'] in ['bool']:
        val = str(xt[k])
    else:
        if headerdic[k]['type'] == 'int':
            val = int(xt[k])
        elif headerdic[k]['type'] == 'float':
            val = float(xt[k])
        elif headerdic[k]['type'] == 'long':
            val = int(xt[k])
        else:
            if type(xt[k]) is datetime:
                val = datetime.strftime(xt[k], '%Y-%m-%d %H:%M:%S')
            elif type(xt[k]) is dict:
                # Create an empty string
                itemstr = ""
                # Convert the dictionary to a string
                # using for loop only
                for key, value in dict(xt[k]).items():
                    _v = 'none'
                    if type(value) is datetime:
                        _v = datetime.strftime(value, '%Y-%m-%d %H:%M:%S')
                    else:
                        _v = value
                    itemstr = f'{itemstr} {key}:{_v}'
                val = itemstr
            else:
                val = xt[k]
    return val


def prettyprint(format, headerdic, cdata, mw=220, color='green'):
    try:
        x = BeautifulTable(maxwidth=mw)
        x.set_style(BeautifulTable.STYLE_MARKDOWN)
        fmt = format
        if len(format) == 0:
            fmt = headerdic.keys()
        colheadfmt = []
        for a in fmt:
            colheadfmt.append(colored(a, color))
        x.columns.header = colheadfmt
        if cdata is None or len(cdata) == 0:
            nx = BeautifulTable(maxwidth=mw)
            nx.rows.append([i for i in colheadfmt])
            print(nx)
        else:
            for obj in cdata:
                xt = obj.to_dict()
                line = []
                for k in fmt:
                    #print(f'Parse element {k} {xt[k]} using {headerdic}')
                    val = parse_dictionary(k, xt, headerdic)
                    #print(f'Value parsed is {val}')
                    line.append(val)
                x.rows.append(line)
            x.columns.alignment = BeautifulTable.ALIGN_RIGHT
            print(x)
    except Exception as e:
        print(f'Cannot print using format {format}')


def print_help(data_type):
    if (data_type == 'tags'):
        print(f'Parameters for {data_type} are: {tagfieldsdicheader.keys()}')
    elif (data_type == 'opticallines'):
        print(f'Parameters for {data_type} are: {optlinefieldsdicheader.keys()}')
    elif (data_type == 'iovs'):
        print(f'Parameters for {data_type} are: {iovfieldsdicheader.keys()}')
    elif (data_type == 'corrections'):
        print(f'Parameters for {data_type} are: {corrfieldsdicheader.keys()}')
    elif (data_type == 'fitsteps'):
        print(f'Parameters for {data_type} are: {stepsfieldsdicheader.keys()}')
    elif (data_type == 'fitpulls'):
        print(f'Parameters for {data_type} are: {pullsfieldsdicheader.keys()}')
    elif (data_type == 'cooltags'):
        print(f'Parameters for {data_type} are: {cooltagfieldsdicheader.keys()}')
    elif (data_type == 'sagittas'):
        print(f'Parameters for {data_type} are: {sagittasfieldsdicheader.keys()}')
    elif (data_type == 'imtags'):
        print(f'Parameters for {data_type} are:  {imtagfieldsdicheader.keys()}')
    elif (data_type == 'imiovs'):
        print(f'Parameters for {data_type} are:  {imiovfieldsdicheader.keys()}')
    else:
        print('Cannot find help for this kind of data_type')


def print_corrections():
    print('List of possible correction files and keys:')
    print('alines=<the alines file name>')
    print('blines=<the blines file name>')
    print('fitsteps=<the fitsteps file name>')
    print('fitpulls=<the fitpulls file name>')
    print('badsens=<the bad sensors file name>')
    print('sagittas=<the sagittas file name>')


def aramys_readout_format(image_name, image, image_table):
    if image_table == "B":
        lines = "{name} {x} {y} {mag} {stime}\n#DQMR {name} {bpx} px {bin} in {bse} se {bth} th {bex} ex {bav} av {bst} st {bmx} mx {bmn} mn {bec} ec".format(
                    name=image_name,
                    x=image["x"],
                    y=image["y"],
                    mag=image["mag"],
                    stime=image["id"]["stime"],
                    bpx=int(image["bpx"]),
                    bin=int(image["bin"]),
                    bse=image["bse"],
                    bth=int(image["bth"]),
                    bex=int(image["bex"]),
                    bav=int(image["bav"]),
                    bst=int(image["bst"]),
                    bmx=int(image["bmx"]),
                    bmn=int(image["bmn"]),
                    bec=int(image["bec"])
                )
    if image_table == "R":
        lines = "{name} {x} {y} {mag} {stime}\n#DQMR {name} {rdm} dm {rro} ro {rdx} dx {rsq} sq {ror} or {rrf} rf {rex} ex {rav} av {rst} st {rmx} mx {rmn} mn {rec} ec".format(
                    name=image_name,
                    x=image["x"],
                    y=image["y"],
                    mag=image["mag"],
                    stime=image["id"]["stime"],
                    rdm=f'{image["rdm"]:.8f}', # suppress scientific notation
                    rro=image["rro"],
                    rdx=int(image["rdx"]),
                    rsq=int(image["rsq"]),
                    ror=int(image["ror"]),
                    rrf=int(image["rrf"]),
                    rex=int(image["rex"]),
                    rav=int(image["rav"]),
                    rst=int(image["rst"]),
                    rmx=int(image["rmx"]),
                    rmn=int(image["rmn"]),
                    rec=int(image["rec"])
                )
    if image_table == "T":
        lines = "{name} {tsd} {tec} {temp} {stime}\n#DQMR {name} {tec} ec".format(
                    name=image_name,
                    tsd=image["tsd"],
                    tec=int(image["tec"]),          
                    temp=image["temp"],
                    stime=image["id"]["stime"]
                )




    return lines


def dump_clob(filename, rows=[]):
    with open(filename, 'w') as f:
        for row in rows:
            f.write(row)
            f.write('\n')
    f.close()


def read_fitpulls(filename):
    import re
    headers = ['olname', 'x_pull', 'y_pull', 'z_pull']
    with open(filename) as f:
        lines = f.readlines()
        # If our first line is the header then skip it, if not then read all lines
        if lines[0].split() == headers:
            lines = lines[1:]
        alldic = []
        for row in lines:
            fields = re.split('\s+', row)
            dictionary = dict(zip(headers, fields))
            for k in dictionary.keys():
                dictionary[k] = convert_fp_tonumber(dictionary[k].strip(), k)
            alldic.append(dictionary)
    f.close()
    return alldic


def read_fitsteps(filename):
    import re
    headers = ['chi_square_ov_dof', 'chi_square', 'dof', 'prob', 'sequence_number', 'fit_description']
    with open(filename) as f:
        lines = f.readlines()
        # If our first line is the header then skip it, if not then read all lines
        if lines[0].split() == headers:
            lines = lines[1:]
        alldic = []
        for row in lines:
            # Have to do some weird string manipulation to get the required fields
            fragmented_fields = re.split(r'[\s%/()#]+', row.lstrip())
            fit_desc = [' '.join(fragmented_fields[5:])]
            fields = fragmented_fields[0:5] + fit_desc
            dictionary = dict(zip(headers, fields))
            for k in dictionary.keys():
                dictionary[k] = convert_fs_tonumber(dictionary[k].strip(), k)
            alldic.append(dictionary)
    f.close()
    return alldic


def read_badsensors(filename):
    import re
    headers = ['sensor_failure', 'sensor_total', 'sensor_type', 'rate_good']
    with open(filename) as f:
        lines = f.readlines()
        # If our first line is the header then skip it, if not then read all lines
        if lines[0].split() == headers:
            lines = lines[1:]
        alldic = []
        for row in lines:
            # Have to do some weird string manipulation to get the required fields
            # Unfortunately the sensor_type sometimes has special characters that we want to keep so we can't use the regular expression method to filter out these special characters
            fragmented_fields = re.split(r'[\s%]+', row.lstrip())
            for i in (fragmented_fields[:11]):
                if i in ["/", "(", ")", "bad", "total", "good"]:
                    fragmented_fields.remove(i)
            fields = fragmented_fields[:4]
            dictionary = dict(zip(headers, fields))
            for k in dictionary.keys():
                dictionary[k] = convert_bs_tonumber(dictionary[k].strip(), k)
            alldic.append(dictionary)
    f.close()
    return alldic


def read_A_lines(filename):
    print(f'read lines from input file {filename}')
    headers = ['corrDescription', 'typ', 'jff', 'jzz', 'job', 'svalue', 'zvalue', 'tvalue', 'tsv', 'tzv', 'ttv', 'hwElement']
    with open(filename) as f:
        lines = f.readlines()
        # If our first line is the header then skip it, if not then read all lines
        if lines[0].split() == headers:
            lines = lines[1:]
        alldic = []
        for row in lines:
            fields = row.split()
            dictionary = dict(zip(headers, fields))
            alldic.append(dictionary)
            print(f'{dictionary}')
    f.close()
    nrows = len(alldic)
    print(f'Found {nrows} A corrections')
    return alldic

def read_B_lines(filename):
    print(f'read lines from input file {filename}')
    headers = ['corrDescription', 'typ', 'jff', 'jzz', 'job', 'bz', 'bp', 'bn', 'sp', 'sn', 'tw', 'pg', 'tr', 'eg', 'ep', 'en', 'hwElement']
    with open(filename) as f:
        lines = f.readlines()
        # If our first line is the header then skip it, if not then read all lines
        if lines[0].split() == headers:
            lines = lines[1:]
        alldic = []
        for row in lines:
            fields = row.split()
            dictionary = dict(zip(headers, fields))
            alldic.append(dictionary)
            print(f'{dictionary}')
    f.close()
    nrows = len(alldic)
    print(f'Found {nrows} B corrections')
    return alldic


def print_corrections_clob(cdata=None, filename='out.txt'):
    retrows = []
    ldic = corrfieldsdicheaderamdb
    format = list(ldic.keys())
    format.remove('corrId')
    format.remove('corrDescription')
    headerfmtstr = ' '.join([ldic[k]['key'] for k in format])
    headic = {}
    for k in format:
        headic[k] = ldic[k]['val']
    print(f'Print header format string:')
    print(headerfmtstr.format(**headic))
    fmtstr = ' '.join([ldic[k]['field'] for k in format])
    print(f'....{fmtstr}')
    for xt in cdata:
        adic = {}
        for k in format:
            ###print(f'Check {k} in {xt}')
            if k not in xt or xt[k] is None:
                ##print(f'WARN: {k} is not in cdata')
                if 'default' in ldic[k]:
                    xt[k] = ldic[k]['default']
                elif ldic[k]['type'] in ['int', 'long', 'float']:  # True
                    xt[k] = -1
                else:
                    xt[k] = ' - '
            if ldic[k]['type'] in ['bool']:
                adic[k] = str(xt[k])
            else:
                adic[k] = xt[k]
        ##print('Use dictionary %s'%adic)
        abline = fmtstr.format(**adic)
        retrows.append(f'Corr: {abline}')
    return retrows


def merge_corrections(dictarrA, dictarrB):
    newdictarrAB = []
    for al in dictarrA:
        print(f'Merge element in A lines : {al}')
        dictAB = {'corrDescription': 'AB'}
        for akeys in al.keys():
            if akeys not in ['corrDescription']:
                dictAB[akeys] = convert_corr_tonumber(al[akeys], akeys)
        bel = get_hwelement(dictarrB, al['hwElement'], al['job'])
        print(f'...check B lines : {bel}')
        # suppose that only A lines were provided, we should still upload them
        if bel is None:
            bel = {'bz': 0, 'bp': 0, 'bn': 0, 'sp': 0, 'sn': 0, 'tw': 0, 'pg': 0, 'tr': 0, 'eg': 0,
                   'ep': 0, 'en': 0}
        for bkeys in bel.keys():
            if bkeys not in ['corrDescription', 'typ', 'jff', 'jzz', 'job', 'hwElement']:
                dictAB[bkeys] = convert_corr_tonumber(bel[bkeys], bkeys)
        # add missing fields
        dictAB['xatlas'] = -999.
        dictAB['yatlas'] = -999.
        dictAB['hwType'] = 'none'
        dictAB['corrId'] = None
        dictAB['aligniov'] = None
        newdictarrAB.append(dictAB)
    nrows = len(newdictarrAB)
    first_row = newdictarrAB[0]
    last_row = newdictarrAB[len(newdictarrAB)-1]
    print(f'Merged corrections of length {nrows}, first row: {first_row} ; last row: {last_row}')
    return newdictarrAB


def get_hwelement(dictarr, element, job):
    for el in dictarr:
        if el['hwElement'] == element and el['job'] == job:
            return el
    return None


def convert_fs_tonumber(val, element):
    if element not in ['fit_description']:
        if element not in ['sequence_number', 'dof']:
            return float(val)
        return int(val)
    return val


def convert_fp_tonumber(val, element):
    if element not in ['olname']:
        if element not in ['iovid']:
            return float(val)
        return int(val)
    return val


def convert_bs_tonumber(val, element):
    if element not in ['sensor_type']:
        if element not in ['sensor_failure', 'sensor_total']:
            return float(val)
        return int(val)
    return val


def convert_corr_tonumber(val, element):
    if element not in ['corrDescription', 'typ', 'hwElement']:
        if element not in ['jff', 'jzz', 'job']:
            return float(val)
        return int(val)
    return val


def clob_head():
    ch = """
#: Header line is since till chi2,
#:                           chi2norm,
#:                           runstart, runend, dof,
#:                           inpBad,  praBad,  axiBad,  projBad,      * bad sensors (per optical line type: this should be 
#:                           refBad,  cccBad,  brmBad,                * different for barrel end endcap) 
#:                           inpAll, praAll,  axiAll,  projAll,       * Number of sensors (Good+Bad) 
#:                           refAll, cccAll,  brmAll,  
#:                           icarasSince,  icarasTill,                * optical line data intervals and measurement cycle 
#:                           cycleSince,  cycleTill.......
#: Corr line is counter typ,  jff,  jzz, job,                         * Chamber information 
#:                       svalue,  zvalue, tvalue,  tsv,  tzv,  ttv,   * A lines 
#:                       bz, bp, bn, sp, sn, tw, pg, tr, eg, ep, en   * B lines 
#:                       chamber                                      * Chamber name 
IOV: -1
HFS: | 2018/12/05 10:09:00 | 2018/12/05 12:09:00 |    0.24963 | 4515.04969 |  18087 |   0.00000 |      1 | asap fit |
HBS: | PRA  99.66571   2094      7 
HBS: | CCC  99.23077    260      2 
HBS: | BRM  96.87500     32      1 
HBS: | REF  99.60938    256      1 
HBS: | INP  77.12264   2120    485 
HBS: | TSENSOR  98.94463   7675     81 
HBS: | AXI  99.06015   1064     10 
HBS: | PRO  99.14530    117      1 
    """
