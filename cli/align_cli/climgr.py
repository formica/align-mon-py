import sys
from datetime import datetime
import pytz
from pytz import timezone
import os
# Log
import logging
from align_cli.log_setup import setup_logger
import openapi_client
from .api_call import ApiCall
from .alignutils import *
from .kc_tokens import KcApiTokens, KcExchangeTokens, AuthorizationConfig

log = logging.getLogger("aligncli.climgr")


# log.info('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__,
#                                                                          str(__package__)))


def convert_time_str(st, tt):
    since = None
    until = None
    v_info = sys.version_info
    log.info(f'Check python version {v_info}')
    if v_info[1] < 7:
        fmt_str = r"%Y-%m-%d %H:%M:%S"  # replaces the fromisoformatm, not available in python 3.6
        if st is not None:
            since = datetime.strptime(st, fmt_str).astimezone(pytz.utc)
        if tt is not None:
            until = datetime.strptime(tt, fmt_str).astimezone(pytz.utc)
    else:
        if st is not None:
            since = datetime.fromisoformat(st).astimezone(pytz.utc)
        if tt is not None:
            until = datetime.fromisoformat(tt).astimezone(pytz.utc)
        #since = datetime.strptime(st, '%Y-%m-%d %H:%M:%S')
        #until = datetime.strptime(tt, '%Y-%m-%d %H:%M:%S')
    return (since, until)

def format_ecol_time_str(datetime):
    # Need to bug test how find_ecols() reacts to receiving only one time stand instead of two
    # If it breaks without two timestamps, should add some clause to this function to avoid it breaking, or add an error message response to do_ls()
    # Need to retest if find ecols can use the regular time function now that the regular one has the timezone added properly
    if datetime is not None:
        time_out = datetime.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    return time_out

def init_tokens():
    auth_config = AuthorizationConfig.from_env()
    if auth_config.kc_type == 'exchange':
        # Use exchange token
        tok_ex = KcExchangeTokens(auth_config=auth_config)
        log.info(f'Token exchange obj is {tok_ex}')
        tok_ex.get_exchange_token()
        # Now the token has been retrieved, switch to API mode
        auth_config.kc_type = "api"

    tok = KcApiTokens(
        max_tries=1,
        backoff_factor=1,
        auth_config=auth_config
    )
    if tok is not None:
        # If use_tokens is True, automatically request Keycloak
        # for tokens, and format the Authorization header with
        # Keycloak response.
        log.info(f'Get headers from token {tok}')
        auth_headers = tok.get_authorization_header()
        log.info(f'Retrieved headers...{auth_headers}')
        if len(auth_headers['Authorization']) < 10:
            raise ValueError('Cannot get correct token')
    return tok


class AlignCli(object):

    def __init__(self, server_url=None, use_tokens=True):
        if server_url is None:
            server_url = 'http://localhost:8080/api'

        self.cm = None
        self._config = None
        self._api = None
        self.host = server_url
        self._use_tokens = use_tokens
        self._access_token = None
        loc_parser = None
        import re
        self.rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        self.rr = re.compile(self.rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None, verify_ssl=True):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        if self._use_tokens:
            tok = init_tokens()
            self._access_token = tok.get_access_token()
            log.info(f'Retrieved token...{self._access_token}')

        self._config = openapi_client.Configuration(host=self.host, access_token=self._access_token)
        if not verify_ssl:
            self._config.verify_ssl = verify_ssl
        self._api = openapi_client.ApiClient(self._config)
        self.cm = ApiCall(self._api)
        log.info(f'CLI is connected to {url}')

    def do_dump(self, args=None):
        """dump <datatype> [-i iovid]
        Dump data for a given iovid: iovs.
        datatype: iovs.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'iovs'
        tagname = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        if args:
            log.info(f'Received args {args}')
            if 'type' in args:
                cmd = args['type']
            log.info(f'Searching for data of type {cmd}')
            if 'iovid' in args:
                iovid = args['iovid']
        if cmd == 'iovs':
            if iovid is None:
                log.error('Cannot dump iov {iovid}')
                return
            log.info(f'Get iov using {iovid}')
            out = self.cm.get_iov_file(iovid=iovid, format='COOL')
            print(f'Found file {out}')
            return out
        return None

    def do_ls(self, args=None):
        """ls <datatype> [-t tag_name] [--olname opticalline_name] [other options: --size, --page, --sort, --format]
        Search for data collection of different kinds: opticallines, tags.
        datatype: opticallines, iovs, tags.
        Type ls -h for help on available options (not all will be appliable depending on the chosen datatype)
        """
        out = None
        cmd = 'tags'
        tagname = None
        tagtype = None
        cooltagname = None
        optline = '%'
        iovid = None
        cdic = {}
        pdic = {}
        since = None
        until = None
        oltype = None
        sensors = None
        olproblem = None
        try:
            if args:
                log.info(f'Received args {args}')
                if 'data' in args:
                    cmd = args['data']
                log.info(f'Searching for data of type {cmd}')
                if 'tag' in args:
                    tagname = args['tag']
                if 'type' in args:
                    tagtype = args['type']
                if 'cooltag' in args:
                    cooltagname = args['cooltag']
                if 'olname' in args:
                    optline = args['olname']
                if 'olproblem' in args:
                    olproblem = args['olproblem']
                if 'iovid' in args:
                    iovid = args['iovid']
                if 'oltype' in args:
                    oltype = args['oltype']
                if 'sensors' in args:
                    sensors = args['sensors']
                # Init general sort, since, until only if they are present
                sort = None
                if 'sort' in args:
                    sort = args['sort']
                page = args['page']
                size = args['size']
                if 'since' in args and 'until' in args:
                    (since, until) = convert_time_str(args['since'], args['until'])
                if 'cut' in args and args['cut'] is not None:
                    cutstringarr = args['cut'].split(',')
                    for el in cutstringarr:
                        ss = self.rr.findall(el)
                        (k, v) = el.split(ss[0])
                        cdic[k] = f'{ss[0]}{v}'
                    log.info('use cut params : %s' % cdic)

                if 'params' in args and args['params'] is not None:
                    pararr = args['params'].split(',')
                    for par in pararr:
                        kv = par.split('=')
                        pdic[kv[0]] = kv[1]
                fields = []
                if 'fields' in args:
                    log.debug('fields is %s' % args.get('fields'))
                    if 'none' == args['fields']:
                        fields = []
                    else:
                        fields = args.get('fields').split(',')

                # Check the data type and search
                if cmd == 'tags':
                    if tagname is None:
                        tagname = "%"
                    # Use the params option to get further arguments.
                    log.info(f'Search tags using {tagname} and {tagtype}')
                    out = self.cm.find_aligntags(tag_type=tagtype, tag_pattern=tagname)
                    out['format'] = 'AligntagSetDto'

                elif cmd == 'cooltags':
                    cdic = {}
                    log.info(f'Search cool tags  using {cooltagname}')
                    if cooltagname is not None:
                        if '%' in cooltagname:
                            cdic['cool_tag_like'] = cooltagname
                        else:
                            cdic['cool_tag'] = cooltagname

                    if tagname is not None:
                        if '%' in tagname:
                            cdic['align_tag_like'] = tagname
                        else:
                            cdic['align_tag'] = tagname
                    out = self.cm.find_align_cooltag(**cdic)
                    out['format'] = 'AlignCoolTagSetDto'

                elif cmd == 'ecols':
                    log.info(f'Search ecols...')
                    cargs = {}
                    # this other args are not implemented at the moment and would need to be added to api_call
                    #cargs['page'] = page
                    #cargs['size'] = size
                    #cargs['sort'] = sort
                    if since:
                        since = format_ecol_time_str(since)
                    if until:
                        until = format_ecol_time_str(until)
                    if oltype is not None:
                        if oltype == "B":
                            oltype = "AlB"
                        if oltype == "R":
                            oltype = "AlR"
                        if oltype == "T":
                            oltype = "AlT"
                        if oltype == "D":
                            oltype = "AlD"
                        cargs["oltype"] = oltype
                    if sensors is not None:
                        cargs["sensors"] = sensors
                    out = self.cm.find_ecols(since=since, until=until, **cargs)
                    # this print should eventually be removed when the server print formatting is set up for ecols 
                    print(f'Received : {out}')
                    out['format'] = 'AlignMonBaseResponse'

                elif cmd == 'ecol_map':
                    log.info(f'Searching for ecol map...')
                    if oltype is None:
                        oltype = "B"
                    if sensors is None:
                        sensors = "%"
                    out = self.cm.find_ecol_map(image=oltype,sensor=sensors)
                    # this print should eventually be removed when the server print formatting is set up for ecols 
                    print(f'Received : {out}')
                    out['format'] = 'AlignMonBaseResponse'

                elif cmd == 'iovs':
                    log.info(f'Search iovs using {tagname}')
                    cargs = {}
                    if tagname is None:
                        log.error('Cannot search iovs without a tagname specified')
                        return
                    if sort is None:
                        sort = 'sinceT:ASC'
                    if since:
                        cargs['since'] = since
                    if until:
                        cargs['till'] = until
                    cargs['page'] = page
                    cargs['size'] = size
                    cargs['sort'] = sort
                    out = self.cm.find_align_iovs(tag=tagname, **cargs)
                    out['format'] = 'AligniovSetDto'

                elif cmd == 'corrections':
                    # Need to come back to these ls commands later and properly introduce keyword parameters to the 
                    # api call. Args to include: since/till, sort, size, page

                    if iovid is not None:
                        log.info(f'Search corrections using iovid {iovid} ')
                    # elif since is not None and until is not None:
                    #     log.info(f'Search corrections using {since} and {until}')
                    # if sort == None:
                    #     sort = "hwElement:ASC"
                    out = self.cm.find_corrections(iovid=iovid)
                    out['format'] = 'AligncorrectionsSetDto'

                elif cmd == 'fitsteps':
                    if iovid is not None:
                        log.info(f'Search fit steps using iovid {iovid} ')
                    # if sort == None:
                    #     sort = "fitId:ASC"
                    # cdic['iovId'] = iovid
                    out = self.cm.find_fitsteps(iovid=iovid)
                    out['format'] = 'AlignFitStepsSetDto'

                elif cmd == 'fitpulls':
                    if iovid is not None:
                        log.info(f'Search sensor pulls using iovid {iovid} ')
                    # if sort == None::
                    #     sort = "pk.olname:ASC"
                    # cdic['iovId'] = iovid
                    # cdic['tagname'] = tagname
                    out = self.cm.find_fitpulls(iovid=iovid)
                    # out = self.cm.find_fitpulls(page=args['page'], size=args['size'], sort=sort, **cdic)
                    out['format'] = 'AlignFitPullsSetDto'
                
                elif cmd == 'badsensors':
                    if iovid is not None:
                        log.info(f'Search bad sensors using iovid {iovid}')
                    out = self.cm.find_badsensors(iovid=iovid)
                    out['format'] = 'AlignBadSensorsSetDto'
                    
                elif cmd == 'opticallines':
                    log.info(f'Search opticallines by name using {optline}')
                    out = self.cm.find_opticallines_by_name(name=optline)
                    out['format'] = 'OpticallinesSetDto'
                
                elif cmd == 'opticallinesproblems':
                    log.info(f'Search opticallines by problem using {olproblem}')
                    out = self.cm.find_opticallines_by_problem(qtype=olproblem)
                    out['format'] = 'OpticallinesSetDtoProblems'
                
                elif cmd == 'imtags':
                    log.info(f'Search intervalmaker tags using {tagname}')
                #                out = self.cm.search_im_tags(tagname=tagname)
                #                out['format'] = 'IntervalMakerTagSetDto'
                
                elif cmd == 'imconfigs':
                    log.info(f'Search intervalmaker configurations using {tagname}')
                #                out = self.cm.search_im_configurations(tagname=tagname)
                #                out['format'] = 'IntervalMakerConfigurationSetDto'
                
                elif cmd == 'iovsmigration':
                    log.info(f'Search iovs to migrate to cool using {tagname} and {cooltagname}')
                    out = self.cm.find_iovs_to_migrate(align_tag=tagname, cool_tag=cooltagname)
                    out['format'] = 'AligniovSetDto'
                
                elif cmd == 'imiovs':
                    log.info(f'Search intervalmaker iovs using {tagname}')
                #                out = self.cm.search_im_iovs(mode=args['mode'], tagname=tagname, since=cdic['since'],
                #                                             until=cdic['until'], fmt='iso')
                #                out['format'] = 'IntervalMakerIovSetDto'
                else:
                    log.error(f'Command {cmd} is not recognized in this context')
            server_print(out, format=fields)
            return out
        except openapi_client.ApiException as e:
            log.error(f'Response from server is not 200')

    def do_amdb(self, args):
        """amdb
         Provide help for k=val pairs to be used in amdb
         """
        if args:
            log.info(args)
            out = None
            cmd = args['cmd']
            if cmd == 'amdb':
                amdbdic = {}
                if 'amdb' in args and args['amdb'] is not None:
                    amdbarr = args['amdb'].split(',')
                    for par in amdbarr:
                        kv = par.split('=')
                        log.info(f'par= {par} kv= {kv}')
                        amdbdic[kv[0]] = kv[1]
                    
                    dataA = None
                    dataB = None
                    if 'alines' in amdbdic and amdbdic['alines'] is not None:
                        dataA = read_A_lines(amdbdic['alines'])
                    if 'blines' in amdbdic and amdbdic['blines'] is not None:
                        dataB = read_B_lines(amdbdic['blines'])
                    if dataA is not None and dataB is not None:
                        ii = int(0)
                        corr_ab = merge_corrections(dataA, dataB)
                        for line in corr_ab:
                            line['corrId'] = int(ii)
                            line['corrDescription'] = 'AB'
                            ii += 1
                        log.info(f'Creating new set of corrections as blob list of size {len(corr_ab)}')
                        out = print_corrections_clob(corr_ab)
                else:
                    log.warning('Cannot create object without arguments')
                dump_clob('out.txt', out)
        log.info(f'Dump clob in out.txt')

    def upload_corrections(self, iovid, corrdic):
        dataA = None
        dataB = None
        dataFS = None
        dataFP = None
        dataBS = None
        if 'alines' in corrdic and corrdic['alines'] is not None:
            dataA = read_A_lines(corrdic['alines'])
        if 'blines' in corrdic and corrdic['blines'] is not None:
            dataB = read_B_lines(corrdic['blines'])
        if 'fitsteps' in corrdic and corrdic['fitsteps'] is not None:
            dataFS = read_fitsteps(corrdic['fitsteps'])
        if 'fitpulls' in corrdic and corrdic['fitpulls'] is not None:
            dataFP = read_fitpulls(corrdic['fitpulls'])
        if 'badsensors' in corrdic and corrdic['badsensors'] is not None:
            dataBS = read_badsensors(corrdic['badsensors'])
        if dataA is not None:
            corr_ab = merge_corrections(dataA, dataB)
            log.info(
                f'Creating new set of corrections for iov {iovid} and corrections list of size {len(corr_ab)}')
            out = self.cm.create_iov_aligncorrections(iovid=int(iovid), corrections=corr_ab)
        if dataFS is not None:
            log.info(
                f'Creating new set of fitsteps for iov {iovid} and fitsteps list of size {len(dataFS)}')
            out = self.cm.create_iov_fitsteps(iovid=int(iovid), corrections=dataFS)
        if dataFP is not None:
            log.info(
                f'Creating new set of fitpulls for iov {iovid} and fitpulls list of size {len(dataFP)}')
            out = self.cm.create_iov_fitpulls(iovid=int(iovid), corrections=dataFP)
        if dataBS is not None:
            log.info(
                f'Creating new set of badsensors for iov {iovid} and badsensors list of size {len(dataBS)}')
            out = self.cm.create_iov_fitbadsensors(iovid=int(iovid), corrections=dataBS)
        return out

    def do_update(self, args):
        """update <datatype> k=v,k1=v1,....
        Update iov using a list of k=val pairs, separated
        by commas
        """
        out = None
        cmd = None
        pdic = {}
        tname = None
        cooltname = None
        iovid = None
        start_status = None
        end_status = None
        since = None
        until = None
        dto_name = ''
        if args:
            if 'data' in args:
                cmd = args['data']
            if 'tag' in args and cmd in ['tags']:
                tname = args['tag']
            if 'iovid' in args and cmd in ['iovs']:
                iovid = args['iovid']
            if 'since' in args and 'until' in args:
                (since, until) = convert_time_str(args['since'], args['until'])
            # Check if the user needs hints on fields
            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
                if "start_status" in pdic:
                    start_status = pdic["start_status"]
                if "end_status" in pdic:
                    end_status = pdic["end_status"]
            else:
                self.cm.help_params(dto_name)
                return

            if cmd == 'iovs':
                # the default for reco_status
                log.info(f'Update iov id {iovid} '
                         f'using args {pdic}')
                out = self.cm.update_aligniov(iov_id=int(iovid), **pdic)
                log.info(f'Update iov : {out}')

            if cmd == 'tags':
                if tname is None:
                    log.error('No tag name given')
                    return

                find_iov_args = {"sort": "sinceT:ASC"}
                if since is not None:
                    find_iov_args["since"] = since
                if until is not None:
                    find_iov_args["till"] = until
                tag_iovs = self.cm.find_align_iovs(tag=tname, **find_iov_args)
                log.info(f"{args}\nFor tag: {tname}, converting iovs with {start_status} to {end_status}")
                for iov in tag_iovs["resources"]:
                    iov = iov.to_dict()
                    iovid = int(iov["iov_id"])
                    iov_status = iov["reco_status"]
                    if iov_status == start_status:
                       log.info(f'Converting iov {iovid} reco status from {iov_status} to {end_status}.')
                       out = self.cm.update_aligniov(iov_id=int(iovid), **{"reco_status": end_status})
                    elif iov_status in ["BAD", end_status]:
                       log.info(f'Passing over iov {iovid} with reco status {iov_status}.')
                       pass
                    else:
                       log.info(f'Encountered iov {iovid} with reco status {iov_status}. Stopping reco status conversion')
                       return
                log.info("Finished updating iov reco status")







    def do_delete(self, args):
        """update <datatype> k=v,k1=v1,....
        Update iov using a list of k=val pairs, separated
        by commas
        """
        out = None
        cmd = None
        pdic = {}
        iovid = None
        if args:
            if 'data' in args:
                cmd = args['data']
            if 'iovid' in args and cmd in ['iovs']:
                iovid = args['iovid']
            # Check if the user needs hints on fields

            if cmd == 'iovs':
                # the default for reco_status
                log.info(f'Delete iov id {iovid}')
                out = self.cm.delete_aligniov(iovid=iovid)
                log.info(f'Delete iov : {out}')

    def do_upload(self, args):
        """upload <iovs> iovid
        Upload the specified iovid into COOL
        """
        out = None
        cmd = None
        iovid = None
        cooltag = None
        pdic = {}
        if args:
            if 'data' in args:
                cmd = args['data']
            if 'iovid' in args and cmd in ['iovs']:
                iovid = args['iovid']
            if 'cooltag' in args:
                cooltag = args['cooltag']
            if 'params' in args and args['params'] is not None:
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
            if 'method' in pdic.keys():
                _m = pdic['method']
                log.info(f'Use request in mode : {_m}')
            # Check if the user needs hints on fields

            if cmd == 'iovs':
                # the default for reco_status
                log.info(f'Upload to COOL  iov id {iovid}')
                out = self.cm.upload_to_cool(iovid=iovid, cooltag=cooltag, **pdic)
                log.info(f'Upload to COOL response : {out}')

    def do_create(self, args):
        """create <datatype> k=v,k1=v1,....
        Create a new tag or cool tag, using a series of k=val pairs, separated
        by commas
        """
        out = None
        cmd = None
        pdic = {}
        corrdic = {}
        tname = None
        reco_status = None
        cooltname = None
        iovid = None
        dto_name = ''
        if args:
            if 'data' in args:
                cmd = args['data']
            if cmd == 'tags':
                dto_name = 'AligntagDto'
            elif cmd == 'iovs':
                dto_name = 'AligniovDto'
            log.info(f'Creating object for type {cmd}')
            if 'tag' in args and cmd in ['tags', 'iovs', 'cooltags', 'imconfigs', 'imiovs',
                                         'imtags']:
                tname = args['tag']
            if 'cooltag' in args and cmd in ['cooltags', 'cooliovs']:
                cooltname = args['cooltag']
            if 'iovid' in args and cmd in ['iovs', 'corrections']:
                if args['iovid'] is not None:
                    iovid = int(args['iovid'])
                else:
                    iovid = args['iovid']
            # Check if the user needs hints on fields
            if 'params' in args and args['params'] is not None:
                if args['params'] == 'help':
                    self.cm.help_params(dto_name)
                    return
                pararr = args['params'].split(',')
                for par in pararr:
                    kv = par.split('=')
                    pdic[kv[0]] = kv[1]
                if 'reco_status' in pdic:
                    reco_status = pdic['reco_status']
            # Check if the user needs hints on fields
            if 'corrections' in args and args['corrections'] is not None:
                corrarr = args['corrections'].split(',')
                for par in corrarr:
                    kv = par.split('=')
                    corrdic[kv[0]] = kv[1]
            if cmd == 'tags':
                log.info(f'Creating tag {tname} and args {pdic}')
                if tname is None:
                    log.error(f"Cannot create tag without the name provided via -t or --tag")
                    return
                out = self.cm.create_aligntag(tag=tname, tag_type=pdic['tag_type'],
                                              tag_description=pdic['tag_description'],
                                              tag_author=pdic['tag_author'])
            elif cmd == 'cooltags':
                log.info(
                    f'Creating cooltag {cooltname} associate to align tag {tname} and args {pdic}')
                if cooltname is None or tname is None:
                    log.error(
                        f"Cannot create cooltag without the name of a cool and align tags provided via --tag and --cooltag")
                    return
                out = self.cm.create_align_cooltag(tag=tname, cooltag=cooltname,
                                                   tag_comment=pdic['tag_comment'],
                                                   cherrypy_config=pdic['cherrypy_config'],
                                                   tag_type=pdic['tag_type'])
            #                out = self.cm.create_cooltags(name=cooltname, aligntag=tname, **pdic)
            elif cmd == 'iovs':
                since = None
                until = None
                if 'since' in args and 'until' in args:
                    if args['since'] == "previous":
                        find_args = {"sort": "tillT:DESC"}
                        out = self.cm.find_align_iovs(tag=tname, **find_args)
                        # Need to do these time conversions because find_align_iovs() returns timestamp in UTC while we need to write to DB in Geneva time
                        # This is bad code. The smart thing to do would be to write timestamps in UTC for consistency, but this would involve changing and potentially breaking a lot of things
                        # IMPORTANT NOTE: the astimezone() method doesn't work so well for timezones far in the future (i.e. I tested it for 2060 and it broke).
                        geneva_tz = timezone("Europe/Zurich")
                        since = out["resources"][0]["till_t"].astimezone(geneva_tz).strftime("%Y-%m-%d %H:%M:%S")
                        args['since'] = since
                    (since, until) = convert_time_str(args['since'], args['until'])
                    if since == until:
                        log.error(f'Error with creating iov for given since/until; cannot create iovs with since = until.\nsince: {since}, until: {until}')
                        return 
                log.info(f'Creating new iov for tag {tname} time {since} '
                         f'and {until} and args {pdic}')
                if 'fit_status' not in pdic:
                    pdic['fit_status'] = 0
                out = self.cm.create_aligniov(iov_tag=tname, since=since, until=until,
                                              reco_status='UPLOADING',
                                              fit_status=pdic['fit_status'])
                log.info(f'Created iov {out}')
                _iovid = out['iov_id']
                if len(corrdic.keys()) > 0:
                    log.info(f'Upload corrections provided in input for iov {_iovid}')
                    out = self.upload_corrections(_iovid, corrdic)
                    log.info(f'Uploaded corrections...finished')
                    if reco_status == None:
                        reco_status = 'QC_PENDING'
                    log.info(f'Setting reco status as {reco_status} for iov {_iovid}')
                    self.cm.update_aligniov(iov_id=_iovid, reco_status=reco_status)
                else:
                    if reco_status == None:
                        reco_status = 'EMPTY'
                    log.info(f'Setting reco status as {reco_status} for iov {_iovid}')
                    self.cm.update_aligniov(iov_id=_iovid, reco_status=reco_status)
                
            elif cmd == 'corrections':
                if reco_status == None:
                    reco_status = 'QC_PENDING'
                log.info(f'Setting reco status as UPLOADING for iov {iovid}')
                self.cm.update_aligniov(iov_id=iovid, reco_status='UPLOADING')
                log.info(f'Creating corrections using {iovid} and {corrdic}')
                out = self.upload_corrections(iovid, corrdic)
                log.info(f'Uploaded corrections...finished')
                log.info(f'Setting reco status as {reco_status} for iov {iovid}')
                self.cm.update_aligniov(iov_id=iovid, reco_status=reco_status)
            
            elif cmd == 'imconfigs':
                log.info(f'Creating new IntervalMaker config with {tname} and args {pdic}')
            #                out = self.cm.create_im_conf(tagname=tname, **pdic)
            elif cmd == 'imtags':
                log.info(f'Creating new IntervalMaker tag with {tname} and args {pdic}')
            #                out = self.cm.create_im_tags(tagname=tname, **pdic)
            else:
                log.error(f'Command {cmd} is not recognized in this context')

        else:
            log.warning('Cannot create object without arguments')
        log.info(f'Response is : {out}')

    def do_convert(self, line):
        """convert date
        Convert a date to UTC unix time."""
        v_info = sys.version_info

        if v_info[1] < 7:
            fmt_str = r"%Y-%m-%dT%H:%M:%S"  # replaces the fromisoformatm, not available in python 3.6
            dt = datetime.strptime(line, fmt_str)
        else:
            dt = datetime.fromisoformat(line)
        log.info('create time from string %s %s' % (line, dt.timestamp()))
        since = int(dt.timestamp() * 1000)
        log.info(f'date {line} = {since}')

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            log.info('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            log.info('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

    def get_corrections_dict(self, corrs):
        pdic = {}
        pararr = corrs.split(',')
        for par in pararr:
            kv = par.split('=')
            pdic[kv[0]] = kv[1]
        return pdic

    def check_tagname(self, tag):
        if str(tag).startswith('EC_A'):
            return ('ENDCAP', 'SIDEA')
        elif str(tag).startswith('EC_C'):
            return ('ENDCAP', 'SIDEC')
        elif str(tag).startswith('BA_'):
            return ('BARREL', 'NONE')
        elif str(tag).startswith('TGC_A'):
            return ('TGC', 'SIDEA')
        elif str(tag).startswith('TGC_C'):
            return ('TGC', 'SIDEC')
        log.error('This tag name %s is not recognized as standard', tag)
        return None

    def create_image_id_to_name_map(self, image_table):
        out = self.cm.find_ecol_map(image=image_table, sensor="%")
        image_dict = {}
        image_list = out["resources"]
        for image in image_list:
            image_dict[image["image_id"]] = image["image_name"]
        return image_dict

    def do_aramys_readout(self, args=None):
        since = args["since"]
        until = args["until"]
        (since, until) = convert_time_str(since, until)
        since = format_ecol_time_str(since)
        until = format_ecol_time_str(until)

        sensor_type_list = ["B","R","T"]
        #with open("readout.txt", "w") as file_out:
        #    for sensor_type in sensor_type_list:
        #        image_dict = self.create_image_id_to_name_map(sensor_type)
        #        if sensor_type == "B":
        #            al_table = "AlB"
        #        if sensor_type == "R":
        #            al_table = "AlR"
        #        if sensor_type == "T":
        #            al_table = "AlT"

        #        out = self.cm.find_ecols(since=since, until=until, oltype=al_table, sensors="%")
        #        for image in out["resources"]:
        #            image_name = image_dict[image["id"]["image_id"]]
        #            lines = aramys_readout_format(image_name, image, sensor_type)
        #            file_out.write(lines)

        for sensor_type in sensor_type_list:
            image_dict = self.create_image_id_to_name_map(sensor_type)
            if sensor_type == "B":
                al_table = "AlB"
            if sensor_type == "R":
                al_table = "AlR"
            if sensor_type == "T":
                al_table = "AlT"

            out = self.cm.find_ecols(since=since, until=until, oltype=al_table, sensors="%")
            for image in out["resources"]:
                    image_name = image_dict[image["id"]["image_id"]]
                    lines = aramys_readout_format(image_name, image, sensor_type)
                    print(lines)

        log.info("Finished printing LWDAQ data")
        return
