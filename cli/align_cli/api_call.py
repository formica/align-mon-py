from datetime import datetime
import os
# Log
import logging
from align_cli.log_setup import setup_logger

import pytz

import openapi_client
from pprint import pprint
from openapi_client.api import alignconf_api
from openapi_client.api import aligntag_api
from openapi_client.api import aligniov_api
from openapi_client.api import ecol_api
from openapi_client.api import opticallines_api
from openapi_client.api import cool_api
from openapi_client.model.align_reco_conf_set_dto import AlignRecoConfSetDto
from openapi_client.model.aligntag_dto import AligntagDto
from openapi_client.model.aligntag_set_dto import AligntagSetDto
from openapi_client.model.aligncorrections_dto import AligncorrectionsDto
from openapi_client.model.align_fit_steps_dto import AlignFitStepsDto
from openapi_client.model.align_fit_pulls_dto import AlignFitPullsDto
from openapi_client.model.align_fit_bad_sensors_dto import AlignFitBadSensorsDto
from openapi_client.model.aligniov_dto import AligniovDto
from openapi_client.model.aligniov_base_dto import AligniovBaseDto
from openapi_client.model.aligniov_payload_dto import AligniovPayloadDto
from openapi_client.model.aligniov_set_dto import AligniovSetDto
from openapi_client.model.opticallines_dto import OpticallinesDto
from openapi_client.model.opticallines_set_dto import OpticallinesSetDto
from openapi_client.model.ol_incident_set_dto import OlIncidentSetDto
from openapi_client.model.ol_incident_category import OlIncidentCategory

from openapi_client.model.al_b_set_dto import AlBSetDto
from openapi_client.model.al_b_dto import AlBDto
from openapi_client.model.align_cool_tag_dto import AlignCoolTagDto

from openapi_client.model.aligniov_reco_status_enum import AligniovRecoStatusEnum
from openapi_client.model.http_response import HTTPResponse
import time


log = logging.getLogger("aligncli.api_call")


# log.info('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__, str(__package__)))


class ApiCall(object):
    """List of API methods implemented."""

    def __init__(self, api_client=None):
        self.api_client = api_client

    def find_reco_conf(self, tag=None, activated_online=None):
        # Create an instance of the API class
        api_instance = alignconf_api.AlignconfApi(self.api_client)
        activated_online = True  # bool | the flag to search tags activated or not (optional) (default to True)
        try:
            # Retrieves a reconstruction configuration based on tag name search
            api_response = api_instance.find_reco_conf(tag, activated_online=activated_online)
            pprint(api_response)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AlignconfApi->find_reco_conf: %s\n" % e)
            raise e

    def find_ecols(self, since=None, until=None, **kwargs):
        valid_fields = ['oltype','sensors']
        # Check request validity                                                                                                                     
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)
            return
        # Create an instance of the API class
        api_instance = ecol_api.EcolApi(self.api_client)
        try:
            # Finds an AlignMonBaseResponse by tag name
            if "oltype" in kwargs.keys():
                oltype = kwargs["oltype"]
            else:
                oltype = "AlB"
            if "sensors" in kwargs.keys():
                sensors = kwargs["sensors"]
            else:
                sensors = ""
            api_response = api_instance.get_al_history(since=since, till=until, image=oltype, sensors=sensors)
            # api_response = api_instance.get_images(image="B", sensor="%")
            return api_response

        except openapi_client.ApiException as e:
            print("Exception when calling EcolApi->get_al_history: %s\n" % e)
            raise e


    def find_ecol_map(self, image=None, sensor=None):
        # Create an instance of the API class
        api_instance = ecol_api.EcolApi(self.api_client)
        try:
            api_response = api_instance.get_images(image=image, sensor=sensor)
            return api_response

        except openapi_client.ApiException as e:
            print("Exception when calling EcolApi->get_images: %s\n" % e)
            raise e

    def find_aligntags(self, tag_type=None, tag_pattern=None):
        # Create an instance of the API class
        api_instance = aligntag_api.AligntagApi(self.api_client)
        try:
            # Finds an AligntagSetDto by tag name
            api_response = api_instance.find_aligntags(type=tag_type, tag_pattern=tag_pattern)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligntagApi->find_aligntags: %s\n" % e)
            raise e

    def find_align_cooltag(self, **kwargs):
        # Create an instance of the API class
        valid_fields = ['align_tag', 'align_tag_like', 'cool_tag', 'cool_tag_like']
        # check request validity
        if not set(kwargs.keys()).issubset(valid_fields):
            log.error('Requested fields should be in %s', valid_fields)
            return

        api_instance = cool_api.CoolApi(self.api_client)
        try:
            log.info(f'Search AlignCoolTags using : {kwargs}')
            api_response = api_instance.find_align_cool_tags(**kwargs)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling CoolApi->create_align_cool_tag: %s\n" % e)
            raise e

    def upload_to_cool(self, iovid=None, cooltag=None, method='DUMP'):
        # Create an instance of the API class
        api_instance = cool_api.CoolApi(self.api_client)
        try:
            log.info(f'Upload iov to COOL : {iovid}')
            cargs = {}
            if method is None:
                method = "DUMP"
            if "DUMP" in method:
                api_response = api_instance.get_request_for_cool(int(iovid), cooltag)
            if "UPLOAD" in method:
                api_response = api_instance.upload_to_cool(int(iovid), cooltag)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling CoolApi->get_sqlite_for_cool: %s\n" % e)
            raise e

    def find_iovs_to_migrate(self, align_tag=None, cool_tag=None):
        # Create an instance of the API class
        api_instance = cool_api.CoolApi(self.api_client)
        try:
            log.info(f'Search iovs to migrate using : {align_tag} - {cool_tag}')
            api_response = api_instance.find_iovs_to_migrate(aligntag=align_tag, cooltag=cool_tag)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling CoolApi->find_iovs_to_migrate: %s\n" % e)
            raise e

    def create_align_cooltag(self, tag=None, cooltag=None,
                             tag_comment='none', 
                             cherrypy_config='none', 
                             tag_type=None,
                             coollock="LOCKED",
                             do_upload_on_reco=False):
        # Create an instance of the API class
        api_instance = cool_api.CoolApi(self.api_client)
        try:
            # Create an AlignCoolTagDto
            dto = AlignCoolTagDto(
                cool_tag=cooltag,
                align_tag=tag,
                tag_comment=tag_comment,
                coollock=coollock,
                do_upload_on_reco=do_upload_on_reco,
                cherrypy_config=cherrypy_config,
                tag_type=tag_type,
            )
            log.info(f'Insert new AlignCoolTag : {dto}')
            api_response = api_instance.create_align_cool_tag(align_cool_tag_dto=dto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling CoolApi->create_align_cool_tag: %s\n" % e)
            raise e

    def create_aligntag(self, tag=None, tag_type='TESTS', tag_description='none', tag_author='none'):
        # Create an instance of the API class
        api_instance = aligntag_api.AligntagApi(self.api_client)
        try:
            dnow = datetime.now(pytz.UTC)
            # Finds an AligntagDto by tag name
            dto = AligntagDto(tag_id=0, iov_tag=tag, tag_type=tag_type,
                              tag_insertion=dnow,
                              tag_description=tag_description, tag_author=tag_author)
            log.info(f'Insert new Aligntag : {dto}')
            api_response = api_instance.create_aligntag(aligntag_dto=dto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligntagApi->create_aligntag: %s\n" % e)
            raise e

    def find_align_iovs(self, tag=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            # Finds an AligniovSetDto by tag name and time range
            api_response = api_instance.find_align_iovs(tagname=tag,
                                                        **kwargs)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->find_align_iovs: %s\n" % e)
            raise e

    def create_aligniov(self, iov_tag=None, since=None, until=None, reco_status='EMPTY',
                        fit_status=0):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            reco_status = AligniovRecoStatusEnum(reco_status)
            # Create an AligniovDto
            dto = AligniovDto(iov_id=0, since_t=since, till_t=until,
                              reco_status=reco_status, fit_status=int(fit_status))
            api_response = api_instance.create_aligniov(tagname=iov_tag, aligniov_dto=dto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->create_aligniov: %s\n" % e)
            raise e

    def update_aligniov(self, iov_id=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            # define output fields
            valid_fields = ['reco_status', 'iov_comment', 'fit_status']
            # check request validity
            if not set(kwargs.keys()).issubset(valid_fields):
                log.error('Requested fields should be in %s', valid_fields)
                return
            upd = {}
            if 'reco_status' in kwargs.keys():
                reco_status = AligniovRecoStatusEnum(kwargs['reco_status'])
                upd['reco_status'] = reco_status
            if 'fit_status' in kwargs.keys():
                upd['fit_status'] = int(kwargs['fit_status'])
            if 'iov_comment' in kwargs.keys():
                upd['iov_comment'] = kwargs['iov_comment']
            dto = AligniovPayloadDto(**upd)
            # Update an iov using AligniovPayloadDto
            api_response = api_instance.update_align_iov(iovid=iov_id, aligniov_payload_dto=dto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->update_align_iov: %s\n" % e)
            raise e

    def get_iov_base(self, iovid=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            iovdto = api_instance.fetch_align_iov(iovid=iovid)
            clz = type(iovdto)
            rs = iovdto.to_dict()
            log.info(f'Fetched iov dto {iovdto}')
            reco_status = AligniovRecoStatusEnum(rs['reco_status'])
            basedto = AligniovBaseDto(iov_id=iovdto['iov_id'], since_t=iovdto['since_t'],
                                      till_t=iovdto['till_t'], reco_status=reco_status,
                                      fit_status=iovdto['fit_status'])
            log.info(f'Transform to base iov dto {basedto}')
            if iovdto is None:
                log.error(f'Cannot update iov id {iovid} because it could not find it')
                return
            return basedto
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->fetch_align_iov: %s\n" % e)
            raise e

    def create_iov_aligncorrections(self, iovid=None, corrections=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            basedto = self.get_iov_base(iovid=iovid)
            ncorr = len(corrections)
            log.info(f'Upload list of {ncorr} corrections to {basedto}')
            setdto = []
            for item in corrections:
                item['aligniov'] = basedto
                corrdto = AligncorrectionsDto(**item)
                setdto.append(corrdto)
            nset = len(setdto)
            log.info(f'Created a Set of length {nset} using Dto objects for insertion')
            # Create the corrections
            api_response = api_instance.create_iov_aligncorrections(
                iovid=iovid, aligncorrections_dto=setdto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->create_iov_aligncorrections: %s\n" % e)
            raise e

    def create_iov_fitsteps(self, iovid=None, corrections=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            basedto = self.get_iov_base(iovid=iovid)
            ncorr = len(corrections)
            log.info(f'Upload list of {ncorr} fitsteps to {basedto}')
            setdto = []
            for item in corrections:
                item['aligniov'] = basedto
                corrdto = AlignFitStepsDto(**item)
                setdto.append(corrdto)
            nset = len(setdto)
            log.info(f'Created a Set of length {nset} using Dto objects for insertion')
            # Create the corrections
            api_response = api_instance.create_iov_alignfitsteps(
                iovid=iovid, align_fit_steps_dto=setdto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->create_iov_alignfitsteps: %s\n" % e)
            raise e

    def create_iov_fitpulls(self, iovid=None, corrections=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            basedto = self.get_iov_base(iovid=iovid)
            ncorr = len(corrections)
            log.info(f'Upload list of {ncorr} fitpulls to {basedto}')
            setdto = []
            for item in corrections:
                item['aligniov'] = basedto
                corrdto = AlignFitPullsDto(**item)
                setdto.append(corrdto)
            nset = len(setdto)
            log.info(f'Created a Set of length {nset} using Dto objects for insertion')
            # Create the corrections
            api_response = api_instance.create_iov_alignfitpulls(
                iovid=iovid, align_fit_pulls_dto=setdto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->create_iov_alignfitpulls: %s\n" % e)
            raise e

    def create_iov_fitbadsensors(self, iovid=None, corrections=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            basedto = self.get_iov_base(iovid=iovid)
            ncorr = len(corrections)
            log.info(f'Upload list of {ncorr} bad sensors to {basedto}')
            setdto = []
            for item in corrections:
                # item['aligniov'] = basedto
                corrdto = AlignFitBadSensorsDto(**item)
                setdto.append(corrdto)
            nset = len(setdto)
            print(setdto)
            log.info(f'Created a Set of length {nset} using Dto objects for insertion')
            # Create the corrections
            api_response = api_instance.create_iov_bad_sensors(
                iovid=iovid, align_fit_bad_sensors_dto=setdto)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->create_iov_bad_sensors: %s\n" % e)
            raise e

    def delete_aligniov(self, iovid=None):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            api_response = api_instance.delete_align_iov(iovid=int(iovid))
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->delete_align_iov: %s\n" % e)
            raise e

    def find_opticallines_by_problem(self, qtype=None, **kwargs):
        # Create an instance of the API class
        api_instance = opticallines_api.OpticallinesApi(self.api_client)
        if qtype is None:
            qtype = "all"  # str | The set of optical lines to query  * `all` - all optical lines  * `problem` - all optical lines with failing glob_err flag  * `todo` - TODO list items, failing optical lines on positioned chambers  (optional) if omitted the server will use the default value of "all"

        # example passing only required values which don't have defaults set
        # and optional values
        try:
            # Retrieves a list of optical lines with their last OL tests
            api_response = api_instance.find_opticallines(query=qtype)
            pprint(api_response)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling OpticallinesApi->find_opticallines: %s\n" % e)
            raise e

    def find_opticallines_by_name(self, name=None, **kwargs):
        # Create an instance of the API class
        api_instance = opticallines_api.OpticallinesApi(self.api_client)
        # example passing only required values which don't have defaults set
        try:
            # Finds an OpticallinesSetDto by optical line name
            api_response = api_instance.find(name)
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling OpticallinesApi->find: %s\n" % e)
            raise e

    def find_corrections(self, iovid=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            api_response = api_instance.fetch_corrections(int(iovid))
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->fetch_corrections: %s\n" % e)
            raise e

    def find_fitpulls(self, iovid=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            api_response = api_instance.fetch_sensor_pulls(int(iovid))
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->fetch_fitpulls: %s\n" % e)
            raise e

    def find_fitsteps(self, iovid=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            api_response = api_instance.fetch_fit_steps(int(iovid))
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->fetch_fitsteps: %s\n" % e)
            raise e

    def find_badsensors(self, iovid=None, **kwargs):
        # Create an instance of the API class
        api_instance = aligniov_api.AligniovApi(self.api_client)
        try:
            api_response = api_instance.fetch_bad_sensors(int(iovid))
            return api_response
        except openapi_client.ApiException as e:
            print("Exception when calling AligniovApi->fetch_badsensors: %s\n" % e)
            raise e

    def help_params(self, dto_name):
        import inspect
        print(f'Help with parameters for {dto_name}')
        signature = None
        dto = None
        if 'AligntagDto' == dto_name:
            signature = inspect.signature(AligntagDto)
            dto = AligntagDto(tag_id=0, iov_tag='TEST', tag_type='TESTS')
        elif 'AligniovDto' == dto_name:
            signature = inspect.signature(AligniovBaseDto)
            reco_status = AligniovRecoStatusEnum('EMPTY')
            dto = AligniovBaseDto(iov_id=0, since_t=datetime.now(), till_t=datetime.now(),
                                  reco_status=reco_status, fit_status=0)
        else:
            print(f'Cannot help with type {dto_name}')
            return
        # Disable print of signature params
        if False:
            for param in signature.parameters.values():
                print(param)
        print(dto.openapi_types)
