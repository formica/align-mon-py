"""
    Atlas Muon Alignment

    OpenApi3 for Atlas Muon Alignment  # noqa: E501

    The version of the OpenAPI document: 0.1
    Generated by: https://openapi-generator.tech
"""


import re  # noqa: F401
import sys  # noqa: F401

from openapi_client.api_client import ApiClient, Endpoint as _Endpoint
from openapi_client.model_utils import (  # noqa: F401
    check_allowed_values,
    check_validations,
    date,
    datetime,
    file_type,
    none_type,
    validate_and_convert_types
)
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.interval_maker_conf_dto import IntervalMakerConfDto
from openapi_client.model.interval_maker_configuration_set_dto import IntervalMakerConfigurationSetDto
from openapi_client.model.interval_maker_iov_set_dto import IntervalMakerIovSetDto
from openapi_client.model.interval_maker_tag_dto import IntervalMakerTagDto
from openapi_client.model.interval_maker_tag_set_dto import IntervalMakerTagSetDto


class IntervalsApi(object):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client
        self.create_interval_maker_configuration_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerConfDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/configurations',
                'operation_id': 'create_interval_maker_configuration',
                'http_method': 'POST',
                'servers': None,
            },
            params_map={
                'all': [
                    'interval_maker_conf_dto',
                ],
                'required': [],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'interval_maker_conf_dto':
                        (IntervalMakerConfDto,),
                },
                'attribute_map': {
                },
                'location_map': {
                    'interval_maker_conf_dto': 'body',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [
                    'application/json'
                ]
            },
            api_client=api_client
        )
        self.create_interval_maker_iovs_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerIovSetDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/iovs',
                'operation_id': 'create_interval_maker_iovs',
                'http_method': 'POST',
                'servers': None,
            },
            params_map={
                'all': [
                    'since',
                    'till',
                    'tag',
                    'mode',
                ],
                'required': [
                    'since',
                    'till',
                    'tag',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'since':
                        (datetime,),
                    'till':
                        (datetime,),
                    'tag':
                        (str,),
                    'mode':
                        (str,),
                },
                'attribute_map': {
                    'since': 'since',
                    'till': 'till',
                    'tag': 'tag',
                    'mode': 'mode',
                },
                'location_map': {
                    'since': 'query',
                    'till': 'query',
                    'tag': 'query',
                    'mode': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json',
                    'application/xml'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.create_interval_maker_tag_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerTagDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/tags',
                'operation_id': 'create_interval_maker_tag',
                'http_method': 'POST',
                'servers': None,
            },
            params_map={
                'all': [
                    'interval_maker_tag_dto',
                ],
                'required': [],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'interval_maker_tag_dto':
                        (IntervalMakerTagDto,),
                },
                'attribute_map': {
                },
                'location_map': {
                    'interval_maker_tag_dto': 'body',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json'
                ],
                'content_type': [
                    'application/json'
                ]
            },
            api_client=api_client
        )
        self.find_interval_maker_configurations_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerConfigurationSetDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/configurations',
                'operation_id': 'find_interval_maker_configurations',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'tag',
                    'mode',
                ],
                'required': [],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'tag':
                        (str,),
                    'mode':
                        (str,),
                },
                'attribute_map': {
                    'tag': 'tag',
                    'mode': 'mode',
                },
                'location_map': {
                    'tag': 'query',
                    'mode': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json',
                    'application/xml'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.find_interval_maker_iovs_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerIovSetDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/iovs',
                'operation_id': 'find_interval_maker_iovs',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'since',
                    'till',
                    'tag',
                    'mode',
                ],
                'required': [
                    'since',
                    'till',
                    'tag',
                ],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'since':
                        (datetime,),
                    'till':
                        (datetime,),
                    'tag':
                        (str,),
                    'mode':
                        (str,),
                },
                'attribute_map': {
                    'since': 'since',
                    'till': 'till',
                    'tag': 'tag',
                    'mode': 'mode',
                },
                'location_map': {
                    'since': 'query',
                    'till': 'query',
                    'tag': 'query',
                    'mode': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json',
                    'application/xml'
                ],
                'content_type': [],
            },
            api_client=api_client
        )
        self.find_interval_maker_tags_endpoint = _Endpoint(
            settings={
                'response_type': (IntervalMakerTagSetDto,),
                'auth': [
                    'BearerAuth'
                ],
                'endpoint_path': '/intervals/tags',
                'operation_id': 'find_interval_maker_tags',
                'http_method': 'GET',
                'servers': None,
            },
            params_map={
                'all': [
                    'tag',
                    'mode',
                ],
                'required': [],
                'nullable': [
                ],
                'enum': [
                ],
                'validation': [
                ]
            },
            root_map={
                'validations': {
                },
                'allowed_values': {
                },
                'openapi_types': {
                    'tag':
                        (str,),
                    'mode':
                        (str,),
                },
                'attribute_map': {
                    'tag': 'tag',
                    'mode': 'mode',
                },
                'location_map': {
                    'tag': 'query',
                    'mode': 'query',
                },
                'collection_format_map': {
                }
            },
            headers_map={
                'accept': [
                    'application/json',
                    'application/xml'
                ],
                'content_type': [],
            },
            api_client=api_client
        )

    def create_interval_maker_configuration(
        self,
        **kwargs
    ):
        """Create a new IntervalMakerConfiguration.  # noqa: E501

        This method allows to insert IntervalMakerConfiguration.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_interval_maker_configuration(async_req=True)
        >>> result = thread.get()


        Keyword Args:
            interval_maker_conf_dto (IntervalMakerConfDto): [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerConfDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        return self.create_interval_maker_configuration_endpoint.call_with_http_info(**kwargs)

    def create_interval_maker_iovs(
        self,
        since,
        till,
        tag,
        **kwargs
    ):
        """Create IntervalMakerIovSetDto  # noqa: E501

        Create an IntervalMakerIovSetDto  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_interval_maker_iovs(since, till, tag, async_req=True)
        >>> result = thread.get()

        Args:
            since (datetime): the since time as string
            till (datetime): the until time as string
            tag (str): the IntervalMaker tag name

        Keyword Args:
            mode (str): the search mode. [optional] if omitted the server will use the default value of "compute"
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerIovSetDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['since'] = \
            since
        kwargs['till'] = \
            till
        kwargs['tag'] = \
            tag
        return self.create_interval_maker_iovs_endpoint.call_with_http_info(**kwargs)

    def create_interval_maker_tag(
        self,
        **kwargs
    ):
        """Create a new IntervalMakerTag.  # noqa: E501

        This method allows to insert IntervalMakerTag.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.create_interval_maker_tag(async_req=True)
        >>> result = thread.get()


        Keyword Args:
            interval_maker_tag_dto (IntervalMakerTagDto): [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerTagDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        return self.create_interval_maker_tag_endpoint.call_with_http_info(**kwargs)

    def find_interval_maker_configurations(
        self,
        **kwargs
    ):
        """Finds an IntervalMakerConfigurationSetDto  # noqa: E501

        Finds an IntervalMakerConfigurationSetDto  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.find_interval_maker_configurations(async_req=True)
        >>> result = thread.get()


        Keyword Args:
            tag (str): the IntervalMaker tag name. [optional] if omitted the server will use the default value of "DEFAULT"
            mode (str): the search mode. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerConfigurationSetDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        return self.find_interval_maker_configurations_endpoint.call_with_http_info(**kwargs)

    def find_interval_maker_iovs(
        self,
        since,
        till,
        tag,
        **kwargs
    ):
        """Finds an IntervalMakerIovSetDto  # noqa: E501

        Finds an IntervalMakerIovSetDto  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.find_interval_maker_iovs(since, till, tag, async_req=True)
        >>> result = thread.get()

        Args:
            since (datetime): the since time as string
            till (datetime): the until time as string
            tag (str): the IntervalMaker tag name

        Keyword Args:
            mode (str): the search mode. [optional] if omitted the server will use the default value of "history"
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerIovSetDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        kwargs['since'] = \
            since
        kwargs['till'] = \
            till
        kwargs['tag'] = \
            tag
        return self.find_interval_maker_iovs_endpoint.call_with_http_info(**kwargs)

    def find_interval_maker_tags(
        self,
        **kwargs
    ):
        """Finds an IntervalMakerTagSetDto  # noqa: E501

        Finds an IntervalMakerTagSetDto  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True

        >>> thread = api.find_interval_maker_tags(async_req=True)
        >>> result = thread.get()


        Keyword Args:
            tag (str): the IntervalMaker tag name. [optional] if omitted the server will use the default value of "*"
            mode (str): the search mode. [optional]
            _return_http_data_only (bool): response data without head status
                code and headers. Default is True.
            _preload_content (bool): if False, the urllib3.HTTPResponse object
                will be returned without reading/decoding response data.
                Default is True.
            _request_timeout (int/float/tuple): timeout setting for this request. If
                one number provided, it will be total request timeout. It can also
                be a pair (tuple) of (connection, read) timeouts.
                Default is None.
            _check_input_type (bool): specifies if type checking
                should be done one the data sent to the server.
                Default is True.
            _check_return_type (bool): specifies if type checking
                should be done one the data received from the server.
                Default is True.
            _spec_property_naming (bool): True if the variable names in the input data
                are serialized names, as specified in the OpenAPI document.
                False if the variable names in the input data
                are pythonic names, e.g. snake case (default)
            _content_type (str/None): force body content-type.
                Default is None and content-type will be predicted by allowed
                content-types and body.
            _host_index (int/None): specifies the index of the server
                that we want to use.
                Default is read from the configuration.
            async_req (bool): execute request asynchronously

        Returns:
            IntervalMakerTagSetDto
                If the method is called asynchronously, returns the request
                thread.
        """
        kwargs['async_req'] = kwargs.get(
            'async_req', False
        )
        kwargs['_return_http_data_only'] = kwargs.get(
            '_return_http_data_only', True
        )
        kwargs['_preload_content'] = kwargs.get(
            '_preload_content', True
        )
        kwargs['_request_timeout'] = kwargs.get(
            '_request_timeout', None
        )
        kwargs['_check_input_type'] = kwargs.get(
            '_check_input_type', True
        )
        kwargs['_check_return_type'] = kwargs.get(
            '_check_return_type', True
        )
        kwargs['_spec_property_naming'] = kwargs.get(
            '_spec_property_naming', False
        )
        kwargs['_content_type'] = kwargs.get(
            '_content_type')
        kwargs['_host_index'] = kwargs.get('_host_index')
        return self.find_interval_maker_tags_endpoint.call_with_http_info(**kwargs)

