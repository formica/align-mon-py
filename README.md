# Muon Alignment Client
This client can be used to interact with `muon alignment service` via REST.
Part of the client is generated from the REST API description.
A command line wrapper is available for the generated client in order to perform complex adminitration actions on the server.

## Requirements.

Python >=3.6

## Installation & Usage
In order to install this package you need 2 separate step (in general using a virtual env in python).
1. Install the open_api align client: 
```sh
pip install .
```
2. Install the CLI:
```sh
$ cd cli
$ pip install .
```

### Set up and authentication
To be allowed to connect with the alignment service you need to authentify via the CERN SSO.
If you have a client id and secret please proceed in the following way:
```sh
$ mkdir config
$ cd config
``` 
Now create a configuration file `config/authentication.json` for the authentication with the following format:
```
{
 "KC_CLIENT_ID": "atlas-muon-align-mon-aramys",
 "KC_CLIENT_SECRET":"my-wonderful-secret",
 "KC_TYPE": "secret"
}
```

### Certificate problems
In case you have certificate problems you better try to get CAs from CERN. Here below a simple example with some commands that can be useful to diagnose the problem.

First of all you can verify and set the location of standard CAs found by python. Be aware that with python > 3.6 some thigs changed respect to certificate location.

```sh
 $ CERT_PATH=$(python -m certifi)
 $ export SSL_CERT_FILE=${CERT_PATH}
 $ export REQUESTS_CA_BUNDLE=${CERT_PATH}
```

Inside the `CERT_PATH` you need a file with the full certificate authority bundle. For example in our `aiatlas093` machine you can find it in `/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem`. You should copy this file where you want to run the CLI client.
Supposing that our virtual env installation is in `<my-env>` directory, you will then create a link there to the location where your file has been copied. Something similar to this:
```sh
$ scp user@aiatlas095.cern.ch:/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem /usr/local/etc/ca-certificates/
$ cd $CERT_PATH; cp cacert.pem cacert.pem.orig;
$ ln -s /usr/local/etc/ca-certificates/tls-ca-bundle.pem cacert.pem
``` 

## Run a command
Here we list some useful examples for data management. We add in these examples the `--socks` option which indicates you are accessing the muon alignment service via an ssh tunnel.

### Get cool-tags
```
python aligncli.py ls --data cooltags -t EC_A_202% --host aiatlas095.cern.ch --port 4431 --ssl --socks
```
### Get iovs for an align-tag
```
python aligncli.py ls --data iovs -t BA_0901_RELATIVE --host aiatlas095.cern.ch --port 4431 --ssl --socks
```
