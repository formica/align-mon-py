# Instruction for COOL upload
These instructions describe the sequence of things to do in order to configure 
the system for the upload of align iovs in COOL.

## Cool commands
Start setting up athena from lxplus:
`setupATLAS`
`asetup Athena,master,latest`
Then you can use `AtlCool*` commands. 
And example to copy a tag into another one:
```commandline
export COOL_FLASK=http://aiatlas001.cern.ch:5000
python ~atlcond/utilsflask/AtlCoolCopyFlask.py "COOLOFL_MUONALIGN/CONDBR2" "oracle://ATONR_COOLOFL_GPN;schema=ATLAS_COOLOFL_MUONALIGN;dbname=CONDBR2;user=ATLAS_COOLOFL_MUONALIGN_W;password=<the writer password>" -f /MUONALIGN/MDT/BARREL -t MuonAlignMDTBarrelAlign-TEST-100 -ot MuonAlignMDTBarrelAlign-TEST-200 --host $COOL_FLASK
```
In case of a new UPD tag initialization, some arguments need to be added:
```commandline
--ignoremode IGNOREMODE
```
or the iovs will be squeezed following the chosed UPD convention.
## Authentication
The python client utilise the SSO authentication, but for the moment this is a bit manual.
If you access a protected API of align-mon please be careful to remove files like:
```commandline
.client-token.json
.client-api-token.json
```
They will be regenerated in the authentication process.
Once they are generated you can use the files for a limited time duration.
## Setup
### Cool Cherrypy Config
Configuration of the access to `cool-proxy` is done by hand in the appropriate table. The name has
to be something like `PROD-RUN3_BARREL`, which by default will try to use the official `ATONR_COOLOFL_GPN` 
as destination. The URL of the `cool-proxy` is `aiatlas001.cern.ch:5000/cool/iov` for the upload of single IOVs.

### Mapping between AlignTag and CoolTag
We suppose here that you already have an Aligntag used for automatic processing. 
We need now to associate it with a Cool tag. You can do this via the python doing something like:
```
 python aligncli.py create --data cooltags -t AF-TEST-01 --cooltag SOME_COOL_TAG --params "tag_comment=none,cherrypy_config=PROD-RUN3_BARREL,tag_type=TEST" --host localhost --port 8080
 or...
 python aligncli.py create --data cooltags -t BA_0900_TEST  --cooltag SOME_COOL_TAG --params "tag_comment=none,cherrypy_config=PROD-RUN3_BARREL,tag_type=TEST" --host aiatlas095.cern.ch --port 4431 --ssl --socks
```
This command suppose that `AF-TEST-01` already exists, and that a `CherryPy` configuration `PROD-RUN3_BARREL` is available.
You can now check the IOVs to be migrated:
```commandline
 python aligncli.py ls --data iovsmigration -t BA_0900_TEST -c SOME_COOL_TAG  --host aiatlas095.cern.ch --port 4431 --ssl --socks
```
