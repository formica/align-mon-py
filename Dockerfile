#_____________________________________________________________________________
#
# Andrea Formica
#
# Recipe to build a python image with the following features:
#      * Python 3.11
#      * netcat-traditional
#      * jq
#      * openapi_client
#      * setup.py
#      * setup.cfg
#      * README.md
#      * requirements.txt
#      * scripts
#      * cli
#
# Optimized:
#      * Most RUN commands are grouped in a single one to minimize
#        the number of layers and allow for clean-up after compilation
# ___________________________________________________________________________

# start from minimal image
FROM python:3.11

# install dependencies
RUN apt-get update && apt-get install -y netcat-traditional && apt-get install -y jq

# set up workdir
COPY openapi_client /home/openapi_client
COPY setup.py /home/setup.py
COPY setup.cfg /home/setup.cfg
COPY README.md /home/README.md
COPY requirements.txt /home/requirements.txt
COPY scripts /home/scripts
COPY cli /home/cli
COPY aligncli.py /home/aligncli.py

WORKDIR /home/

RUN pip install --upgrade pip && pip3 install . \
    && pip3 install -r ./cli/requirements.txt \
    && cd /home/cli && pip3 install . \
    && cd ${HOME} \
    # clean up
    && rm -rf ${HOME}/.cache/pip

CMD ["bash"]
