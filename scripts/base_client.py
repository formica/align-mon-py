import sys
from datetime import datetime
import pytz
from pytz import timezone
import os
# Log
import logging
from align_cli.log_setup import setup_logger
import openapi_client
from align_cli.api_call import ApiCall
from align_cli.alignutils import *
from align_cli.kc_tokens import KcApiTokens, KcExchangeTokens, AuthorizationConfig

log = logging.getLogger("aligncli.climgr")


# log.info('__file__={0:<35} | __name__={1:<20} | __package__={2:<20}'.format(__file__, __name__,
#                                                                          str(__package__)))


def convert_time_str(st, tt):
    since = None
    until = None
    v_info = sys.version_info
    log.info(f'Check python version {v_info}')
    if v_info[1] < 7:
        fmt_str = r"%Y-%m-%d %H:%M:%S"  # replaces the fromisoformatm, not available in python 3.6
        if st is not None:
            since = datetime.strptime(st, fmt_str).astimezone(pytz.utc)
        if tt is not None:
            until = datetime.strptime(tt, fmt_str).astimezone(pytz.utc)
    else:
        if st is not None:
            since = datetime.fromisoformat(st).astimezone(pytz.utc)
        if tt is not None:
            until = datetime.fromisoformat(tt).astimezone(pytz.utc)
        #since = datetime.strptime(st, '%Y-%m-%d %H:%M:%S')
        #until = datetime.strptime(tt, '%Y-%m-%d %H:%M:%S')
    return (since, until)

def format_ecol_time_str(datetime):
    # Need to bug test how find_ecols() reacts to receiving only one time stand instead of two
    # If it breaks without two timestamps, should add some clause to this function to avoid it breaking, or add an error message response to do_ls()
    # Need to retest if find ecols can use the regular time function now that the regular one has the timezone added properly
    if datetime is not None:
        time_out = datetime.strftime("%Y-%m-%dT%H:%M:%S+00:00")
    return time_out

def init_tokens():
    auth_config = AuthorizationConfig.from_env()
    if auth_config.kc_type == 'exchange':
        # Use exchange token
        tok_ex = KcExchangeTokens(auth_config=auth_config)
        log.info(f'Token exchange obj is {tok_ex}')
        tok_ex.get_exchange_token()
        # Now the token has been retrieved, switch to API mode
        auth_config.kc_type = "api"

    tok = KcApiTokens(
        max_tries=1,
        backoff_factor=1,
        auth_config=auth_config
    )
    if tok is not None:
        # If use_tokens is True, automatically request Keycloak
        # for tokens, and format the Authorization header with
        # Keycloak response.
        log.info(f'Get headers from token {tok}')
        auth_headers = tok.get_authorization_header()
        log.info(f'Retrieved headers...{auth_headers}')
        if len(auth_headers['Authorization']) < 10:
            raise ValueError('Cannot get correct token')
    return tok


class BaseClient(object):

    def __init__(self, server_url=None, use_tokens=True):
        if server_url is None:
            server_url = 'http://localhost:8080/api'

        self.cm = None
        self._config = None
        self._api = None
        self.host = server_url
        self._use_tokens = use_tokens
        self._access_token = None
        loc_parser = None
        import re
        self.rr = r"""
            ([<|>|:]+)  # match one of the symbols
        """
        self.rr = re.compile(self.rr, re.VERBOSE)

    def set_host(self, url):
        self.host = url

    def set_parser(self, prs):
        self.loc_parser = prs

    def do_connect(self, url=None):
        """connect [url]
        Use the url for server connections"""
        if not url:
            url = self.host
        if self._use_tokens:
            tok = init_tokens()
            self._access_token = tok.get_access_token()
            log.info(f'Retrieved token...{self._access_token}')

        self._config = openapi_client.Configuration(host=self.host, access_token=self._access_token)
        self._api = openapi_client.ApiClient(self._config)
        self.cm = ApiCall(self._api)
        log.info(f'CLI is connected to {url}')

    def socks(self):
        SOCKS5_PROXY_HOST = os.getenv('CDMS_SOCKS_HOST', 'localhost')
        SOCKS5_PROXY_PORT = 3129
        try:
            import socket
            import socks  # you need to install pysocks (use the command: pip install pysocks)
            # Configuration

            # Remove this if you don't plan to "deactivate" the proxy later
            #        default_socket = socket.socket
            # Set up a proxy
            #            if self.useSocks:
            socks.set_default_proxy(socks.SOCKS5, SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT)
            socket.socket = socks.socksocket
            log.info('Activated socks proxy on %s:%s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))
        except:
            log.info('Error activating socks...%s %s' % (SOCKS5_PROXY_HOST, SOCKS5_PROXY_PORT))

    def api_client(self):
        return self.cm