'''
Created on Nov 24, 2017

@author: formica
'''

import logging
import argparse
import sys, os
from base_client import BaseClient
from align_cli.alignutils import *
from align_cli.log_setup import setup_logger


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='CoolTag manager.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create'], default='ls')
    parser.add_argument('--host', default='aiatlas095.cern.ch',
                        help='Host of the Align service (default: aiatlas093.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='4431',
                        help='Port of the AlignMon service (default: 4431)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: true)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-t", "--tag",  default='%', help="the tag name")
    parser.add_argument("-c", "--cooltag", default='%', help="the cool tag name")
    parser.add_argument('--type', default='OPEN_ENDED', help="the cool tag type (UPD4, UPD1, ...): default is OPEN_ENDED")
    parser.add_argument('--comment', default='none', help="A description of the tag")
    parser.add_argument('--do_upload', action='store_true', help="default False")
    parser.add_argument('--config', default=None, help="The cherryPy config name, be careful to use the right one [i.e. PROD-RUN3_EC_X]")
    parser.add_argument('--lock', action='store_true', help="Locked status, default False")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    log = setup_logger(logging.INFO)

    prot = "http"
    use_tokens = False
    if args.ssl:
        prot = "https"
        use_tokens = True
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = BaseClient(use_tokens=use_tokens)
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    ## We should fix this !!
    ## For the moment on MACOS I need to append the all cern bundle to cert.pem
    os.environ['REQUESTS_CA_BUNDLE'] = '/usr/local/etc/openssl@1.1/certs/CERN-bundle.pem'

    import ssl
    log.info('========== CERTIFICATES help ==========')
    log.info(ssl.get_default_verify_paths())

    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)

    api_call = ui.api_client()
    fields = cooltagfieldsdicheader.keys()
    lock = False
    if args.lock:
        lock = True
    doupload = False
    if args.do_upload:
        doupload = True
#  From here we implement the methods to list and create cooltags
    if args.cmd in ['ls']:
        cdic = {}
        cooltagname = argsdic['cooltag']
        tagname = argsdic['tag']
        if cooltagname is not None:
            if '%' in cooltagname:
                cdic['cool_tag_like'] = cooltagname
            else:
                cdic['cool_tag'] = cooltagname
        if tagname is not None:
            if '%' in tagname:
                cdic['align_tag_like'] = tagname
            else:
                cdic['align_tag'] = tagname
        log.info(f'Search cool tags using: coolltag={cooltagname} and aligntag={tagname}')
        out = api_call.find_align_cooltag(**cdic)
        out['format'] = 'AlignCoolTagSetDto'
        server_print(out, format=fields)
    elif args.cmd in ['create']:
        cooltagname = argsdic['cooltag']
        tagname = argsdic['tag']
        tagtype = argsdic['type']
        if tagtype not in ['UPD4', 'UPD1', 'OPEN_ENDED']:
            log.error(f"Cannot create cooltag with type {tagtype}")
            exit
        comment = argsdic['comment']
        config = argsdic['config']
        cool_lock = 'UNLOCKED'
        if lock:
            cool_lock = 'LOCKED'

        if config is None:
            if 'EC_A' in tagname:
                config = 'PROD-RUN3_EC_A'
            elif 'EC_C' in tagname:
                config = 'PROD-RUN3_EC_C'
            elif 'BA_' in tagname:
                config = 'PROD-RUN3_BARREL'
                
        log.info(
            f'Creating cooltag {cooltagname}: associate to align tag {tagname}')
        if cooltagname is None or tagname is None:
            log.error(
                f"Cannot create cooltag without the name of a cool and align tags provided via --tag and --cooltag")
            exit
        out = api_call.create_align_cooltag(tag=tagname, cooltag=cooltagname,
                                            tag_comment=comment,
                                            cherrypy_config=config,
                                            tag_type=tagtype, 
                                            do_upload_on_reco=doupload, 
                                            coollock=cool_lock)
        print(f'Cooltag {cooltagname} created: {out}')
    else:
        log.info(f'Cannot launch command {args.cmd}')


