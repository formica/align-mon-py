'''
Created on Nov 24, 2017

@author: formica
'''

import logging
import argparse
import sys, os
from base_client import BaseClient
from align_cli.alignutils import *
from align_cli.log_setup import setup_logger


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Align Tag manager.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create'], default='ls')
    parser.add_argument('--host', default='aiatlas095.cern.ch',
                        help='Host of the Align service (default: aiatlas093.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='4431',
                        help='Port of the AlignMon service (default: 4431)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: true)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-t", "--tag",  default='%', help="the tag name")
    parser.add_argument('--type', default='PRODUCTION', help="the tag type (PRODUCTION, TESTS, MONTECARLO) default is PRODUCTION")
    parser.add_argument('--author', default='none', help="the author of the tag")
    parser.add_argument('--desciption', default='none', help="A description of the tag")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    log = setup_logger(logging.INFO)

    prot = "http"
    use_tokens = False
    if args.ssl:
        prot = "https"
        use_tokens = True
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host
    ui = BaseClient(use_tokens=use_tokens)
    ui.set_host(host)
    ui.do_connect()
    log.info('Start application')
    ## We should fix this !!
    ## For the moment on MACOS I need to append the all cern bundle to cert.pem
    os.environ['REQUESTS_CA_BUNDLE'] = '/usr/local/etc/openssl@1.1/certs/CERN-bundle.pem'

    import ssl
    log.info('========== CERTIFICATES help ==========')
    log.info(ssl.get_default_verify_paths())

    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_parser(parser)
    argsdic = vars(args)

    api_call = ui.api_client()
    fields = tagfieldsdicheader.keys()
#  From here we implement the methods to list and create cooltags
    if args.cmd in ['ls']:
        tagname = argsdic['tag']
        tagtype = argsdic['type']
        if tagname is  None:
            tagname = '%'
        log.info(f'Search tags using: aligntag={tagname} and type {tagtype}')
        out = api_call.find_aligntags(tag_type=tagtype, tag_pattern=tagname)
        out['format'] = 'AlignTagSetDto'

    elif args.cmd in ['create']:
        dto_name = 'AligntagDto'
        tagname = argsdic['tag']
        tagtype = argsdic['type']
        description = argsdic['description']    
        author = argsdic['author']
        log.info(f'Creating tag {tagname} and type {tagtype}')
        if tagname is None:
            log.error(f"Cannot create tag without the name provided via -t or --tag")
            sys.exit(1)
        out = api_call.create_aligntag(tag=tagname, 
                                       tag_type=tagtype,
                                       tag_description=description,
                                       tag_author=author)

    else:
        log.info(f'Cannot launch command {args.cmd}')

    server_print(out, format=fields)

