'''
Created on Nov 24, 2017

@author: formica
'''

import logging
from align_cli.log_setup import setup_logger
import argparse
import sys, os
from align_cli.climgr import AlignCli
from align_cli.alignutils import *
import requests
import time


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser(description='Align browser.', add_help=False)
    parser.add_argument('cmd', nargs='?', choices=['ls', 'create', 'dump', 'amdb','update', 'upload', 'rm', 'aramys_readout'], default='ls')
    parser.add_argument('--data', choices=['tags', 'iovs', 'corrections', 'fitsteps', 'fitpulls', 'badsensors', 'opticallinesproblems',
                                           'ecols', 'ecol_map', 'iovsmigration',
                                           'cooltags', 'opticallines', 'imtags', 'imiovs', 'imconfigs'], default='tags')
    parser.add_argument('--host', default='localhost',
                        help='Host of the Align service (default: aiatlas061.cern.ch)')
    parser.add_argument('--api', default='api',
                        help='Base name of the api (default: api)')
    parser.add_argument('--port', default='8080',
                        help='Port of the AlignMon service (default: 8080)')
    parser.add_argument('--socks', action='store_true',
                        help='Activate socks (default: false)')
    parser.add_argument('--nocert', action='store_true',
                        help='Deactivate certificate verification (default: false)')
    parser.add_argument('--ssl', action='store_true',
                        help='Activate ssl (default: false)')
    parser.add_argument('-h', '--help', action="store_true", help='show this help message')
    parser.add_argument("-m", "--mode", help="the mode to request intervalmaker iovs: [history, execute, store]",
                        default="history")
    parser.add_argument("-t", "--tag", help="the tag name")
    parser.add_argument("-c", "--cooltag", help="the cool tag name")
    parser.add_argument("-i", "--iovid", default=None, help="the IovID.")
    parser.add_argument("--oltype", help="the optical line table for reading LWDAQ data", choices=["B","R","T","D"])
    parser.add_argument("--sensors", help='pattern recognition filtering for EC sensor names, e.g. BAZ_AEI%%')
    parser.add_argument("--olname", help="the opticalline name")
    parser.add_argument("--olproblem", help="the opticalline problem [all, problem, todo]", default='all')
    parser.add_argument("--type", help="the tag type", default="PRODUCTION")
    parser.add_argument("--params", help="the string containing k=v pairs for tags or global tags creation")
    parser.add_argument("--cut", help="additional selection parameters. e.g. since>1000,until<2000")
    parser.add_argument("--inpfile", help="the input file to upload")
    parser.add_argument("--since", help="the since time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00. Can also use 'previous' to get the until time of the previous IoV (this only works for the create cmd)")
    parser.add_argument("--until", help="the until time for the alignment iov. Ex: 2020-01-01 15:10:00+00:00")
    parser.add_argument("--corrections", help="the list of files to upload: alines=alines.txt,blines=blines.txt,"
                                              "fitsteps=fs.txt,pulls=pulls.txt")
    parser.add_argument("--amdb", help="the list of files to merge: alines=alines.txt,blines=blines.txt")
    parser.add_argument("--side", default='NONE', help="the Endcap Side [A|C].")
    parser.add_argument("--page", default=0, help="the page number.")
    parser.add_argument("--size", default=100, help="the page size.")
    parser.add_argument("--sort", default=None, help="the sort parameter (depend on the selection).")
    parser.add_argument("-f", "--fields", default='none',
                        help="the list of fields to show, separated with a comma. Use -f help to get the available fields.")
    parser.add_argument("-H", "--header", default="BLOB", help="set header request for payload: BLOB, JSON, ...")
    parser.add_argument("--showinfo", default=True, help="Show diagnostic information (True or False)")

    args = parser.parse_args()
    if args.help:
        parser.print_help()
        sys.exit()

    if args.showinfo == True:
        log = setup_logger(logging.INFO)
    elif args.showinfo == False:
        log = setup_logger(logging.ERROR)

    prot = "http"
    use_tokens = False
    if args.ssl:
        prot = "https"
        use_tokens = True
    host = "{0}://{1}:{2}/{3}".format(prot, args.host, args.port, args.api)
    log.info('The host is set to %s' % host)
    os.environ['CDMS_HOST'] = host

    import ssl
    log.info('========== CERTIFICATES help ==========')
    log.info(ssl.get_default_verify_paths())

    verify_ssl = True
    if args.nocert:
        log.info("Deactivating certificate verification")
        verify_ssl = False
    ## Initialize the AlignCli    
    ui = AlignCli(use_tokens=use_tokens)
    ## Activate socks if requested
    if args.socks:
        log.info("Activating socks on localhost:3129\n if you want another address please set CDMS_SOCKS_HOST env.")
        ui.socks()

    ui.set_host(host)
    ui.do_connect(verify_ssl=verify_ssl)
    log.info('Start application')
    ## We should fix this !!
    ## For the moment on MACOS I need to append the all cern bundle to cert.pem
    os.environ['REQUESTS_CA_BUNDLE'] = '/usr/local/etc/openssl@1.1/certs/CERN-bundle.pem'

    ui.set_parser(parser)
    argsdic = vars(args)
    if args.fields:
        if args.fields == 'help':
            if args.data == 'tags':
                print(f'Fields for {args.cmd} are {tagfieldsdicheader.keys()}')
            else:
                print('not available for this command')
            sys.exit()
    if args.params:
        if args.params == 'help':
            print_help(args.data)
            sys.exit()
    if args.corrections:
        if args.corrections == 'help':
            print_corrections()
            sys.exit()
    if args.cmd in ['ls']:
        log.info(f'Launch ls command on {args.data}')
        ui.do_ls(argsdic)
    elif args.cmd in ['create']:
        log.info(f'Launch create command on {args.data}')
        ui.do_create(argsdic)
    elif args.cmd in ['dump']:
        log.info(f'Launch dump command on {args.data}')
        ui.do_dump(argsdic)
    elif args.cmd in ['amdb']:
        log.info(f'Read amdb files to create CLOB in ascii format')
        ui.do_amdb(argsdic)
    elif args.cmd in ['update']:
        log.info(f'Update pojo: for example iov fit and reco status')
        ui.do_update(argsdic)
    elif args.cmd in ['rm']:
        log.info(f'Delete data: only for iovs now....')
        ui.do_delete(argsdic)
    elif args.cmd in ['upload']:
        log.info(f'Upload data to COOL: only iovs as data type is allowed....')
        ui.do_upload(argsdic)
    elif args.cmd in ['aramys_readout']:
        log.info(f'Create readout.db ARAMyS input file')
        ui.do_aramys_readout(argsdic)
    else:
        log.info(f'Cannot launch command {args.cmd}')
