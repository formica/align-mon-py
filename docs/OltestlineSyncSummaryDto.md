# OltestlineSyncSummaryDto

Summary of the sync actions performed by query sync_batch_files_to_db

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number_of_tests** | **int** | number of tests synced | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


