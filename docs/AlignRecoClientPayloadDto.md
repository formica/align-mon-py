# AlignRecoClientPayloadDto

The information posted by reco clients to register itself

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_name** | **str** |  | 
**user_name** | **str** |  | 
**client_name** | **str** |  | 
**branch** | **str** |  | 
**revision** | **str** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


