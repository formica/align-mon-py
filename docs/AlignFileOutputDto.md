# AlignFileOutputDto

The AlignFile dto object returned by GET queries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_id** | **int** |  | 
**file_size** | **int** |  | 
**insertion_time** | **datetime** |  | 
**compression_type** | **str** |  | 
**compressed_file_size** | **int** |  | 
**sha_hash** | **str** |  | 
**file_name** | **str** |  | 
**user_name** | **str** |  | 
**file_date** | **datetime** |  | 
**mime_type** | **str** |  | 
**external_ref** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


