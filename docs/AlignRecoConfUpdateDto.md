# AlignRecoConfUpdateDto

Limited update payload for an AlignRecoConf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_description** | **str** |  | [optional] 
**activated_online** | **bool** |  | [optional] 
**online_lookback_minutes** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


