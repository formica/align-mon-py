# DaqSummaryDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qname** | **str** |  | 
**server** | **int** |  | 
**total** | **int** |  | 
**status_ok** | **int** |  | 
**rate_ok** | **float** |  | 
**status_bad** | **int** |  | 
**rate_bad** | **float** |  | 
**err1** | **int** |  | 
**rate_err1** | **float** |  | 
**err2** | **int** |  | 
**rate_err2** | **float** |  | 
**err3** | **int** |  | 
**rate_err3** | **float** |  | 
**err4** | **int** |  | 
**rate_err4** | **float** |  | 
**err5** | **int** |  | 
**rate_err5** | **float** |  | 
**err6** | **int** |  | 
**rate_err6** | **float** |  | 
**err7** | **int** |  | 
**rate_err7** | **float** |  | 
**err8** | **int** |  | 
**rate_err8** | **float** |  | 
**err9** | **int** |  | 
**rate_err9** | **float** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


