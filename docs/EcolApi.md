# openapi_client.EcolApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_al_cycle**](EcolApi.md#get_al_cycle) | **GET** /ecol/cycles | Retrieves cycles
[**get_al_history**](EcolApi.md#get_al_history) | **GET** /ecol/als | Retrieves al data history
[**get_al_history_cycles**](EcolApi.md#get_al_history_cycles) | **GET** /ecol/alcycles | Retrieves al data by cycle
[**get_images**](EcolApi.md#get_images) | **GET** /ecol/images | Retrieves images data


# **get_al_cycle**
> AlignMonBaseResponse get_al_cycle()

Retrieves cycles

Retrieves cycles by id or range in time 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import ecol_api
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = ecol_api.EcolApi(api_client)
    cycle = 1 # int | the start time of the query (optional)
    start = 1 # int | start time (optional)
    end = 1 # int | end time (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves cycles
        api_response = api_instance.get_al_cycle(cycle=cycle, start=start, end=end)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_al_cycle: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cycle** | **int**| the start time of the query | [optional]
 **start** | **int**| start time | [optional]
 **end** | **int**| end time | [optional]

### Return type

[**AlignMonBaseResponse**](AlignMonBaseResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_al_history**
> AlignMonBaseResponse get_al_history(since, till)

Retrieves al data history

Retrieves Al data history, with parameterizable source, error query and optional sensor list 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import ecol_api
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = ecol_api.EcolApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the query
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the query
    image = "AlB" # str | The image type to retrieve  * `AlB` - the ALB table  * `AlR` - the ALR table  * `AlT` - the ALT table  * `AlD` - the ALD table  (optional) if omitted the server will use the default value of "AlB"
    sensors = "sensors_example" # str | an (optional) list of sensors to query. I empty, retrieves all sensors (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves al data history
        api_response = api_instance.get_al_history(since, till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_al_history: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves al data history
        api_response = api_instance.get_al_history(since, till, image=image, sensors=sensors)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_al_history: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the start time of the query |
 **till** | **datetime**| the end time of the query |
 **image** | **str**| The image type to retrieve  * &#x60;AlB&#x60; - the ALB table  * &#x60;AlR&#x60; - the ALR table  * &#x60;AlT&#x60; - the ALT table  * &#x60;AlD&#x60; - the ALD table  | [optional] if omitted the server will use the default value of "AlB"
 **sensors** | **str**| an (optional) list of sensors to query. I empty, retrieves all sensors | [optional]

### Return type

[**AlignMonBaseResponse**](AlignMonBaseResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_al_history_cycles**
> AlignMonBaseResponse get_al_history_cycles(cycle)

Retrieves al data by cycle

Retrieves Al data cycle 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import ecol_api
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = ecol_api.EcolApi(api_client)
    cycle = 1 # int | the start time of the query
    image = "AlB" # str | The image type to retrieve  * `AlB` - the ALB table  * `AlR` - the ALR table  * `AlT` - the ALT table  * `AlD` - the ALD table  (optional) if omitted the server will use the default value of "AlB"

    # example passing only required values which don't have defaults set
    try:
        # Retrieves al data by cycle
        api_response = api_instance.get_al_history_cycles(cycle)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_al_history_cycles: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves al data by cycle
        api_response = api_instance.get_al_history_cycles(cycle, image=image)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_al_history_cycles: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cycle** | **int**| the start time of the query |
 **image** | **str**| The image type to retrieve  * &#x60;AlB&#x60; - the ALB table  * &#x60;AlR&#x60; - the ALR table  * &#x60;AlT&#x60; - the ALT table  * &#x60;AlD&#x60; - the ALD table  | [optional] if omitted the server will use the default value of "AlB"

### Return type

[**AlignMonBaseResponse**](AlignMonBaseResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_images**
> AlignMonBaseResponse get_images()

Retrieves images data

Retrieve images (optical lines) for endcap optical alignment 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import ecol_api
from openapi_client.model.align_mon_base_response import AlignMonBaseResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = ecol_api.EcolApi(api_client)
    image = "B" # str | The image type to retrieve  * `B` - the B images table  * `R` - the R images table  * `D` - the D images table  * `T` - the T images table  (optional) if omitted the server will use the default value of "B"
    sensor = "none" # str | pattern for search on image name (optional) if omitted the server will use the default value of "none"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves images data
        api_response = api_instance.get_images(image=image, sensor=sensor)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EcolApi->get_images: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image** | **str**| The image type to retrieve  * &#x60;B&#x60; - the B images table  * &#x60;R&#x60; - the R images table  * &#x60;D&#x60; - the D images table  * &#x60;T&#x60; - the T images table  | [optional] if omitted the server will use the default value of "B"
 **sensor** | **str**| pattern for search on image name | [optional] if omitted the server will use the default value of "none"

### Return type

[**AlignMonBaseResponse**](AlignMonBaseResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

