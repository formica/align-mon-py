# AligntagWithSummaryDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_id** | **int** |  | 
**iov_tag** | **str** |  | 
**tag_type** | **str** |  | 
**tag_description** | **str** |  | [optional] 
**tag_insertion** | **datetime** |  | [optional] 
**prev_tag** | **int** |  | [optional] 
**tag_author** | **str** |  | [optional] 
**tag_summary** | [**AligntagSummaryDto**](AligntagSummaryDto.md) |  | [optional] 
**align_reco_conf** | [**AlignRecoConfDto**](AlignRecoConfDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


