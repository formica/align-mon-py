# openapi_client.AligntimeseriesApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**find_align_corrections**](AligntimeseriesApi.md#find_align_corrections) | **GET** /aligntimeseries/corrections | Retrieves alignment corrections, covering a range in time
[**find_align_corrections_ids**](AligntimeseriesApi.md#find_align_corrections_ids) | **GET** /aligntimeseries/corrections/allowedids | Retrieves the list of possible element IDs, for a given alignment tag
[**find_align_sagittas**](AligntimeseriesApi.md#find_align_sagittas) | **GET** /aligntimeseries/sagittas | Retrieves alignment sagittas, covering a range in time
[**find_align_sagittas_ids**](AligntimeseriesApi.md#find_align_sagittas_ids) | **GET** /aligntimeseries/sagittas/allowedids | Retrieves the list of possible chamber IDs, for a given alignment tag
[**find_align_sensor_pulls**](AligntimeseriesApi.md#find_align_sensor_pulls) | **GET** /aligntimeseries/sensorpulls | Retrieves a list of sensor pulls, covering a range in time
[**find_align_sensor_pulls_ids**](AligntimeseriesApi.md#find_align_sensor_pulls_ids) | **GET** /aligntimeseries/sensorpulls/allowedids | Retrieves the list of possible sensor pull IDs, for a given alignment tag
[**find_align_sensor_residuals**](AligntimeseriesApi.md#find_align_sensor_residuals) | **GET** /aligntimeseries/sensorresiduals | Retrieves a list of sensor residuals, covering a range in time
[**find_align_sensor_residuals_ids**](AligntimeseriesApi.md#find_align_sensor_residuals_ids) | **GET** /aligntimeseries/sensorresiduals/allowedids | Retrieves the list of possible sensor residual IDs, for a given alignment tag


# **find_align_corrections**
> AligncorrectionsSetDto find_align_corrections(tagname, elements)

Retrieves alignment corrections, covering a range in time

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.aligncorrections_set_dto import AligncorrectionsSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    elements = [
        "elements_example",
    ] # [str] | a list of chamber hardware IDs to query. Must be non-empty.
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to origin of tag if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to since+offset if not present) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves alignment corrections, covering a range in time
        api_response = api_instance.find_align_corrections(tagname, elements)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_corrections: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves alignment corrections, covering a range in time
        api_response = api_instance.find_align_corrections(tagname, elements, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_corrections: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **elements** | **[str]**| a list of chamber hardware IDs to query. Must be non-empty. |
 **since** | **datetime**| the since time of the interval to query (defaults to origin of tag if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to since+offset if not present) | [optional]

### Return type

[**AligncorrectionsSetDto**](AligncorrectionsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_corrections_ids**
> StringSet find_align_corrections_ids(tagname)

Retrieves the list of possible element IDs, for a given alignment tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.string_set import StringSet
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the list of possible element IDs, for a given alignment tag
        api_response = api_instance.find_align_corrections_ids(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_corrections_ids: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |

### Return type

[**StringSet**](StringSet.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sagittas**
> AlignSagittasSetDto find_align_sagittas(tagname, elements)

Retrieves alignment sagittas, covering a range in time

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.align_sagittas_set_dto import AlignSagittasSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    elements = [
        "elements_example",
    ] # [str] | a list of chamber hardware IDs to query. Must be non-empty.
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to origin of tag if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to since+offset if not present) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves alignment sagittas, covering a range in time
        api_response = api_instance.find_align_sagittas(tagname, elements)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sagittas: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves alignment sagittas, covering a range in time
        api_response = api_instance.find_align_sagittas(tagname, elements, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sagittas: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **elements** | **[str]**| a list of chamber hardware IDs to query. Must be non-empty. |
 **since** | **datetime**| the since time of the interval to query (defaults to origin of tag if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to since+offset if not present) | [optional]

### Return type

[**AlignSagittasSetDto**](AlignSagittasSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sagittas_ids**
> StringSet find_align_sagittas_ids(tagname)

Retrieves the list of possible chamber IDs, for a given alignment tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.string_set import StringSet
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the list of possible chamber IDs, for a given alignment tag
        api_response = api_instance.find_align_sagittas_ids(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sagittas_ids: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |

### Return type

[**StringSet**](StringSet.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sensor_pulls**
> AlignFitPullsSetDto find_align_sensor_pulls(tagname, sensors)

Retrieves a list of sensor pulls, covering a range in time

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_fit_pulls_set_dto import AlignFitPullsSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    sensors = [
        "sensors_example",
    ] # [str] | a list of sensors to query. Must be non-empty.
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to infinite past if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to inifinite future if not present) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a list of sensor pulls, covering a range in time
        api_response = api_instance.find_align_sensor_pulls(tagname, sensors)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_pulls: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a list of sensor pulls, covering a range in time
        api_response = api_instance.find_align_sensor_pulls(tagname, sensors, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_pulls: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **sensors** | **[str]**| a list of sensors to query. Must be non-empty. |
 **since** | **datetime**| the since time of the interval to query (defaults to infinite past if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to inifinite future if not present) | [optional]

### Return type

[**AlignFitPullsSetDto**](AlignFitPullsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sensor_pulls_ids**
> StringSet find_align_sensor_pulls_ids(tagname)

Retrieves the list of possible sensor pull IDs, for a given alignment tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.string_set import StringSet
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the list of possible sensor pull IDs, for a given alignment tag
        api_response = api_instance.find_align_sensor_pulls_ids(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_pulls_ids: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |

### Return type

[**StringSet**](StringSet.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sensor_residuals**
> AlignSensorResidualsSetDto find_align_sensor_residuals(tagname, sensors)

Retrieves a list of sensor residuals, covering a range in time

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_sensor_residuals_set_dto import AlignSensorResidualsSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    sensors = [
        "sensors_example",
    ] # [str] | a list of sensors to query. Must be non-empty.
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to infinite past if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to inifinite future if not present) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a list of sensor residuals, covering a range in time
        api_response = api_instance.find_align_sensor_residuals(tagname, sensors)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_residuals: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a list of sensor residuals, covering a range in time
        api_response = api_instance.find_align_sensor_residuals(tagname, sensors, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_residuals: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **sensors** | **[str]**| a list of sensors to query. Must be non-empty. |
 **since** | **datetime**| the since time of the interval to query (defaults to infinite past if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to inifinite future if not present) | [optional]

### Return type

[**AlignSensorResidualsSetDto**](AlignSensorResidualsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_sensor_residuals_ids**
> StringSet find_align_sensor_residuals_ids(tagname)

Retrieves the list of possible sensor residual IDs, for a given alignment tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntimeseries_api
from openapi_client.model.string_set import StringSet
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntimeseries_api.AligntimeseriesApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the list of possible sensor residual IDs, for a given alignment tag
        api_response = api_instance.find_align_sensor_residuals_ids(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntimeseriesApi->find_align_sensor_residuals_ids: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |

### Return type

[**StringSet**](StringSet.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

