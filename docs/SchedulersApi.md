# openapi_client.SchedulersApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_scheduler**](SchedulersApi.md#create_scheduler) | **POST** /schedulers | Create a new scheduler.
[**find_schedulers**](SchedulersApi.md#find_schedulers) | **GET** /schedulers | Finds schedulers execution status
[**get_scheduler**](SchedulersApi.md#get_scheduler) | **GET** /schedulers/{id} | Finds a specific scheduler execution status
[**list_email_addresses**](SchedulersApi.md#list_email_addresses) | **GET** /schedulers/emails | List experts.
[**update_scheduler**](SchedulersApi.md#update_scheduler) | **PUT** /schedulers/{id} | Update a scheduler.
[**update_scheduler_state**](SchedulersApi.md#update_scheduler_state) | **PUT** /schedulers/{id}/state | Switches the state (RUNNING/IDLE) of a scheduler


# **create_scheduler**
> SchedulerDto create_scheduler()

Create a new scheduler.

This method allows to insert Scheduler.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.scheduler_dto import SchedulerDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)
    scheduler_dto = SchedulerDto(
        name="name_example",
        status="status_example",
        active=True,
        insertion_time=dateutil_parser('1970-01-01T00:00:00.00Z'),
        last_execution=dateutil_parser('1970-01-01T00:00:00.00Z'),
        last_endof_execution=dateutil_parser('1970-01-01T00:00:00.00Z'),
        execution_mode="execution_mode_example",
    ) # SchedulerDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new scheduler.
        api_response = api_instance.create_scheduler(scheduler_dto=scheduler_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->create_scheduler: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduler_dto** | [**SchedulerDto**](SchedulerDto.md)|  | [optional]

### Return type

[**SchedulerDto**](SchedulerDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_schedulers**
> SchedulerSetDto find_schedulers()

Finds schedulers execution status

Get schedulers

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.scheduler_set_dto import SchedulerSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Finds schedulers execution status
        api_response = api_instance.find_schedulers()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->find_schedulers: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**SchedulerSetDto**](SchedulerSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_scheduler**
> SchedulerDto get_scheduler(id)

Finds a specific scheduler execution status

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.scheduler_dto import SchedulerDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)
    id = "id_example" # str | The Scheduler name

    # example passing only required values which don't have defaults set
    try:
        # Finds a specific scheduler execution status
        api_response = api_instance.get_scheduler(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->get_scheduler: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The Scheduler name |

### Return type

[**SchedulerDto**](SchedulerDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_email_addresses**
> MailUserAddressSetDto list_email_addresses()

List experts.

This method allows to get the list of users concerned by emails.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.mail_user_address_set_dto import MailUserAddressSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)
    svc_like = "svcLike_example" # str | A LIKE pattern for the service name (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # List experts.
        api_response = api_instance.list_email_addresses(svc_like=svc_like)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->list_email_addresses: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **svc_like** | **str**| A LIKE pattern for the service name | [optional]

### Return type

[**MailUserAddressSetDto**](MailUserAddressSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scheduler**
> SchedulerDto update_scheduler(id, generic_map)

Update a scheduler.

This method allows to activate or deactivate a Scheduler.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.generic_map import GenericMap
from openapi_client.model.scheduler_dto import SchedulerDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)
    id = "id_example" # str | The Scheduler name
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap | A JSON object containing generic map used for updates

    # example passing only required values which don't have defaults set
    try:
        # Update a scheduler.
        api_response = api_instance.update_scheduler(id, generic_map)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->update_scheduler: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The Scheduler name |
 **generic_map** | [**GenericMap**](GenericMap.md)| A JSON object containing generic map used for updates |

### Return type

[**SchedulerDto**](SchedulerDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_scheduler_state**
> SchedulerDto update_scheduler_state(id, state)

Switches the state (RUNNING/IDLE) of a scheduler

Checks are performed so that the new state is valid

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import schedulers_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.scheduler_dto import SchedulerDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = schedulers_api.SchedulersApi(api_client)
    id = "id_example" # str | The Scheduler name
    state = "RUNNING" # str | The new scheduler state

    # example passing only required values which don't have defaults set
    try:
        # Switches the state (RUNNING/IDLE) of a scheduler
        api_response = api_instance.update_scheduler_state(id, state)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SchedulersApi->update_scheduler_state: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| The Scheduler name |
 **state** | **str**| The new scheduler state |

### Return type

[**SchedulerDto**](SchedulerDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

