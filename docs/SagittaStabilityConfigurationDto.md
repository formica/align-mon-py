# SagittaStabilityConfigurationDto

A configuration to parameterize the stability check of the SagittaDiffService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stability_algo** | **str** |  | 
**factor_sag_error_l** | **float** |  | [optional] 
**factor_sag_error_s** | **float** |  | [optional] 
**common_error_l** | **float** |  | [optional] 
**common_error_s** | **float** |  | [optional] 
**threshold_l** | **float** |  | [optional] 
**threshold_s** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


