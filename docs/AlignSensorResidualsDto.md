# AlignSensorResidualsDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_name** | **str** |  | 
**sensor_measurements** | [**[AlignSensorMeasDto]**](AlignSensorMeasDto.md) |  | 
**res_id** | **int** |  | [optional] 
**type** | **str** |  | [optional] 
**dist_cl** | **float** |  | [optional] 
**dist_lm** | **float** |  | [optional] 
**aligniov** | [**AligniovBaseDto**](AligniovBaseDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


