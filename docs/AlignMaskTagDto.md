# AlignMaskTagDto

A tag grouping several AlignMaskSensor entities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mask_tag** | **str** | The tag name | 
**tag_id** | **int** | Generated primary key | [optional] 
**tag_comment** | **str** | A comment string | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


