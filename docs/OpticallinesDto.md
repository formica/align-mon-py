# OpticallinesDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**olformat** | **str** |  | 
**olname** | **str** |  | 
**testcode** | **int** |  | [optional] 
**last_test** | [**OltestlinesDto**](OltestlinesDto.md) |  | [optional] 
**ol_incidents** | [**[OlIncidentDto]**](OlIncidentDto.md) |  | [optional] 
**oltype** | **str** |  | [optional] 
**olstatus** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


