# AlignCoolTagViewDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schema** | **str** |  | [optional] 
**db** | **str** |  | [optional] 
**folder** | **str** |  | [optional] 
**gtag_id** | **int** |  | [optional] 
**gtag_name** | **str** |  | [optional] 
**gtag_description** | **str** |  | [optional] 
**gtag_lock_status** | **int** |  | [optional] 
**tag_id** | **int** |  | [optional] 
**tag_name** | **str** |  | [optional] 
**tag_description** | **str** |  | [optional] 
**tag_lock_status** | **int** |  | [optional] 
**sys_instime** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


