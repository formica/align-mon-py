# AligniovRecoStatusEnum


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** |  |  must be one of ["EMPTY", "RECO_RUNNING", "UPLOADING", "QC_PENDING", "BAD", "GOOD_FIT", "STABLE", "RELEASED_FOR_CONDDB", ]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


