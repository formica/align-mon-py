# AlBDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**AlIdDto**](AlIdDto.md) |  | [optional] 
**cycle_number** | **int** |  | [optional] 
**x** | **float** |  | [optional] 
**y** | **float** |  | [optional] 
**mag** | **float** |  | [optional] 
**bpx** | **float** |  | [optional] 
**bin** | **float** |  | [optional] 
**bse** | **float** |  | [optional] 
**bth** | **float** |  | [optional] 
**bex** | **float** |  | [optional] 
**bav** | **float** |  | [optional] 
**bst** | **float** |  | [optional] 
**bmx** | **float** |  | [optional] 
**bmn** | **float** |  | [optional] 
**bec** | **float** |  | [optional] 
**bnp** | **float** |  | [optional] 
**bco** | **float** |  | [optional] 
**bbg** | **float** |  | [optional] 
**brn** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


