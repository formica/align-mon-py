# AlignSagittasDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uxtrack** | **float** |  | 
**uytrack** | **float** |  | 
**uztrack** | **float** |  | 
**bi** | **str** |  | 
**bm** | **str** |  | 
**bo** | **str** |  | 
**sag_correction** | **float** |  | 
**sag_error** | **float** |  | 
**iovid** | **int** |  | [optional] 
**sag_id** | **int** |  | [optional] 
**xtrack** | **float** |  | [optional] 
**ytrack** | **float** |  | [optional] 
**ztrack** | **float** |  | [optional] 
**aligniov** | [**AligniovBaseDto**](AligniovBaseDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


