# HTTPResponse

General response object to be used for passing errors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **int** | HTTP status code of the response | 
**error** | **str** | An error code string specific to the error that was raised | 
**message** | **str** | A human friendly message containing the details of the error | 
**timestamp** | **datetime** |  | [optional] 
**path** | **str** | Path or URI of the requested resource | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


