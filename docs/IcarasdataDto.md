# IcarasdataDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channel_name** | **str** |  | [optional] 
**stime** | **datetime** |  | [optional] 
**measure_id** | **int** |  | [optional] 
**seq_nr** | **int** |  | [optional] 
**glob_err** | **int** |  | [optional] 
**anal_err** | **int** |  | [optional] 
**xvalue** | **float** |  | [optional] 
**yvalue** | **float** |  | [optional] 
**scale** | **float** |  | [optional] 
**rotz** | **float** |  | [optional] 
**err_x** | **float** |  | [optional] 
**err_y** | **float** |  | [optional] 
**err_scale** | **float** |  | [optional] 
**err_rotz** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


