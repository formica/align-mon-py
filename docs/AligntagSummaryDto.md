# AligntagSummaryDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag_id** | **int** |  | 
**niovs** | **int** |  | 
**niovs_for_update** | **int** |  | 
**niovs_stable** | **int** |  | 
**niovs_bad** | **int** |  | 
**iov_max_since** | **datetime** |  | [optional] 
**iov_min_since** | **datetime** |  | [optional] 
**iov_max_until** | **datetime** |  | [optional] 
**iov_min_until** | **datetime** |  | [optional] 
**exists_iov** | **bool** |  | [optional] 
**exists_corr** | **bool** |  | [optional] 
**exists_fitsteps** | **bool** |  | [optional] 
**exists_bad_sensors** | **bool** |  | [optional] 
**exists_pull** | **bool** |  | [optional] 
**exists_residuals** | **bool** |  | [optional] 
**exists_sagitta** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


