# openapi_client.CoolApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_align_cool_tag**](CoolApi.md#create_align_cool_tag) | **POST** /cool/aligntags | Create a new AlignCoolTag to database.
[**find_align_cool_tags**](CoolApi.md#find_align_cool_tags) | **GET** /cool/aligntags | Queries for align cool tags
[**find_cool_iovs**](CoolApi.md#find_cool_iovs) | **GET** /cool/iovs | Finds an AlignCoolIovViewSetDto by tag name
[**find_iovs_to_migrate**](CoolApi.md#find_iovs_to_migrate) | **GET** /cool/iovs/migration | Finds a set of AligniovDto that should be migrated from an aligntag into a cooltag
[**find_last_cool_iov**](CoolApi.md#find_last_cool_iov) | **GET** /cool/iovs/last | Finds an AlignCoolIovViewSetDto by tag name
[**get_align_cool_tag**](CoolApi.md#get_align_cool_tag) | **GET** /cool/aligntags/{cooltag}/{aligntag} | Fetch the AlignCoolTag identified by the pair {cooltag} / {aligntag}
[**get_cool_payload**](CoolApi.md#get_cool_payload) | **GET** /cool/payload/{pk} | 
[**get_request_for_cool**](CoolApi.md#get_request_for_cool) | **GET** /cool/proxy/{iovid} | request the generation of a JSON request file containing one iov and a payload
[**update_align_cool_tag**](CoolApi.md#update_align_cool_tag) | **PUT** /cool/aligntags/{cooltag}/{aligntag} | Updates an AlignCoolTag, identified by the pair {cooltag} / {aligntag}
[**upload_to_cool**](CoolApi.md#upload_to_cool) | **POST** /cool/proxy/{iovid} | request the upload of one iov into COOL


# **create_align_cool_tag**
> AlignCoolTagDto create_align_cool_tag()

Create a new AlignCoolTag to database.

This method allows to insert AlignCoolTag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.align_cool_tag_dto import AlignCoolTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    align_cool_tag_dto = AlignCoolTagDto(
        cool_tag="cool_tag_example",
        align_tag="align_tag_example",
        tag_comment="tag_comment_example",
        coollock="coollock_example",
        do_upload_on_reco=True,
        cherrypy_config="cherrypy_config_example",
        tag_type="tag_type_example",
        cool_config=CoolConfigDto(
            system_type="system_type_example",
            db_name="db_name_example",
            schema_name="schema_name_example",
            folder_name="folder_name_example",
            db_cluster="db_cluster_example",
            server_url_get="server_url_get_example",
            server_url_put="server_url_put_example",
            channel="channel_example",
            active="active_example",
        ),
    ) # AlignCoolTagDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new AlignCoolTag to database.
        api_response = api_instance.create_align_cool_tag(align_cool_tag_dto=align_cool_tag_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->create_align_cool_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **align_cool_tag_dto** | [**AlignCoolTagDto**](AlignCoolTagDto.md)|  | [optional]

### Return type

[**AlignCoolTagDto**](AlignCoolTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_cool_tags**
> AlignCoolTagSetDto find_align_cool_tags()

Queries for align cool tags

Search align cool tags with multiple criteria

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_cool_tag_set_dto import AlignCoolTagSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    align_tag = "alignTag_example" # str | the search field (optional)
    align_tag_like = "alignTagLike_example" # str | the search field (optional)
    cool_tag = "coolTag_example" # str | the search field (optional)
    cool_tag_like = "coolTagLike_example" # str | the search field (optional)
    tag_type = "tagType_example" # str | the search field (optional)
    do_upload_on_reco = True # bool | the search field (optional)
    cool_lock = "coolLock_example" # str | the search field (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Queries for align cool tags
        api_response = api_instance.find_align_cool_tags(align_tag=align_tag, align_tag_like=align_tag_like, cool_tag=cool_tag, cool_tag_like=cool_tag_like, tag_type=tag_type, do_upload_on_reco=do_upload_on_reco, cool_lock=cool_lock)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_align_cool_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **align_tag** | **str**| the search field | [optional]
 **align_tag_like** | **str**| the search field | [optional]
 **cool_tag** | **str**| the search field | [optional]
 **cool_tag_like** | **str**| the search field | [optional]
 **tag_type** | **str**| the search field | [optional]
 **do_upload_on_reco** | **bool**| the search field | [optional]
 **cool_lock** | **str**| the search field | [optional]

### Return type

[**AlignCoolTagSetDto**](AlignCoolTagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_cool_iovs**
> AlignCoolIovViewSetDto find_cool_iovs()

Finds an AlignCoolIovViewSetDto by tag name

Finds an AlignCoolIovViewSetDto by tag name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.align_cool_iov_view_set_dto import AlignCoolIovViewSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    schema = "schema_example" # str | the cool schema (optional)
    db = "db_example" # str | the cool db name (optional)
    folder = "folder_example" # str | the cool folder name (optional)
    cool_tag = "coolTag_example" # str | the cool TAG name (optional)
    align_iov = 1 # int | an alignIov ID to query (optional)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since field (optional)
    until = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the until field (optional)
    page = 1 # int | the page number (optional)
    size = 1 # int | the page size (optional)
    sort = "iovSince:ASC" # str | the sort field (optional) if omitted the server will use the default value of "iovSince:ASC"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an AlignCoolIovViewSetDto by tag name
        api_response = api_instance.find_cool_iovs(schema=schema, db=db, folder=folder, cool_tag=cool_tag, align_iov=align_iov, since=since, until=until, page=page, size=size, sort=sort)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_cool_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **schema** | **str**| the cool schema | [optional]
 **db** | **str**| the cool db name | [optional]
 **folder** | **str**| the cool folder name | [optional]
 **cool_tag** | **str**| the cool TAG name | [optional]
 **align_iov** | **int**| an alignIov ID to query | [optional]
 **since** | **datetime**| the since field | [optional]
 **until** | **datetime**| the until field | [optional]
 **page** | **int**| the page number | [optional]
 **size** | **int**| the page size | [optional]
 **sort** | **str**| the sort field | [optional] if omitted the server will use the default value of "iovSince:ASC"

### Return type

[**AlignCoolIovViewSetDto**](AlignCoolIovViewSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_iovs_to_migrate**
> AligniovBaseSetDto find_iovs_to_migrate(aligntag)

Finds a set of AligniovDto that should be migrated from an aligntag into a cooltag

Finds list of AligniovDto from tag name to cool tag name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.aligniov_base_set_dto import AligniovBaseSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    aligntag = "aligntag_example" # str | the source align tag
    cooltag = "none" # str | the destination cool tag name (optional) if omitted the server will use the default value of "none"

    # example passing only required values which don't have defaults set
    try:
        # Finds a set of AligniovDto that should be migrated from an aligntag into a cooltag
        api_response = api_instance.find_iovs_to_migrate(aligntag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_iovs_to_migrate: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds a set of AligniovDto that should be migrated from an aligntag into a cooltag
        api_response = api_instance.find_iovs_to_migrate(aligntag, cooltag=cooltag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_iovs_to_migrate: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aligntag** | **str**| the source align tag |
 **cooltag** | **str**| the destination cool tag name | [optional] if omitted the server will use the default value of "none"

### Return type

[**AligniovBaseSetDto**](AligniovBaseSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_last_cool_iov**
> AlignCoolIovViewDto find_last_cool_iov(folder, tag)

Finds an AlignCoolIovViewSetDto by tag name

Finds an AlignCoolIovViewSetDto by tag name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_cool_iov_view_dto import AlignCoolIovViewDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    folder = "folder_example" # str | the cool folder name
    tag = "tag_example" # str | the cool tag search field
    schema = "ATLAS_COOLOFL_MUONALIGN" # str | the cool schema (optional) if omitted the server will use the default value of "ATLAS_COOLOFL_MUONALIGN"
    db = "CONDBR2" # str | the cool db name (optional) if omitted the server will use the default value of "CONDBR2"

    # example passing only required values which don't have defaults set
    try:
        # Finds an AlignCoolIovViewSetDto by tag name
        api_response = api_instance.find_last_cool_iov(folder, tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_last_cool_iov: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an AlignCoolIovViewSetDto by tag name
        api_response = api_instance.find_last_cool_iov(folder, tag, schema=schema, db=db)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->find_last_cool_iov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **folder** | **str**| the cool folder name |
 **tag** | **str**| the cool tag search field |
 **schema** | **str**| the cool schema | [optional] if omitted the server will use the default value of "ATLAS_COOLOFL_MUONALIGN"
 **db** | **str**| the cool db name | [optional] if omitted the server will use the default value of "CONDBR2"

### Return type

[**AlignCoolIovViewDto**](AlignCoolIovViewDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_align_cool_tag**
> AlignCoolTagDto get_align_cool_tag(cooltag, aligntag)

Fetch the AlignCoolTag identified by the pair {cooltag} / {aligntag}

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.align_cool_tag_dto import AlignCoolTagDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    cooltag = "cooltag_example" # str | the cool field
    aligntag = "aligntag_example" # str | the align tag

    # example passing only required values which don't have defaults set
    try:
        # Fetch the AlignCoolTag identified by the pair {cooltag} / {aligntag}
        api_response = api_instance.get_align_cool_tag(cooltag, aligntag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->get_align_cool_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cooltag** | **str**| the cool field |
 **aligntag** | **str**| the align tag |

### Return type

[**AlignCoolTagDto**](AlignCoolTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_cool_payload**
> file_type get_cool_payload(pk)



Download the payload of a COOL IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    pk = "pk_example" # str | The pk of an AlignCoolIovViewDto

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_cool_payload(pk)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->get_cool_payload: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pk** | **str**| The pk of an AlignCoolIovViewDto |

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_request_for_cool**
> file_type get_request_for_cool(iovid, cooltag)

request the generation of a JSON request file containing one iov and a payload

get back the request JSON file for cool upload using the input IOV ID.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    iovid = 1 # int | the IOV ID
    cooltag = "cooltag_example" # str | the cooltag
    check_last_iov = 1 # int | an alignIOV ID to be checked against the last cool IOV of this tag (optional)

    # example passing only required values which don't have defaults set
    try:
        # request the generation of a JSON request file containing one iov and a payload
        api_response = api_instance.get_request_for_cool(iovid, cooltag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->get_request_for_cool: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # request the generation of a JSON request file containing one iov and a payload
        api_response = api_instance.get_request_for_cool(iovid, cooltag, check_last_iov=check_last_iov)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->get_request_for_cool: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IOV ID |
 **cooltag** | **str**| the cooltag |
 **check_last_iov** | **int**| an alignIOV ID to be checked against the last cool IOV of this tag | [optional]

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_align_cool_tag**
> AlignCoolTagDto update_align_cool_tag(cooltag, aligntag)

Updates an AlignCoolTag, identified by the pair {cooltag} / {aligntag}

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.align_cool_tag_dto import AlignCoolTagDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    cooltag = "cooltag_example" # str | the cool field
    aligntag = "aligntag_example" # str | the align tag
    align_cool_tag_dto = AlignCoolTagDto(
        cool_tag="cool_tag_example",
        align_tag="align_tag_example",
        tag_comment="tag_comment_example",
        coollock="coollock_example",
        do_upload_on_reco=True,
        cherrypy_config="cherrypy_config_example",
        tag_type="tag_type_example",
        cool_config=CoolConfigDto(
            system_type="system_type_example",
            db_name="db_name_example",
            schema_name="schema_name_example",
            folder_name="folder_name_example",
            db_cluster="db_cluster_example",
            server_url_get="server_url_get_example",
            server_url_put="server_url_put_example",
            channel="channel_example",
            active="active_example",
        ),
    ) # AlignCoolTagDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates an AlignCoolTag, identified by the pair {cooltag} / {aligntag}
        api_response = api_instance.update_align_cool_tag(cooltag, aligntag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->update_align_cool_tag: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates an AlignCoolTag, identified by the pair {cooltag} / {aligntag}
        api_response = api_instance.update_align_cool_tag(cooltag, aligntag, align_cool_tag_dto=align_cool_tag_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->update_align_cool_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cooltag** | **str**| the cool field |
 **aligntag** | **str**| the align tag |
 **align_cool_tag_dto** | [**AlignCoolTagDto**](AlignCoolTagDto.md)|  | [optional]

### Return type

[**AlignCoolTagDto**](AlignCoolTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_to_cool**
> file_type upload_to_cool(iovid, cooltag)

request the upload of one iov into COOL

get back the json request send to cool-proxy.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import cool_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = cool_api.CoolApi(api_client)
    iovid = 1 # int | the IOV ID
    cooltag = "cooltag_example" # str | the cooltag
    check_last_iov = 1 # int | an alignIOV ID to be checked against the last cool IOV of this tag (optional)

    # example passing only required values which don't have defaults set
    try:
        # request the upload of one iov into COOL
        api_response = api_instance.upload_to_cool(iovid, cooltag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->upload_to_cool: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # request the upload of one iov into COOL
        api_response = api_instance.upload_to_cool(iovid, cooltag, check_last_iov=check_last_iov)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling CoolApi->upload_to_cool: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IOV ID |
 **cooltag** | **str**| the cooltag |
 **check_last_iov** | **int**| an alignIOV ID to be checked against the last cool IOV of this tag | [optional]

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

