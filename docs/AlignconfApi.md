# openapi_client.AlignconfApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**find_reco_conf**](AlignconfApi.md#find_reco_conf) | **GET** /alignconf | Retrieves a reconstruction configuration based on tag name search
[**get_element_conf**](AlignconfApi.md#get_element_conf) | **GET** /alignconf/{tag}/elements | Retrieves the element configuration for a given tag
[**get_optical_line_conf**](AlignconfApi.md#get_optical_line_conf) | **GET** /alignconf/{tag}/opticallines | Retrieves the optical line configuration for a given tag
[**get_optical_line_offsets**](AlignconfApi.md#get_optical_line_offsets) | **GET** /alignconf/oloffset/{tag} | Retrieves the optical line offsets for a given offset tag
[**get_reco_conf**](AlignconfApi.md#get_reco_conf) | **GET** /alignconf/{tag} | Retrieves a reconstruction configuration
[**get_temperature_sensor_conf**](AlignconfApi.md#get_temperature_sensor_conf) | **GET** /alignconf/{tag}/tsensors | Retrieves the T sensor configuration for a given tag
[**get_tsensor_offsets**](AlignconfApi.md#get_tsensor_offsets) | **GET** /alignconf/tsoffset/{tag} | Retrieves the T sensor offsets for a given offset tag
[**post_element_conf**](AlignconfApi.md#post_element_conf) | **POST** /alignconf/{tag}/elements | Sets the element configuration for a given tag
[**post_optical_line_conf**](AlignconfApi.md#post_optical_line_conf) | **POST** /alignconf/{tag}/opticallines | Sets the optical lines configuration for a given tag
[**post_optical_line_offsets**](AlignconfApi.md#post_optical_line_offsets) | **POST** /alignconf/oloffset/{tag} | Sets the optical line offsets for a given offset tag
[**post_temperature_sensor_conf**](AlignconfApi.md#post_temperature_sensor_conf) | **POST** /alignconf/{tag}/tsensors | Sets the T sensor configuration for a given tag
[**post_tsensor_offsets**](AlignconfApi.md#post_tsensor_offsets) | **POST** /alignconf/tsoffset/{tag} | Sets the T sensor offsets for a given offset tag
[**update_reco_conf**](AlignconfApi.md#update_reco_conf) | **PUT** /alignconf/{tag} | Updates a reconstruction configuration


# **find_reco_conf**
> AlignRecoConfSetDto find_reco_conf(tag)

Retrieves a reconstruction configuration based on tag name search

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_conf_set_dto import AlignRecoConfSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name or a pattern (use like in query)
    activated_online = True # bool | the flag to search tags activated or not (optional) if omitted the server will use the default value of True

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a reconstruction configuration based on tag name search
        api_response = api_instance.find_reco_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->find_reco_conf: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a reconstruction configuration based on tag name search
        api_response = api_instance.find_reco_conf(tag, activated_online=activated_online)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->find_reco_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name or a pattern (use like in query) |
 **activated_online** | **bool**| the flag to search tags activated or not | [optional] if omitted the server will use the default value of True

### Return type

[**AlignRecoConfSetDto**](AlignRecoConfSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_element_conf**
> AlignRecoElementConfSetDto get_element_conf(tag)

Retrieves the element configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_element_conf_set_dto import AlignRecoElementConfSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the element configuration for a given tag
        api_response = api_instance.get_element_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_element_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoElementConfSetDto**](AlignRecoElementConfSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_optical_line_conf**
> AlignRecoOpticalLineConfSetDto get_optical_line_conf(tag)

Retrieves the optical line configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_reco_optical_line_conf_set_dto import AlignRecoOpticalLineConfSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the optical line configuration for a given tag
        api_response = api_instance.get_optical_line_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_optical_line_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoOpticalLineConfSetDto**](AlignRecoOpticalLineConfSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_optical_line_offsets**
> AlignRecoOpticalLineOffsetSetDto get_optical_line_offsets(tag)

Retrieves the optical line offsets for a given offset tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_optical_line_offset_set_dto import AlignRecoOpticalLineOffsetSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the optical line offsets for a given offset tag
        api_response = api_instance.get_optical_line_offsets(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_optical_line_offsets: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoOpticalLineOffsetSetDto**](AlignRecoOpticalLineOffsetSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reco_conf**
> AlignRecoConfDto get_reco_conf(tag)

Retrieves a reconstruction configuration

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_conf_dto import AlignRecoConfDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a reconstruction configuration
        api_response = api_instance.get_reco_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_reco_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoConfDto**](AlignRecoConfDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_temperature_sensor_conf**
> AlignRecoTempSensorConfSetDto get_temperature_sensor_conf(tag)

Retrieves the T sensor configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_temp_sensor_conf_set_dto import AlignRecoTempSensorConfSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the T sensor configuration for a given tag
        api_response = api_instance.get_temperature_sensor_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_temperature_sensor_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoTempSensorConfSetDto**](AlignRecoTempSensorConfSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tsensor_offsets**
> AlignRecoTsensorOffsetSetDto get_tsensor_offsets(tag)

Retrieves the T sensor offsets for a given offset tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_tsensor_offset_set_dto import AlignRecoTsensorOffsetSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the T sensor offsets for a given offset tag
        api_response = api_instance.get_tsensor_offsets(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->get_tsensor_offsets: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |

### Return type

[**AlignRecoTsensorOffsetSetDto**](AlignRecoTsensorOffsetSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_element_conf**
> post_element_conf(tag, align_reco_element_conf_dto)

Sets the element configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_element_conf_dto import AlignRecoElementConfDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_element_conf_dto = [
        AlignRecoElementConfDto(
            tag="tag_example",
            element_id="element_id_example",
            activated=True,
        ),
    ] # [AlignRecoElementConfDto] | 

    # example passing only required values which don't have defaults set
    try:
        # Sets the element configuration for a given tag
        api_instance.post_element_conf(tag, align_reco_element_conf_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->post_element_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_element_conf_dto** | [**[AlignRecoElementConfDto]**](AlignRecoElementConfDto.md)|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The element configuration was successfully saved |  -  |
**409** | Element configuration already exists for this tag |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_optical_line_conf**
> post_optical_line_conf(tag, align_reco_optical_line_conf_dto)

Sets the optical lines configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_optical_line_conf_dto import AlignRecoOpticalLineConfDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_optical_line_conf_dto = [
        AlignRecoOpticalLineConfDto(
            tag="tag_example",
            element_id="element_id_example",
            activated=True,
            absolute=True,
        ),
    ] # [AlignRecoOpticalLineConfDto] | 

    # example passing only required values which don't have defaults set
    try:
        # Sets the optical lines configuration for a given tag
        api_instance.post_optical_line_conf(tag, align_reco_optical_line_conf_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->post_optical_line_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_optical_line_conf_dto** | [**[AlignRecoOpticalLineConfDto]**](AlignRecoOpticalLineConfDto.md)|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The optical line configuration was successfully saved |  -  |
**409** | Optcial line configuration already exists for this tag |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_optical_line_offsets**
> post_optical_line_offsets(tag, align_reco_optical_line_offset_dto)

Sets the optical line offsets for a given offset tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_optical_line_offset_dto import AlignRecoOpticalLineOffsetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_optical_line_offset_dto = [
        AlignRecoOpticalLineOffsetDto(
            tag="tag_example",
            element_id="element_id_example",
            xvalue=3.14,
            yvalue=3.14,
            scale=3.14,
            rotz=3.14,
            err_x=3.14,
            err_y=3.14,
            err_scale=3.14,
            err_rotz=3.14,
        ),
    ] # [AlignRecoOpticalLineOffsetDto] | 

    # example passing only required values which don't have defaults set
    try:
        # Sets the optical line offsets for a given offset tag
        api_instance.post_optical_line_offsets(tag, align_reco_optical_line_offset_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->post_optical_line_offsets: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_optical_line_offset_dto** | [**[AlignRecoOpticalLineOffsetDto]**](AlignRecoOpticalLineOffsetDto.md)|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The offset configuration was successfully saved |  -  |
**409** | The offset configuration already exists for this tag |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_temperature_sensor_conf**
> post_temperature_sensor_conf(tag, align_reco_temp_sensor_conf_dto)

Sets the T sensor configuration for a given tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_temp_sensor_conf_dto import AlignRecoTempSensorConfDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_temp_sensor_conf_dto = [
        AlignRecoTempSensorConfDto(
            tag="tag_example",
            element_id="element_id_example",
            activated=True,
            absolute=True,
        ),
    ] # [AlignRecoTempSensorConfDto] | 

    # example passing only required values which don't have defaults set
    try:
        # Sets the T sensor configuration for a given tag
        api_instance.post_temperature_sensor_conf(tag, align_reco_temp_sensor_conf_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->post_temperature_sensor_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_temp_sensor_conf_dto** | [**[AlignRecoTempSensorConfDto]**](AlignRecoTempSensorConfDto.md)|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The T sensor configuration was successfully saved |  -  |
**409** | T sensor configuration already exists for this tag |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_tsensor_offsets**
> post_tsensor_offsets(tag, align_reco_tsensor_offset_dto)

Sets the T sensor offsets for a given offset tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_tsensor_offset_dto import AlignRecoTsensorOffsetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_tsensor_offset_dto = [
        AlignRecoTsensorOffsetDto(
            tag="tag_example",
            element_id="element_id_example",
            temp=3.14,
            err_temp=3.14,
        ),
    ] # [AlignRecoTsensorOffsetDto] | 

    # example passing only required values which don't have defaults set
    try:
        # Sets the T sensor offsets for a given offset tag
        api_instance.post_tsensor_offsets(tag, align_reco_tsensor_offset_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->post_tsensor_offsets: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_tsensor_offset_dto** | [**[AlignRecoTsensorOffsetDto]**](AlignRecoTsensorOffsetDto.md)|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The offset configuration was successfully saved |  -  |
**409** | The offset configuration already exists for this tag |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_reco_conf**
> AlignRecoConfDto update_reco_conf(tag)

Updates a reconstruction configuration

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignconf_api
from openapi_client.model.align_reco_conf_update_dto import AlignRecoConfUpdateDto
from openapi_client.model.align_reco_conf_dto import AlignRecoConfDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignconf_api.AlignconfApi(api_client)
    tag = "tag_example" # str | the Aligntag name
    align_reco_conf_update_dto = AlignRecoConfUpdateDto(
        tag_description="tag_description_example",
        activated_online=True,
        online_lookback_minutes=1,
    ) # AlignRecoConfUpdateDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates a reconstruction configuration
        api_response = api_instance.update_reco_conf(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->update_reco_conf: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates a reconstruction configuration
        api_response = api_instance.update_reco_conf(tag, align_reco_conf_update_dto=align_reco_conf_update_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignconfApi->update_reco_conf: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the Aligntag name |
 **align_reco_conf_update_dto** | [**AlignRecoConfUpdateDto**](AlignRecoConfUpdateDto.md)|  | [optional]

### Return type

[**AlignRecoConfDto**](AlignRecoConfDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

