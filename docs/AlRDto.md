# AlRDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**AlIdDto**](AlIdDto.md) |  | [optional] 
**cycle_number** | **int** |  | [optional] 
**x** | **float** |  | [optional] 
**y** | **float** |  | [optional] 
**mag** | **float** |  | [optional] 
**rdm** | **float** |  | [optional] 
**rro** | **float** |  | [optional] 
**rdx** | **float** |  | [optional] 
**rsq** | **float** |  | [optional] 
**ror** | **float** |  | [optional] 
**rrf** | **float** |  | [optional] 
**rex** | **float** |  | [optional] 
**rav** | **float** |  | [optional] 
**rst** | **float** |  | [optional] 
**rmx** | **float** |  | [optional] 
**rmn** | **float** |  | [optional] 
**rec** | **float** |  | [optional] 
**rel** | **float** |  | [optional] 
**rco** | **float** |  | [optional] 
**rln** | **float** |  | [optional] 
**rdn** | **float** |  | [optional] 
**rsh** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


