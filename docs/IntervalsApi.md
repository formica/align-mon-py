# openapi_client.IntervalsApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_interval_maker_configuration**](IntervalsApi.md#create_interval_maker_configuration) | **POST** /intervals/configurations | Create a new IntervalMakerConfiguration.
[**create_interval_maker_iovs**](IntervalsApi.md#create_interval_maker_iovs) | **POST** /intervals/iovs | Create IntervalMakerIovSetDto
[**create_interval_maker_tag**](IntervalsApi.md#create_interval_maker_tag) | **POST** /intervals/tags | Create a new IntervalMakerTag.
[**find_interval_maker_configurations**](IntervalsApi.md#find_interval_maker_configurations) | **GET** /intervals/configurations | Finds an IntervalMakerConfigurationSetDto
[**find_interval_maker_iovs**](IntervalsApi.md#find_interval_maker_iovs) | **GET** /intervals/iovs | Finds an IntervalMakerIovSetDto
[**find_interval_maker_tags**](IntervalsApi.md#find_interval_maker_tags) | **GET** /intervals/tags | Finds an IntervalMakerTagSetDto


# **create_interval_maker_configuration**
> IntervalMakerConfDto create_interval_maker_configuration()

Create a new IntervalMakerConfiguration.

This method allows to insert IntervalMakerConfiguration.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.interval_maker_conf_dto import IntervalMakerConfDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    interval_maker_conf_dto = IntervalMakerConfDto() # IntervalMakerConfDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new IntervalMakerConfiguration.
        api_response = api_instance.create_interval_maker_configuration(interval_maker_conf_dto=interval_maker_conf_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->create_interval_maker_configuration: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **interval_maker_conf_dto** | [**IntervalMakerConfDto**](IntervalMakerConfDto.md)|  | [optional]

### Return type

[**IntervalMakerConfDto**](IntervalMakerConfDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_interval_maker_iovs**
> IntervalMakerIovSetDto create_interval_maker_iovs(since, till, tag)

Create IntervalMakerIovSetDto

Create an IntervalMakerIovSetDto

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.interval_maker_iov_set_dto import IntervalMakerIovSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time as string
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the until time as string
    tag = "tag_example" # str | the IntervalMaker tag name
    mode = "compute" # str | the search mode (optional) if omitted the server will use the default value of "compute"

    # example passing only required values which don't have defaults set
    try:
        # Create IntervalMakerIovSetDto
        api_response = api_instance.create_interval_maker_iovs(since, till, tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->create_interval_maker_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create IntervalMakerIovSetDto
        api_response = api_instance.create_interval_maker_iovs(since, till, tag, mode=mode)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->create_interval_maker_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the since time as string |
 **till** | **datetime**| the until time as string |
 **tag** | **str**| the IntervalMaker tag name |
 **mode** | **str**| the search mode | [optional] if omitted the server will use the default value of "compute"

### Return type

[**IntervalMakerIovSetDto**](IntervalMakerIovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_interval_maker_tag**
> IntervalMakerTagDto create_interval_maker_tag()

Create a new IntervalMakerTag.

This method allows to insert IntervalMakerTag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.interval_maker_tag_dto import IntervalMakerTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    interval_maker_tag_dto = IntervalMakerTagDto(
        tag_id=1,
        tag_name="tag_name_example",
        tag_description="tag_description_example",
        tag_insertion=dateutil_parser('1970-01-01T00:00:00.00Z'),
    ) # IntervalMakerTagDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new IntervalMakerTag.
        api_response = api_instance.create_interval_maker_tag(interval_maker_tag_dto=interval_maker_tag_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->create_interval_maker_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **interval_maker_tag_dto** | [**IntervalMakerTagDto**](IntervalMakerTagDto.md)|  | [optional]

### Return type

[**IntervalMakerTagDto**](IntervalMakerTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_interval_maker_configurations**
> IntervalMakerConfigurationSetDto find_interval_maker_configurations()

Finds an IntervalMakerConfigurationSetDto

Finds an IntervalMakerConfigurationSetDto

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.interval_maker_configuration_set_dto import IntervalMakerConfigurationSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    tag = "DEFAULT" # str | the IntervalMaker tag name (optional) if omitted the server will use the default value of "DEFAULT"
    mode = "mode_example" # str | the search mode (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an IntervalMakerConfigurationSetDto
        api_response = api_instance.find_interval_maker_configurations(tag=tag, mode=mode)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->find_interval_maker_configurations: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the IntervalMaker tag name | [optional] if omitted the server will use the default value of "DEFAULT"
 **mode** | **str**| the search mode | [optional]

### Return type

[**IntervalMakerConfigurationSetDto**](IntervalMakerConfigurationSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_interval_maker_iovs**
> IntervalMakerIovSetDto find_interval_maker_iovs(since, till, tag)

Finds an IntervalMakerIovSetDto

Finds an IntervalMakerIovSetDto

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.interval_maker_iov_set_dto import IntervalMakerIovSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time as string
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the until time as string
    tag = "tag_example" # str | the IntervalMaker tag name
    mode = "history" # str | the search mode (optional) if omitted the server will use the default value of "history"

    # example passing only required values which don't have defaults set
    try:
        # Finds an IntervalMakerIovSetDto
        api_response = api_instance.find_interval_maker_iovs(since, till, tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->find_interval_maker_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an IntervalMakerIovSetDto
        api_response = api_instance.find_interval_maker_iovs(since, till, tag, mode=mode)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->find_interval_maker_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the since time as string |
 **till** | **datetime**| the until time as string |
 **tag** | **str**| the IntervalMaker tag name |
 **mode** | **str**| the search mode | [optional] if omitted the server will use the default value of "history"

### Return type

[**IntervalMakerIovSetDto**](IntervalMakerIovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_interval_maker_tags**
> IntervalMakerTagSetDto find_interval_maker_tags()

Finds an IntervalMakerTagSetDto

Finds an IntervalMakerTagSetDto

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import intervals_api
from openapi_client.model.interval_maker_tag_set_dto import IntervalMakerTagSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = intervals_api.IntervalsApi(api_client)
    tag = "*" # str | the IntervalMaker tag name (optional) if omitted the server will use the default value of "*"
    mode = "mode_example" # str | the search mode (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an IntervalMakerTagSetDto
        api_response = api_instance.find_interval_maker_tags(tag=tag, mode=mode)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IntervalsApi->find_interval_maker_tags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the IntervalMaker tag name | [optional] if omitted the server will use the default value of "*"
 **mode** | **str**| the search mode | [optional]

### Return type

[**IntervalMakerTagSetDto**](IntervalMakerTagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

