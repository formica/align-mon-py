# AsapOptlineDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**asap_id** | **str** |  | [optional] 
**oltype** | **str** |  | [optional] 
**rastyp** | **str** |  | [optional] 
**ccd** | **str** |  | [optional] 
**lens** | **str** |  | [optional] 
**mask** | **str** |  | [optional] 
**cx** | **int** |  | [optional] 
**cy** | **int** |  | [optional] 
**cz** | **int** |  | [optional] 
**mx** | **int** |  | [optional] 
**my** | **int** |  | [optional] 
**mz** | **int** |  | [optional] 
**activate** | **int** |  | [optional] 
**calibrate** | **int** |  | [optional] 
**absolute** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


