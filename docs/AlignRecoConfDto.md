# AlignRecoConfDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **str** |  | 
**config_file_name** | **str** |  | 
**save_to_tree** | **bool** |  | 
**save_to_db** | **bool** |  | 
**save_to_cool** | **bool** |  | 
**activated_online** | **bool** |  | 
**online_lookback_minutes** | **int** |  | 
**iterations** | **int** |  | 
**tag_description** | **str** |  | [optional] 
**im_conf** | [**IntervalMakerConfDto**](IntervalMakerConfDto.md) |  | [optional] 
**cooltag** | **str** |  | [optional] 
**amdb_file** | **str** |  | [optional] 
**offset_tag** | **str** |  | [optional] 
**mask_tag** | **str** |  | [optional] 
**icaras_query_length** | **int** |  | [optional] 
**sag_stability_config** | [**SagittaStabilityConfigurationDto**](SagittaStabilityConfigurationDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


