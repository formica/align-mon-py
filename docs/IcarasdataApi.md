# openapi_client.IcarasdataApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fill_icdata_avg**](IcarasdataApi.md#fill_icdata_avg) | **POST** /icarasdata/averages | Launch Icaras data processing task
[**get_daq_status_summary_by_server**](IcarasdataApi.md#get_daq_status_summary_by_server) | **GET** /icarasdata/daqsummary/byserver | Retrieve a DAQ status summary by server
[**get_icaras_history**](IcarasdataApi.md#get_icaras_history) | **GET** /icarasdata | Retrieves Icaras data history
[**get_last_time**](IcarasdataApi.md#get_last_time) | **GET** /icarasdata/last | Retrieves Icaras data or average last since time


# **fill_icdata_avg**
> HTTPResponse fill_icdata_avg()

Launch Icaras data processing task

Compute Icaras average using interval in input 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import icarasdata_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.icaras_avg_request_post_dto import IcarasAvgRequestPostDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = icarasdata_api.IcarasdataApi(api_client)
    icaras_avg_request_post_dto = IcarasAvgRequestPostDto(
        since=1,
        until=1,
        olname="olname_example",
    ) # IcarasAvgRequestPostDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Launch Icaras data processing task
        api_response = api_instance.fill_icdata_avg(icaras_avg_request_post_dto=icaras_avg_request_post_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IcarasdataApi->fill_icdata_avg: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **icaras_avg_request_post_dto** | [**IcarasAvgRequestPostDto**](IcarasAvgRequestPostDto.md)|  | [optional]

### Return type

[**HTTPResponse**](HTTPResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | icaras data processing is completed |  -  |
**404** | Not found |  -  |
**500** | exception in procedure, send error message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_daq_status_summary_by_server**
> DaqSummarySetDto get_daq_status_summary_by_server()

Retrieve a DAQ status summary by server

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import icarasdata_api
from openapi_client.model.daq_summary_set_dto import DaqSummarySetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = icarasdata_api.IcarasdataApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the report (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the report (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieve a DAQ status summary by server
        api_response = api_instance.get_daq_status_summary_by_server(since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IcarasdataApi->get_daq_status_summary_by_server: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the start time of the report | [optional]
 **till** | **datetime**| the end time of the report | [optional]

### Return type

[**DaqSummarySetDto**](DaqSummarySetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_icaras_history**
> IcarasdataExtendedSetDto get_icaras_history(since, till)

Retrieves Icaras data history

Retrieves Icaras data history, with parameterizable source, error query and optional sensor list 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import icarasdata_api
from openapi_client.model.icarasdata_extended_set_dto import IcarasdataExtendedSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = icarasdata_api.IcarasdataApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the query
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the query
    icaras_source = "icarasdata" # str | The datasource to retrieve Icaras data  * `icarasdata` - the main ICARASDATA table  * `icaras_avg` - Convert values from ALIGN_ICAVG_REGR to IcarasData (approximately 1 data point per channel every 6 hours)  * `oltestlines` - Convert values from ALIGN_OLTESTLINES to IcarasData (approximately 1 data point per channel every 24 hours)  (optional) if omitted the server will use the default value of "icarasdata"
    query_errors = "none" # str | The type of error query to perform  * `none` - Do not perform additional error query  * `from_icavg_regr` - Retrieve pre-computed errors from the ALIGN_ICAVG_REGR table  (optional) if omitted the server will use the default value of "none"
    sensors = [] # [str] | an (optional) list of sensors to query. I empty, retrieves all sensors (optional) if omitted the server will use the default value of []

    # example passing only required values which don't have defaults set
    try:
        # Retrieves Icaras data history
        api_response = api_instance.get_icaras_history(since, till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IcarasdataApi->get_icaras_history: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves Icaras data history
        api_response = api_instance.get_icaras_history(since, till, icaras_source=icaras_source, query_errors=query_errors, sensors=sensors)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IcarasdataApi->get_icaras_history: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the start time of the query |
 **till** | **datetime**| the end time of the query |
 **icaras_source** | **str**| The datasource to retrieve Icaras data  * &#x60;icarasdata&#x60; - the main ICARASDATA table  * &#x60;icaras_avg&#x60; - Convert values from ALIGN_ICAVG_REGR to IcarasData (approximately 1 data point per channel every 6 hours)  * &#x60;oltestlines&#x60; - Convert values from ALIGN_OLTESTLINES to IcarasData (approximately 1 data point per channel every 24 hours)  | [optional] if omitted the server will use the default value of "icarasdata"
 **query_errors** | **str**| The type of error query to perform  * &#x60;none&#x60; - Do not perform additional error query  * &#x60;from_icavg_regr&#x60; - Retrieve pre-computed errors from the ALIGN_ICAVG_REGR table  | [optional] if omitted the server will use the default value of "none"
 **sensors** | **[str]**| an (optional) list of sensors to query. I empty, retrieves all sensors | [optional] if omitted the server will use the default value of []

### Return type

[**IcarasdataExtendedSetDto**](IcarasdataExtendedSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_last_time**
> GenericMap get_last_time()

Retrieves Icaras data or average last since time

Retrieves Icaras data or Icaras average last since time 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import icarasdata_api
from openapi_client.model.generic_map import GenericMap
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = icarasdata_api.IcarasdataApi(api_client)
    icaras_source = "icarasdata" # str | The datasource to retrieve Icaras data  * `icaras` - the main ICARASDATA table  * `icaras_avg` - the ALIGN_ICAVG_REGR  table  (optional) if omitted the server will use the default value of "icarasdata"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves Icaras data or average last since time
        api_response = api_instance.get_last_time(icaras_source=icaras_source)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling IcarasdataApi->get_last_time: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **icaras_source** | **str**| The datasource to retrieve Icaras data  * &#x60;icaras&#x60; - the main ICARASDATA table  * &#x60;icaras_avg&#x60; - the ALIGN_ICAVG_REGR  table  | [optional] if omitted the server will use the default value of "icarasdata"

### Return type

[**GenericMap**](GenericMap.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

