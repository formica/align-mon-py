# IntervalMakerIovDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**since** | **datetime** |  | [optional] 
**till** | **datetime** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**is_leak** | **bool** |  | [optional] 
**is_done** | **bool** |  | [optional] 
**is_floating** | **bool** |  | [optional] 
**is_stable** | **bool** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


