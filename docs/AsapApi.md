# openapi_client.AsapApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**calc_max_sagitta_diff**](AsapApi.md#calc_max_sagitta_diff) | **GET** /asap/sagittaDiff | Computes the max sagitta difference between two IOVs
[**finish_reco_client**](AsapApi.md#finish_reco_client) | **DELETE** /asap/recoclient/{clientId} | Declares a reconstruction client as FINISHED
[**get_reco_client**](AsapApi.md#get_reco_client) | **GET** /asap/recoclient/{clientId} | Retrieves the metadata about an alignment reconstruction client
[**get_reco_clients**](AsapApi.md#get_reco_clients) | **GET** /asap/recoclient | Retrieves the list of reconstruction clients
[**make_empty_iovs**](AsapApi.md#make_empty_iovs) | **PUT** /asap/emptyIovMaker/{tag} | Execute interval maker on tag and return the empty IOVs
[**make_stable_iovs**](AsapApi.md#make_stable_iovs) | **POST** /asap/stableIovMaker/{tag} | Loops on the latest reconstructed IOVs of tag and computes the reco status flag STABLE or RELEASED_FOR_CONDDB
[**register_reco_client**](AsapApi.md#register_reco_client) | **POST** /asap/recoclient | Register an alignment reconstruction client


# **calc_max_sagitta_diff**
> float calc_max_sagitta_diff(iov1, iov2)

Computes the max sagitta difference between two IOVs

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    iov1 = 1 # int | 
    iov2 = 1 # int | 
    select_bml = True # bool |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Computes the max sagitta difference between two IOVs
        api_response = api_instance.calc_max_sagitta_diff(iov1, iov2)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->calc_max_sagitta_diff: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Computes the max sagitta difference between two IOVs
        api_response = api_instance.calc_max_sagitta_diff(iov1, iov2, select_bml=select_bml)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->calc_max_sagitta_diff: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iov1** | **int**|  |
 **iov2** | **int**|  |
 **select_bml** | **bool**|  | [optional]

### Return type

**float**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **finish_reco_client**
> AlignRecoClientDto finish_reco_client(client_id)

Declares a reconstruction client as FINISHED

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.align_reco_client_dto import AlignRecoClientDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    client_id = 1 # int | the client ID
    to_state = "STOPPING" # str | if set, sets the client state to supplied value (optional)

    # example passing only required values which don't have defaults set
    try:
        # Declares a reconstruction client as FINISHED
        api_response = api_instance.finish_reco_client(client_id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->finish_reco_client: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Declares a reconstruction client as FINISHED
        api_response = api_instance.finish_reco_client(client_id, to_state=to_state)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->finish_reco_client: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**| the client ID |
 **to_state** | **str**| if set, sets the client state to supplied value | [optional]

### Return type

[**AlignRecoClientDto**](AlignRecoClientDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reco_client**
> AlignRecoClientDto get_reco_client(client_id)

Retrieves the metadata about an alignment reconstruction client

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.align_reco_client_dto import AlignRecoClientDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    client_id = 1 # int | the client ID

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the metadata about an alignment reconstruction client
        api_response = api_instance.get_reco_client(client_id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->get_reco_client: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **int**| the client ID |

### Return type

[**AlignRecoClientDto**](AlignRecoClientDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reco_clients**
> AlignRecoClientSetDto get_reco_clients()

Retrieves the list of reconstruction clients

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.align_reco_client_set_dto import AlignRecoClientSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Retrieves the list of reconstruction clients
        api_response = api_instance.get_reco_clients()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->get_reco_clients: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**AlignRecoClientSetDto**](AlignRecoClientSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **make_empty_iovs**
> AligniovSetDto make_empty_iovs(tag)

Execute interval maker on tag and return the empty IOVs

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.aligniov_set_dto import AligniovSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    tag = "tag_example" # str | the tag name
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to present - look-back interval if not provided) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to present if not provided) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Execute interval maker on tag and return the empty IOVs
        api_response = api_instance.make_empty_iovs(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->make_empty_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Execute interval maker on tag and return the empty IOVs
        api_response = api_instance.make_empty_iovs(tag, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->make_empty_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |
 **since** | **datetime**| the since time of the interval to query (defaults to present - look-back interval if not provided) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to present if not provided) | [optional]

### Return type

[**AligniovSetDto**](AligniovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **make_stable_iovs**
> int make_stable_iovs(tag)

Loops on the latest reconstructed IOVs of tag and computes the reco status flag STABLE or RELEASED_FOR_CONDDB

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    tag = "tag_example" # str | the tag name

    # example passing only required values which don't have defaults set
    try:
        # Loops on the latest reconstructed IOVs of tag and computes the reco status flag STABLE or RELEASED_FOR_CONDDB
        api_response = api_instance.make_stable_iovs(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->make_stable_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |

### Return type

**int**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_reco_client**
> AlignRecoClientDto register_reco_client(align_reco_client_payload_dto)

Register an alignment reconstruction client

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import asap_api
from openapi_client.model.align_reco_client_dto import AlignRecoClientDto
from openapi_client.model.align_reco_client_payload_dto import AlignRecoClientPayloadDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = asap_api.AsapApi(api_client)
    align_reco_client_payload_dto = AlignRecoClientPayloadDto(
        host_name="host_name_example",
        user_name="user_name_example",
        client_name="client_name_example",
        branch="branch_example",
        revision="revision_example",
    ) # AlignRecoClientPayloadDto | 

    # example passing only required values which don't have defaults set
    try:
        # Register an alignment reconstruction client
        api_response = api_instance.register_reco_client(align_reco_client_payload_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AsapApi->register_reco_client: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **align_reco_client_payload_dto** | [**AlignRecoClientPayloadDto**](AlignRecoClientPayloadDto.md)|  |

### Return type

[**AlignRecoClientDto**](AlignRecoClientDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

