# AligncorrectionsDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**typ** | **str** |  | 
**jff** | **int** |  | 
**jzz** | **int** |  | 
**job** | **int** |  | 
**svalue** | **float** |  | 
**zvalue** | **float** |  | 
**tvalue** | **float** |  | 
**tsv** | **float** |  | 
**tzv** | **float** |  | 
**ttv** | **float** |  | 
**corr_id** | **int** |  | [optional] 
**corr_description** | **str** |  | [optional] 
**bz** | **float** |  | [optional] 
**bp** | **float** |  | [optional] 
**bn** | **float** |  | [optional] 
**sp** | **float** |  | [optional] 
**sn** | **float** |  | [optional] 
**tw** | **float** |  | [optional] 
**pg** | **float** |  | [optional] 
**tr** | **float** |  | [optional] 
**eg** | **float** |  | [optional] 
**ep** | **float** |  | [optional] 
**en** | **float** |  | [optional] 
**xatlas** | **float** |  | [optional] 
**yatlas** | **float** |  | [optional] 
**hw_element** | **str** |  | [optional] 
**hw_type** | **str** |  | [optional] 
**aligniov** | [**AligniovBaseDto**](AligniovBaseDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


