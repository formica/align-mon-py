# openapi_client.OpticallinesApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**convert_comment_to_incident**](OpticallinesApi.md#convert_comment_to_incident) | **PUT** /opticallines/tests/convert_comment_to_incident | 
[**create_opticallines**](OpticallinesApi.md#create_opticallines) | **POST** /opticallines | Create a new opticaline.
[**find**](OpticallinesApi.md#find) | **GET** /opticallines/find | Finds an OpticallinesSetDto by optical line name
[**find_chambers**](OpticallinesApi.md#find_chambers) | **GET** /opticallines/config/chambers | Lists the configured asap chambers
[**find_ol_incidents**](OpticallinesApi.md#find_ol_incidents) | **GET** /opticallines/incidents | Retrieves a list of OL incidents, parametrized by query
[**find_opticallines**](OpticallinesApi.md#find_opticallines) | **GET** /opticallines | Retrieves a list of optical lines with their last OL tests
[**find_tests**](OpticallinesApi.md#find_tests) | **GET** /opticallines/tests | Finds an OltestlinesSetDto by optical line name and time range
[**find_tests_without_image**](OpticallinesApi.md#find_tests_without_image) | **GET** /opticallines/tests/withoutimage | Finds the OL tests for which an image should be uploaded
[**get_chamber**](OpticallinesApi.md#get_chamber) | **GET** /opticallines/config/chambers/{chamberId} | Retrieves a configured asap chamber
[**get_ol_incident**](OpticallinesApi.md#get_ol_incident) | **GET** /opticallines/incidents/{id} | Retrieves a single OL incident, by ID
[**get_ol_incident_comments**](OpticallinesApi.md#get_ol_incident_comments) | **GET** /opticallines/incidents/{id}/comments | Retrieves the list of comments linked to this OL incident
[**get_rasdim_file**](OpticallinesApi.md#get_rasdim_file) | **GET** /opticallines/tests/rasdimfile | 
[**get_status_summary**](OpticallinesApi.md#get_status_summary) | **GET** /opticallines/statussummary | Computes a status summary for the optical lines
[**get_test**](OpticallinesApi.md#get_test) | **GET** /opticallines/tests/{testid} | 
[**get_test_comment**](OpticallinesApi.md#get_test_comment) | **GET** /opticallines/tests/{testid}/comment | 
[**get_test_image**](OpticallinesApi.md#get_test_image) | **GET** /opticallines/tests/{testid}/image | 
[**post_ol_incident**](OpticallinesApi.md#post_ol_incident) | **POST** /opticallines/incidents | post a new OL incident
[**post_rasdim_file**](OpticallinesApi.md#post_rasdim_file) | **POST** /opticallines/tests/rasdimfile | 
[**post_test_image**](OpticallinesApi.md#post_test_image) | **POST** /opticallines/tests/{testid}/image | 
[**put_ol_incident**](OpticallinesApi.md#put_ol_incident) | **PUT** /opticallines/incidents/{id} | update comment and/or status of OL incident
[**set_test_comment**](OpticallinesApi.md#set_test_comment) | **PUT** /opticallines/tests/{testid}/comment | 
[**sync_files_batch**](OpticallinesApi.md#sync_files_batch) | **PUT** /opticallines/tests/sync_batch_files_to_db | 
[**sync_test_files_to_db**](OpticallinesApi.md#sync_test_files_to_db) | **PUT** /opticallines/tests/{testid}/sync_files_to_db | 
[**update_chamber**](OpticallinesApi.md#update_chamber) | **PUT** /opticallines/config/chambers/{chamberId} | Updates a configured asap chamber


# **convert_comment_to_incident**
> convert_comment_to_incident(ol_name)



Looks for the icon and image files of an OL test, and upload them to the ALIGN_FILE tables, modifying the references of this test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    ol_name = "olName_example" # str | 

    # example passing only required values which don't have defaults set
    try:
        api_instance.convert_comment_to_incident(ol_name)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->convert_comment_to_incident: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ol_name** | **str**|  |

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_opticallines**
> OpticallinesDto create_opticallines()

Create a new opticaline.

This method allows to insert Opticallines.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.opticallines_dto import OpticallinesDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    opticallines_dto = OpticallinesDto(None) # OpticallinesDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new opticaline.
        api_response = api_instance.create_opticallines(opticallines_dto=opticallines_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->create_opticallines: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **opticallines_dto** | [**OpticallinesDto**](OpticallinesDto.md)|  | [optional]

### Return type

[**OpticallinesDto**](OpticallinesDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find**
> OpticallinesSetDto find(name)

Finds an OpticallinesSetDto by optical line name

Finds an OpticallinesSetDto by  by optical line name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.opticallines_set_dto import OpticallinesSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    name = "name_example" # str | the optical line name
    status = "ACTIVE" # str | the optical line status (optional) if omitted the server will use the default value of "ACTIVE"

    # example passing only required values which don't have defaults set
    try:
        # Finds an OpticallinesSetDto by optical line name
        api_response = api_instance.find(name)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an OpticallinesSetDto by optical line name
        api_response = api_instance.find(name, status=status)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| the optical line name |
 **status** | **str**| the optical line status | [optional] if omitted the server will use the default value of "ACTIVE"

### Return type

[**OpticallinesSetDto**](OpticallinesSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_chambers**
> AsapChamberSetDto find_chambers()

Lists the configured asap chambers

Queries asap chambers, selecting active and/or in position chambers

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.asap_chamber_set_dto import AsapChamberSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    activate = 1 # int | the activate bit of the chamber (optional)
    inposition = 1 # int | the inposition bit of the chamber (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Lists the configured asap chambers
        api_response = api_instance.find_chambers(activate=activate, inposition=inposition)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_chambers: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activate** | **int**| the activate bit of the chamber | [optional]
 **inposition** | **int**| the inposition bit of the chamber | [optional]

### Return type

[**AsapChamberSetDto**](AsapChamberSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_ol_incidents**
> OlIncidentSetDto find_ol_incidents()

Retrieves a list of OL incidents, parametrized by query

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.ol_incident_set_dto import OlIncidentSetDto
from openapi_client.model.ol_incident_category import OlIncidentCategory
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    category = [
        OlIncidentCategory("PIT_TODO"),
    ] # [OlIncidentCategory] | the (optional) category to query (optional)
    olname = "olname_example" # str | the (optional) optical line name (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a list of OL incidents, parametrized by query
        api_response = api_instance.find_ol_incidents(category=category, olname=olname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_ol_incidents: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | [**[OlIncidentCategory]**](OlIncidentCategory.md)| the (optional) category to query | [optional]
 **olname** | **str**| the (optional) optical line name | [optional]

### Return type

[**OlIncidentSetDto**](OlIncidentSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_opticallines**
> OpticallinesSetDto find_opticallines()

Retrieves a list of optical lines with their last OL tests

The opticallines are optionally selected according to their pit hardware status, as parameterized by the `query` enum 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.opticallines_set_dto import OpticallinesSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    query = "all" # str | The set of optical lines to query  * `all` - all optical lines  * `problem` - all optical lines with failing glob_err flag  * `todo` - TODO list items, failing optical lines on positioned chambers  (optional) if omitted the server will use the default value of "all"

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a list of optical lines with their last OL tests
        api_response = api_instance.find_opticallines(query=query)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_opticallines: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **str**| The set of optical lines to query  * &#x60;all&#x60; - all optical lines  * &#x60;problem&#x60; - all optical lines with failing glob_err flag  * &#x60;todo&#x60; - TODO list items, failing optical lines on positioned chambers  | [optional] if omitted the server will use the default value of "all"

### Return type

[**OpticallinesSetDto**](OpticallinesSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_tests**
> OltestlinesSetDto find_tests(name)

Finds an OltestlinesSetDto by optical line name and time range

Finds an OltestlinesSetDto by optical line name and time range

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_set_dto import OltestlinesSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    name = "name_example" # str | the optical line name
    page = 0 # int | the page number (optional) if omitted the server will use the default value of 0
    size = 200 # int | the page size (optional) if omitted the server will use the default value of 200
    sort = "testdate:DESC" # str | the sort field (optional) if omitted the server will use the default value of "testdate:DESC"

    # example passing only required values which don't have defaults set
    try:
        # Finds an OltestlinesSetDto by optical line name and time range
        api_response = api_instance.find_tests(name)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_tests: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an OltestlinesSetDto by optical line name and time range
        api_response = api_instance.find_tests(name, page=page, size=size, sort=sort)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_tests: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**| the optical line name |
 **page** | **int**| the page number | [optional] if omitted the server will use the default value of 0
 **size** | **int**| the page size | [optional] if omitted the server will use the default value of 200
 **sort** | **str**| the sort field | [optional] if omitted the server will use the default value of "testdate:DESC"

### Return type

[**OltestlinesSetDto**](OltestlinesSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_tests_without_image**
> OltestlinesSetDto find_tests_without_image(pcccd)

Finds the OL tests for which an image should be uploaded

Finds the OL tests for which an image should be uploaded

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_set_dto import OltestlinesSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    pcccd = 1 # int | the acquisition PC for the image
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the optical line name (optional)

    # example passing only required values which don't have defaults set
    try:
        # Finds the OL tests for which an image should be uploaded
        api_response = api_instance.find_tests_without_image(pcccd)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_tests_without_image: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds the OL tests for which an image should be uploaded
        api_response = api_instance.find_tests_without_image(pcccd, since=since)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->find_tests_without_image: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pcccd** | **int**| the acquisition PC for the image |
 **since** | **datetime**| the optical line name | [optional]

### Return type

[**OltestlinesSetDto**](OltestlinesSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_chamber**
> AsapChamberDto get_chamber(chamber_id)

Retrieves a configured asap chamber

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.asap_chamber_dto import AsapChamberDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    chamber_id = "chamberId_example" # str | the ID of the chamber

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a configured asap chamber
        api_response = api_instance.get_chamber(chamber_id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_chamber: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chamber_id** | **str**| the ID of the chamber |

### Return type

[**AsapChamberDto**](AsapChamberDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ol_incident**
> OlIncidentDto get_ol_incident(id)

Retrieves a single OL incident, by ID

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.ol_incident_dto import OlIncidentDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    id = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a single OL incident, by ID
        api_response = api_instance.get_ol_incident(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_ol_incident: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**OlIncidentDto**](OlIncidentDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ol_incident_comments**
> OlIncidentCommentSetDto get_ol_incident_comments(id)

Retrieves the list of comments linked to this OL incident

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.ol_incident_comment_set_dto import OlIncidentCommentSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    id = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the list of comments linked to this OL incident
        api_response = api_instance.get_ol_incident_comments(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_ol_incident_comments: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |

### Return type

[**OlIncidentCommentSetDto**](OlIncidentCommentSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_rasdim_file**
> file_type get_rasdim_file(filename)



retrieves an original rasdim file at the origin of the oltestlines

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    filename = "filename_example" # str | the name of the original rasdim file

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_rasdim_file(filename)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_rasdim_file: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | **str**| the name of the original rasdim file |

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_status_summary**
> OptlineStatusSummaryDto get_status_summary()

Computes a status summary for the optical lines

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.optline_status_summary_dto import OptlineStatusSummaryDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Computes a status summary for the optical lines
        api_response = api_instance.get_status_summary()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_status_summary: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**OptlineStatusSummaryDto**](OptlineStatusSummaryDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_test**
> OltestlinesDto get_test(testid)



retrieves an OltestlinesDto by its ID

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_dto import OltestlinesDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_test(testid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_test: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |

### Return type

[**OltestlinesDto**](OltestlinesDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_test_comment**
> str get_test_comment(testid)



Retrieves the comment of a particular OL test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_test_comment(testid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_test_comment: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |

### Return type

**str**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Bad request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_test_image**
> file_type get_test_image(testid)



retrieves the test image for a particular OL test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_test_image(testid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->get_test_image: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_ol_incident**
> OlIncidentDto post_ol_incident()

post a new OL incident

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.ol_incident_post_dto import OlIncidentPostDto
from openapi_client.model.ol_incident_dto import OlIncidentDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    ol_incident_post_dto = OlIncidentPostDto(
        ol_name="ol_name_example",
        incident_date=dateutil_parser('1970-01-01T00:00:00.00Z'),
        incident_category=OlIncidentCategory("PIT_TODO"),
        comment_text="comment_text_example",
        test_code=1,
    ) # OlIncidentPostDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # post a new OL incident
        api_response = api_instance.post_ol_incident(ol_incident_post_dto=ol_incident_post_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->post_ol_incident: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ol_incident_post_dto** | [**OlIncidentPostDto**](OlIncidentPostDto.md)|  | [optional]

### Return type

[**OlIncidentDto**](OlIncidentDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | New OL incident is created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_rasdim_file**
> RasdimFilePayloadDto post_rasdim_file(file)



posts a rasdim files and store in DB the decoded OLtestlines set

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.rasdim_file_payload_dto import RasdimFilePayloadDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    file = open('/path/to/file', 'rb') # file_type | The file content to upload

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.post_rasdim_file(file)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->post_rasdim_file: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **file_type**| The file content to upload |

### Return type

[**RasdimFilePayloadDto**](RasdimFilePayloadDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**204** | this rasdim file has already been uploaded, nothing was done |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_test_image**
> OltestlinesDto post_test_image(testid, file)



sets the image file for a particular OL test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_dto import OltestlinesDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test
    file = open('/path/to/file', 'rb') # file_type | The file content to upload

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.post_test_image(testid, file)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->post_test_image: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |
 **file** | **file_type**| The file content to upload |

### Return type

[**OltestlinesDto**](OltestlinesDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | test image file is accepted |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_ol_incident**
> OlIncidentDto put_ol_incident(id)

update comment and/or status of OL incident

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.ol_incident_put_dto import OlIncidentPutDto
from openapi_client.model.ol_incident_dto import OlIncidentDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    id = 1 # int | 
    ol_incident_put_dto = OlIncidentPutDto(
        incident_category=OlIncidentCategory("PIT_TODO"),
        comment_date=dateutil_parser('1970-01-01T00:00:00.00Z'),
        comment_text="comment_text_example",
        test_code=1,
    ) # OlIncidentPutDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # update comment and/or status of OL incident
        api_response = api_instance.put_ol_incident(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->put_ol_incident: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # update comment and/or status of OL incident
        api_response = api_instance.put_ol_incident(id, ol_incident_put_dto=ol_incident_put_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->put_ol_incident: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  |
 **ol_incident_put_dto** | [**OlIncidentPutDto**](OlIncidentPutDto.md)|  | [optional]

### Return type

[**OlIncidentDto**](OlIncidentDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OL incident is updated |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **set_test_comment**
> OltestlinesDto set_test_comment(testid)



Sets the comment of a particular OL test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_dto import OltestlinesDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test
    body = "body_example" # str |  (optional)

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.set_test_comment(testid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->set_test_comment: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        api_response = api_instance.set_test_comment(testid, body=body)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->set_test_comment: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |
 **body** | **str**|  | [optional]

### Return type

[**OltestlinesDto**](OltestlinesDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**400** | Bad request |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sync_files_batch**
> OltestlineSyncSummaryDto sync_files_batch()



Looks for the icon and image files of an OL test, and upload them to the ALIGN_FILE tables, modifying the references of this test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.oltestline_sync_summary_dto import OltestlineSyncSummaryDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        api_response = api_instance.sync_files_batch()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->sync_files_batch: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**OltestlineSyncSummaryDto**](OltestlineSyncSummaryDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sync_test_files_to_db**
> OltestlinesDto sync_test_files_to_db(testid)



Looks for the icon and image files of an OL test, and upload them to the ALIGN_FILE tables, modifying the references of this test

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.oltestlines_dto import OltestlinesDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    testid = 1 # int | the ID of the OL test

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.sync_test_files_to_db(testid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->sync_test_files_to_db: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testid** | **int**| the ID of the OL test |

### Return type

[**OltestlinesDto**](OltestlinesDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**304** | OL test not modified: files already uploaded to DB |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_chamber**
> AsapChamberDto update_chamber(chamber_id, generic_map)

Updates a configured asap chamber

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import opticallines_api
from openapi_client.model.asap_chamber_dto import AsapChamberDto
from openapi_client.model.generic_map import GenericMap
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = opticallines_api.OpticallinesApi(api_client)
    chamber_id = "chamberId_example" # str | the ID of the chamber
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap | A JSON object containing generic map used for updates

    # example passing only required values which don't have defaults set
    try:
        # Updates a configured asap chamber
        api_response = api_instance.update_chamber(chamber_id, generic_map)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OpticallinesApi->update_chamber: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chamber_id** | **str**| the ID of the chamber |
 **generic_map** | [**GenericMap**](GenericMap.md)| A JSON object containing generic map used for updates |

### Return type

[**AsapChamberDto**](AsapChamberDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

