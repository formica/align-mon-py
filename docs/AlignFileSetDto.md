# AlignFileSetDto

A set of align files.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setformat** | **str** |  | defaults to "AlignFileSetDto"
**setformat** | **str** |  | [optional]  if omitted the server will use the default value of "AlignFileSetDto"
**resources** | [**[AlignFileOutputDto]**](AlignFileOutputDto.md) |  | [optional] 
**size** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


