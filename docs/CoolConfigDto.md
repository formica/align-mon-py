# CoolConfigDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**system_type** | **str** |  | 
**db_name** | **str** |  | 
**schema_name** | **str** |  | 
**folder_name** | **str** |  | 
**db_cluster** | **str** |  | [optional] 
**server_url_get** | **str** |  | [optional] 
**server_url_put** | **str** |  | [optional] 
**channel** | **str** |  | [optional] 
**active** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


