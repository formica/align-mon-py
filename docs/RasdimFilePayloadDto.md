# RasdimFilePayloadDto

The result of the postRasdimFile: details of the rasdim files and the list of OL tests

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resources** | [**[OltestlinesDto]**](OltestlinesDto.md) |  | 
**setformat** | **str** |  | 
**file_name** | **str** |  | [optional] 
**test_date** | **datetime** |  | [optional] 
**test_date_tag** | **str** |  | [optional] 
**test_sequence** | **int** |  | [optional] 
**insertion_time** | **datetime** |  | [optional] 
**size** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


