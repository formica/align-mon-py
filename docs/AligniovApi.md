# openapi_client.AligniovApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_aligniov**](AligniovApi.md#create_aligniov) | **POST** /aligniov | Create a new aligniov in the database.
[**create_iov_aligncorrections**](AligniovApi.md#create_iov_aligncorrections) | **POST** /aligniov/{iovid}/corrections | Uploads alignment corrections for an IOV
[**create_iov_alignfitpulls**](AligniovApi.md#create_iov_alignfitpulls) | **POST** /aligniov/{iovid}/pulls | Upload the fit pulls for and IOV
[**create_iov_alignfitsteps**](AligniovApi.md#create_iov_alignfitsteps) | **POST** /aligniov/{iovid}/fitsteps | Uploads the fit steps for an IOV
[**create_iov_alignsagittas**](AligniovApi.md#create_iov_alignsagittas) | **POST** /aligniov/{iovid}/sagittas | Uploads the sagitta data for an IOV
[**create_iov_alignsensors_residuals**](AligniovApi.md#create_iov_alignsensors_residuals) | **POST** /aligniov/{iovid}/residuals | Uploads sensor residuals for an IOV
[**create_iov_bad_sensors**](AligniovApi.md#create_iov_bad_sensors) | **POST** /aligniov/{iovid}/fitbadsensors | Uploads the bad sensor summary for an IOV
[**delete_align_iov**](AligniovApi.md#delete_align_iov) | **DELETE** /aligniov/{iovid} | Deletes a single IOV by ID, by default performing some checks that it is safe to delete.
[**delete_align_iovs**](AligniovApi.md#delete_align_iovs) | **DELETE** /aligniov | Deletes a list of IOVs in a given time range, performing some checks for safety
[**fetch_align_iov**](AligniovApi.md#fetch_align_iov) | **GET** /aligniov/{iovid} | Retrieves a single IOV, by ID
[**fetch_aligniov_file**](AligniovApi.md#fetch_aligniov_file) | **GET** /aligniov/{iovid}/file | Finds an Aligniov by iovid and dump the ASCII output (COOL format)
[**fetch_bad_sensors**](AligniovApi.md#fetch_bad_sensors) | **GET** /aligniov/{iovid}/fitbadsensors | Retrieves the bad sensor summary associated to an IOV
[**fetch_corrections**](AligniovApi.md#fetch_corrections) | **GET** /aligniov/{iovid}/corrections | Retrieves the corrections of an IOV
[**fetch_fit_steps**](AligniovApi.md#fetch_fit_steps) | **GET** /aligniov/{iovid}/fitsteps | Retrieves the fit steps associated to an IOV
[**fetch_sagittas**](AligniovApi.md#fetch_sagittas) | **GET** /aligniov/{iovid}/sagittas | Retrieves the sagittas associated to an IOV
[**fetch_sensor_pulls**](AligniovApi.md#fetch_sensor_pulls) | **GET** /aligniov/{iovid}/pulls | Retrieves the sensor pulls associated to an IOV
[**fetch_sensor_residuals**](AligniovApi.md#fetch_sensor_residuals) | **GET** /aligniov/{iovid}/residuals | Retrieves the sensor residuals associated to an IOV
[**find_align_iovs**](AligniovApi.md#find_align_iovs) | **GET** /aligniov | Retrieves a list of IOVs dynamically, depending on arguments
[**get_centered_iov_range**](AligniovApi.md#get_centered_iov_range) | **GET** /aligniov/{iovid}/centeredRange | Retrieves a range of IOVs, sorted by time, centered around the given iovid
[**get_reco_summary**](AligniovApi.md#get_reco_summary) | **GET** /aligniov/recosummary | Retrieves the list of align IOVs processed in the last 24h, for the active tags, and a COOL upload count
[**list_iov_files**](AligniovApi.md#list_iov_files) | **GET** /aligniov/{iovid}/iovfiles | Lists the uploaded files associated to this IOV
[**update_align_iov**](AligniovApi.md#update_align_iov) | **POST** /aligniov/{iovid} | Updates an alignIOV, optionally locking it to a particular client ID
[**upload_iov_file**](AligniovApi.md#upload_iov_file) | **POST** /aligniov/{iovid}/iovfiles | 


# **create_aligniov**
> AligniovDto create_aligniov(tagname)

Create a new aligniov in the database.

This method allows to insert an Aligniov.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_dto import AligniovDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    tagname = "tagname_example" # str | 
    aligniov_dto = AligniovDto(None) # AligniovDto |  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Create a new aligniov in the database.
        api_response = api_instance.create_aligniov(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_aligniov: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new aligniov in the database.
        api_response = api_instance.create_aligniov(tagname, aligniov_dto=aligniov_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_aligniov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**|  |
 **aligniov_dto** | [**AligniovDto**](AligniovDto.md)|  | [optional]

### Return type

[**AligniovDto**](AligniovDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_aligncorrections**
> create_iov_aligncorrections(iovid, aligncorrections_dto)

Uploads alignment corrections for an IOV

This method allows to insert multiple Aligncorrections.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligncorrections_dto import AligncorrectionsDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    aligncorrections_dto = [
        AligncorrectionsDto(
            corr_id=1,
            corr_description="corr_description_example",
            typ="typ_example",
            jff=1,
            jzz=1,
            job=1,
            svalue=3.14,
            zvalue=3.14,
            tvalue=3.14,
            tsv=3.14,
            tzv=3.14,
            ttv=3.14,
            bz=3.14,
            bp=3.14,
            bn=3.14,
            sp=3.14,
            sn=3.14,
            tw=3.14,
            pg=3.14,
            tr=3.14,
            eg=3.14,
            ep=3.14,
            en=3.14,
            xatlas=3.14,
            yatlas=3.14,
            hw_element="hw_element_example",
            hw_type="hw_type_example",
            aligniov=AligniovBaseDto(
                iov_id=1,
                since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                reco_status=AligniovRecoStatusEnum("EMPTY"),
                fit_status=1,
            ),
        ),
    ] # [AligncorrectionsDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Uploads alignment corrections for an IOV
        api_instance.create_iov_aligncorrections(iovid, aligncorrections_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_aligncorrections: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Uploads alignment corrections for an IOV
        api_instance.create_iov_aligncorrections(iovid, aligncorrections_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_aligncorrections: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **aligncorrections_dto** | [**[AligncorrectionsDto]**](AligncorrectionsDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_alignfitpulls**
> create_iov_alignfitpulls(iovid, align_fit_pulls_dto)

Upload the fit pulls for and IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_fit_pulls_dto import AlignFitPullsDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    align_fit_pulls_dto = [
        AlignFitPullsDto(
            iovid=1,
            olname="olname_example",
            x_pull=3.14,
            y_pull=3.14,
            z_pull=3.14,
            aligniov=AligniovBaseDto(
                iov_id=1,
                since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                reco_status=AligniovRecoStatusEnum("EMPTY"),
                fit_status=1,
            ),
        ),
    ] # [AlignFitPullsDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Upload the fit pulls for and IOV
        api_instance.create_iov_alignfitpulls(iovid, align_fit_pulls_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignfitpulls: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Upload the fit pulls for and IOV
        api_instance.create_iov_alignfitpulls(iovid, align_fit_pulls_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignfitpulls: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **align_fit_pulls_dto** | [**[AlignFitPullsDto]**](AlignFitPullsDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_alignfitsteps**
> create_iov_alignfitsteps(iovid, align_fit_steps_dto)

Uploads the fit steps for an IOV

This method allows to insert multiple AlignFitSteps.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_fit_steps_dto import AlignFitStepsDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    align_fit_steps_dto = [
        AlignFitStepsDto(
            fit_id=1,
            chi_square_ov_dof=3.14,
            chi_square=3.14,
            dof=1,
            prob=3.14,
            sequence_number=1,
            fit_description="fit_description_example",
            n_iterations=1,
            n_max_iterations=1,
            exit_code=1,
            branch="branch_example",
            revision="revision_example",
            reco_client_id=1,
            aligniov=AligniovBaseDto(
                iov_id=1,
                since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                reco_status=AligniovRecoStatusEnum("EMPTY"),
                fit_status=1,
            ),
        ),
    ] # [AlignFitStepsDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Uploads the fit steps for an IOV
        api_instance.create_iov_alignfitsteps(iovid, align_fit_steps_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignfitsteps: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Uploads the fit steps for an IOV
        api_instance.create_iov_alignfitsteps(iovid, align_fit_steps_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignfitsteps: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **align_fit_steps_dto** | [**[AlignFitStepsDto]**](AlignFitStepsDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_alignsagittas**
> create_iov_alignsagittas(iovid, align_sagittas_dto)

Uploads the sagitta data for an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_sagittas_dto import AlignSagittasDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    align_sagittas_dto = [
        AlignSagittasDto(
            iovid=1,
            sag_id=1,
            xtrack=3.14,
            ytrack=3.14,
            ztrack=3.14,
            uxtrack=3.14,
            uytrack=3.14,
            uztrack=3.14,
            bi="bi_example",
            bm="bm_example",
            bo="bo_example",
            sag_correction=3.14,
            sag_error=3.14,
            aligniov=AligniovBaseDto(
                iov_id=1,
                since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                reco_status=AligniovRecoStatusEnum("EMPTY"),
                fit_status=1,
            ),
        ),
    ] # [AlignSagittasDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Uploads the sagitta data for an IOV
        api_instance.create_iov_alignsagittas(iovid, align_sagittas_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignsagittas: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Uploads the sagitta data for an IOV
        api_instance.create_iov_alignsagittas(iovid, align_sagittas_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignsagittas: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **align_sagittas_dto** | [**[AlignSagittasDto]**](AlignSagittasDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_alignsensors_residuals**
> create_iov_alignsensors_residuals(iovid, align_sensor_residuals_dto)

Uploads sensor residuals for an IOV

This method allows to insert multiple Aligncorrections.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_sensor_residuals_dto import AlignSensorResidualsDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    align_sensor_residuals_dto = [
        AlignSensorResidualsDto(
            res_id=1,
            channel_name="channel_name_example",
            type="type_example",
            dist_cl=3.14,
            dist_lm=3.14,
            aligniov=AligniovBaseDto(
                iov_id=1,
                since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
                reco_status=AligniovRecoStatusEnum("EMPTY"),
                fit_status=1,
            ),
            sensor_measurements=[
                AlignSensorMeasDto(
                    meas_name="meas_name_example",
                    meas=3.14,
                    model=3.14,
                    error=3.14,
                    measraw=3.14,
                    measrawerror=3.14,
                    measrawnpoints=1,
                ),
            ],
        ),
    ] # [AlignSensorResidualsDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Uploads sensor residuals for an IOV
        api_instance.create_iov_alignsensors_residuals(iovid, align_sensor_residuals_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignsensors_residuals: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Uploads sensor residuals for an IOV
        api_instance.create_iov_alignsensors_residuals(iovid, align_sensor_residuals_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_alignsensors_residuals: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **align_sensor_residuals_dto** | [**[AlignSensorResidualsDto]**](AlignSensorResidualsDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_iov_bad_sensors**
> create_iov_bad_sensors(iovid, align_fit_bad_sensors_dto)

Uploads the bad sensor summary for an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_fit_bad_sensors_dto import AlignFitBadSensorsDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IovId to which associate the corrections
    align_fit_bad_sensors_dto = [
        AlignFitBadSensorsDto(
            fit_id=1,
            sensor_type="sensor_type_example",
            rate_good=3.14,
            sensor_total=1,
            sensor_failure=1,
        ),
    ] # [AlignFitBadSensorsDto] | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Uploads the bad sensor summary for an IOV
        api_instance.create_iov_bad_sensors(iovid, align_fit_bad_sensors_dto)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_bad_sensors: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Uploads the bad sensor summary for an IOV
        api_instance.create_iov_bad_sensors(iovid, align_fit_bad_sensors_dto, reco_client_id=reco_client_id)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->create_iov_bad_sensors: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IovId to which associate the corrections |
 **align_fit_bad_sensors_dto** | [**[AlignFitBadSensorsDto]**](AlignFitBadSensorsDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_align_iov**
> delete_align_iov(iovid)

Deletes a single IOV by ID, by default performing some checks that it is safe to delete.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 
    force = False # bool | if true then the safety checks are disabled (optional) if omitted the server will use the default value of False
    replace_with_empty = False # bool | if true then the removed IOV is replaced with a new AlignIov with state EMPTY (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Deletes a single IOV by ID, by default performing some checks that it is safe to delete.
        api_instance.delete_align_iov(iovid)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->delete_align_iov: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Deletes a single IOV by ID, by default performing some checks that it is safe to delete.
        api_instance.delete_align_iov(iovid, force=force, replace_with_empty=replace_with_empty)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->delete_align_iov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |
 **force** | **bool**| if true then the safety checks are disabled | [optional] if omitted the server will use the default value of False
 **replace_with_empty** | **bool**| if true then the removed IOV is replaced with a new AlignIov with state EMPTY | [optional] if omitted the server will use the default value of False

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_align_iovs**
> delete_align_iovs(tagname)

Deletes a list of IOVs in a given time range, performing some checks for safety

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to infinite past if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to inifinite future if not present) (optional)
    force = False # bool | if true then the safety checks are disabled (optional) if omitted the server will use the default value of False
    replace_with_empty = False # bool | if true then the removed IOVs are replaced with new AlignIovs with state EMPTY (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Deletes a list of IOVs in a given time range, performing some checks for safety
        api_instance.delete_align_iovs(tagname)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->delete_align_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Deletes a list of IOVs in a given time range, performing some checks for safety
        api_instance.delete_align_iovs(tagname, since=since, till=till, force=force, replace_with_empty=replace_with_empty)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->delete_align_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **since** | **datetime**| the since time of the interval to query (defaults to infinite past if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to inifinite future if not present) | [optional]
 **force** | **bool**| if true then the safety checks are disabled | [optional] if omitted the server will use the default value of False
 **replace_with_empty** | **bool**| if true then the removed IOVs are replaced with new AlignIovs with state EMPTY | [optional] if omitted the server will use the default value of False

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | successful operation |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_align_iov**
> AligniovDto fetch_align_iov(iovid)

Retrieves a single IOV, by ID

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_dto import AligniovDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 
    fetch_fitsteps = False # bool | if true then the AlignFitSteps will be fetched (optional) if omitted the server will use the default value of False
    fetch_badsensors = False # bool | if true then the AlignFitBadSensors will be fetched (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a single IOV, by ID
        api_response = api_instance.fetch_align_iov(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_align_iov: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a single IOV, by ID
        api_response = api_instance.fetch_align_iov(iovid, fetch_fitsteps=fetch_fitsteps, fetch_badsensors=fetch_badsensors)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_align_iov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |
 **fetch_fitsteps** | **bool**| if true then the AlignFitSteps will be fetched | [optional] if omitted the server will use the default value of False
 **fetch_badsensors** | **bool**| if true then the AlignFitBadSensors will be fetched | [optional] if omitted the server will use the default value of False

### Return type

[**AligniovDto**](AligniovDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_aligniov_file**
> file_type fetch_aligniov_file(iovid)

Finds an Aligniov by iovid and dump the ASCII output (COOL format)

Finds an Aligniov by iovid

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 
    iov_format = "COOL" # str | The ascii format [COOL, AMDB] (optional) if omitted the server will use the default value of "COOL"

    # example passing only required values which don't have defaults set
    try:
        # Finds an Aligniov by iovid and dump the ASCII output (COOL format)
        api_response = api_instance.fetch_aligniov_file(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_aligniov_file: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an Aligniov by iovid and dump the ASCII output (COOL format)
        api_response = api_instance.fetch_aligniov_file(iovid, iov_format=iov_format)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_aligniov_file: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |
 **iov_format** | **str**| The ascii format [COOL, AMDB] | [optional] if omitted the server will use the default value of "COOL"

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/octet-stream


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_bad_sensors**
> AlignFitBadSensorsSetDto fetch_bad_sensors(iovid)

Retrieves the bad sensor summary associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_fit_bad_sensors_set_dto import AlignFitBadSensorsSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the bad sensor summary associated to an IOV
        api_response = api_instance.fetch_bad_sensors(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_bad_sensors: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignFitBadSensorsSetDto**](AlignFitBadSensorsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_corrections**
> AligncorrectionsSetDto fetch_corrections(iovid)

Retrieves the corrections of an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligncorrections_set_dto import AligncorrectionsSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the corrections of an IOV
        api_response = api_instance.fetch_corrections(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_corrections: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AligncorrectionsSetDto**](AligncorrectionsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_fit_steps**
> AlignFitStepsSetDto fetch_fit_steps(iovid)

Retrieves the fit steps associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_fit_steps_set_dto import AlignFitStepsSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the fit steps associated to an IOV
        api_response = api_instance.fetch_fit_steps(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_fit_steps: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignFitStepsSetDto**](AlignFitStepsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_sagittas**
> AlignSagittasSetDto fetch_sagittas(iovid)

Retrieves the sagittas associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_sagittas_set_dto import AlignSagittasSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the sagittas associated to an IOV
        api_response = api_instance.fetch_sagittas(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_sagittas: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignSagittasSetDto**](AlignSagittasSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_sensor_pulls**
> AlignFitPullsSetDto fetch_sensor_pulls(iovid)

Retrieves the sensor pulls associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_fit_pulls_set_dto import AlignFitPullsSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the sensor pulls associated to an IOV
        api_response = api_instance.fetch_sensor_pulls(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_sensor_pulls: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignFitPullsSetDto**](AlignFitPullsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_sensor_residuals**
> AlignSensorResidualsSetDto fetch_sensor_residuals(iovid)

Retrieves the sensor residuals associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_sensor_residuals_set_dto import AlignSensorResidualsSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the sensor residuals associated to an IOV
        api_response = api_instance.fetch_sensor_residuals(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->fetch_sensor_residuals: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignSensorResidualsSetDto**](AlignSensorResidualsSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_align_iovs**
> AligniovSetDto find_align_iovs(tagname)

Retrieves a list of IOVs dynamically, depending on arguments

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_reco_status_enum import AligniovRecoStatusEnum
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.aligniov_set_dto import AligniovSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    tagname = "tagname_example" # str | the name of the tag (required)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to infinite past if not present) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to inifinite future if not present) (optional)
    reco_states = [
        AligniovRecoStatusEnum("EMPTY"),
    ] # [AligniovRecoStatusEnum] | the list of recoStates to query (defaults to all recoStates if not present) (optional)
    fetch_fitsteps = False # bool | if true then the AlignFitSteps will be fetched (optional) if omitted the server will use the default value of False
    fetch_badsensors = False # bool | if true then the AlignFitBadSensors will be fetched (optional) if omitted the server will use the default value of False
    page = 1 # int | the page number (optional)
    size = 1 # int | the page size (optional)
    sort = "sinceT:ASC" # str | the sort field (optional) if omitted the server will use the default value of "sinceT:ASC"

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a list of IOVs dynamically, depending on arguments
        api_response = api_instance.find_align_iovs(tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->find_align_iovs: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a list of IOVs dynamically, depending on arguments
        api_response = api_instance.find_align_iovs(tagname, since=since, till=till, reco_states=reco_states, fetch_fitsteps=fetch_fitsteps, fetch_badsensors=fetch_badsensors, page=page, size=size, sort=sort)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->find_align_iovs: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagname** | **str**| the name of the tag (required) |
 **since** | **datetime**| the since time of the interval to query (defaults to infinite past if not present) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to inifinite future if not present) | [optional]
 **reco_states** | [**[AligniovRecoStatusEnum]**](AligniovRecoStatusEnum.md)| the list of recoStates to query (defaults to all recoStates if not present) | [optional]
 **fetch_fitsteps** | **bool**| if true then the AlignFitSteps will be fetched | [optional] if omitted the server will use the default value of False
 **fetch_badsensors** | **bool**| if true then the AlignFitBadSensors will be fetched | [optional] if omitted the server will use the default value of False
 **page** | **int**| the page number | [optional]
 **size** | **int**| the page size | [optional]
 **sort** | **str**| the sort field | [optional] if omitted the server will use the default value of "sinceT:ASC"

### Return type

[**AligniovSetDto**](AligniovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_centered_iov_range**
> AligniovSetDto get_centered_iov_range(iovid, tagname)

Retrieves a range of IOVs, sorted by time, centered around the given iovid

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_reco_status_enum import AligniovRecoStatusEnum
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.aligniov_set_dto import AligniovSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 
    tagname = "tagname_example" # str | the name of the tag (required)
    reco_states = [
        AligniovRecoStatusEnum("EMPTY"),
    ] # [AligniovRecoStatusEnum] | the list of recoStates to query (defaults to all recoStates if not present) (optional)
    size = 7 # int | the page size (optional) if omitted the server will use the default value of 7
    sort = "DESC" # str | the time sort direction (DESC or ASC) (optional) if omitted the server will use the default value of "DESC"

    # example passing only required values which don't have defaults set
    try:
        # Retrieves a range of IOVs, sorted by time, centered around the given iovid
        api_response = api_instance.get_centered_iov_range(iovid, tagname)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->get_centered_iov_range: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves a range of IOVs, sorted by time, centered around the given iovid
        api_response = api_instance.get_centered_iov_range(iovid, tagname, reco_states=reco_states, size=size, sort=sort)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->get_centered_iov_range: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |
 **tagname** | **str**| the name of the tag (required) |
 **reco_states** | [**[AligniovRecoStatusEnum]**](AligniovRecoStatusEnum.md)| the list of recoStates to query (defaults to all recoStates if not present) | [optional]
 **size** | **int**| the page size | [optional] if omitted the server will use the default value of 7
 **sort** | **str**| the time sort direction (DESC or ASC) | [optional] if omitted the server will use the default value of "DESC"

### Return type

[**AligniovSetDto**](AligniovSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reco_summary**
> AligniovRecoSummarySetDto get_reco_summary()

Retrieves the list of align IOVs processed in the last 24h, for the active tags, and a COOL upload count

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_reco_summary_set_dto import AligniovRecoSummarySetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the since time of the interval to query (defaults to 24h ago) (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the till time of the interval to query (defaults to present time) (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves the list of align IOVs processed in the last 24h, for the active tags, and a COOL upload count
        api_response = api_instance.get_reco_summary(since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->get_reco_summary: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the since time of the interval to query (defaults to 24h ago) | [optional]
 **till** | **datetime**| the till time of the interval to query (defaults to present time) | [optional]

### Return type

[**AligniovRecoSummarySetDto**](AligniovRecoSummarySetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_iov_files**
> AlignFileSetDto list_iov_files(iovid)

Lists the uploaded files associated to this IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_file_set_dto import AlignFileSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 

    # example passing only required values which don't have defaults set
    try:
        # Lists the uploaded files associated to this IOV
        api_response = api_instance.list_iov_files(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->list_iov_files: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |

### Return type

[**AlignFileSetDto**](AlignFileSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_align_iov**
> AligniovDto update_align_iov(iovid, aligniov_payload_dto)

Updates an alignIOV, optionally locking it to a particular client ID

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.aligniov_dto import AligniovDto
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.aligniov_payload_dto import AligniovPayloadDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | the IOV id on which to perform the update
    aligniov_payload_dto = AligniovPayloadDto(
        reco_status=AligniovRecoStatusEnum("EMPTY"),
        fit_status=1,
        iov_comment="iov_comment_example",
    ) # AligniovPayloadDto | 
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID, and will lock further operations to this client ID (unless unlockClientId is set)  (optional)
    unlock_client_id = True # bool | If given, will release the recoClientId lock on this IOV  (optional)

    # example passing only required values which don't have defaults set
    try:
        # Updates an alignIOV, optionally locking it to a particular client ID
        api_response = api_instance.update_align_iov(iovid, aligniov_payload_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->update_align_iov: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Updates an alignIOV, optionally locking it to a particular client ID
        api_response = api_instance.update_align_iov(iovid, aligniov_payload_dto, reco_client_id=reco_client_id, unlock_client_id=unlock_client_id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->update_align_iov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the IOV id on which to perform the update |
 **aligniov_payload_dto** | [**AligniovPayloadDto**](AligniovPayloadDto.md)|  |
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID, and will lock further operations to this client ID (unless unlockClientId is set)  | [optional]
 **unlock_client_id** | **bool**| If given, will release the recoClientId lock on this IOV  | [optional]

### Return type

[**AligniovDto**](AligniovDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**409** | Conflict |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_iov_file**
> AlignFileOutputDto upload_iov_file(iovid, metadata, file)



Upload an alignment file to the database, associated to an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligniov_api
from openapi_client.model.align_file_dto import AlignFileDto
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_file_output_dto import AlignFileOutputDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligniov_api.AligniovApi(api_client)
    iovid = 1 # int | 
    metadata = AlignFileDto(
        file_name="file_name_example",
        user_name="user_name_example",
        file_date=dateutil_parser('1970-01-01T00:00:00.00Z'),
        mime_type="mime_type_example",
        external_ref="external_ref_example",
    ) # AlignFileDto | 
    file = open('/path/to/file', 'rb') # file_type | The file content to upload
    compression = False # bool | set this flag to true enable compression of the BLOB (optional) if omitted the server will use the default value of False
    reco_client_id = 1 # int | If given, will check that the update operation is allowed for this client ID  (optional)

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.upload_iov_file(iovid, metadata, file)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->upload_iov_file: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        api_response = api_instance.upload_iov_file(iovid, metadata, file, compression=compression, reco_client_id=reco_client_id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligniovApi->upload_iov_file: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**|  |
 **metadata** | [**AlignFileDto**](AlignFileDto.md)|  |
 **file** | **file_type**| The file content to upload |
 **compression** | **bool**| set this flag to true enable compression of the BLOB | [optional] if omitted the server will use the default value of False
 **reco_client_id** | **int**| If given, will check that the update operation is allowed for this client ID  | [optional]

### Return type

[**AlignFileOutputDto**](AlignFileOutputDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

