# openapi_client.DcsApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_average_current_for_align_iov**](DcsApi.md#get_average_current_for_align_iov) | **GET** /dcs/bfieldcurrent/aligniov/{iovid} | Retrieves the average current for an IOV
[**get_bfieldcurrent_history**](DcsApi.md#get_bfieldcurrent_history) | **GET** /dcs/bfieldcurrent | Retrieves magnet current history
[**get_tsdata_history**](DcsApi.md#get_tsdata_history) | **GET** /dcs/tsensor | Retrieves T sensor measurement history


# **get_average_current_for_align_iov**
> BfieldcurrentSetDto get_average_current_for_align_iov(iovid)

Retrieves the average current for an IOV

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import dcs_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.bfieldcurrent_set_dto import BfieldcurrentSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dcs_api.DcsApi(api_client)
    iovid = 1 # int | the align IOV ID

    # example passing only required values which don't have defaults set
    try:
        # Retrieves the average current for an IOV
        api_response = api_instance.get_average_current_for_align_iov(iovid)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling DcsApi->get_average_current_for_align_iov: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovid** | **int**| the align IOV ID |

### Return type

[**BfieldcurrentSetDto**](BfieldcurrentSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_bfieldcurrent_history**
> BfieldcurrentSetDto get_bfieldcurrent_history(since, till)

Retrieves magnet current history

Retrieves magnet current history, over parameterizable interval, averaging over 10 minutes intervals is performed 

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import dcs_api
from openapi_client.model.bfieldcurrent_set_dto import BfieldcurrentSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dcs_api.DcsApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the query
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the query

    # example passing only required values which don't have defaults set
    try:
        # Retrieves magnet current history
        api_response = api_instance.get_bfieldcurrent_history(since, till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling DcsApi->get_bfieldcurrent_history: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the start time of the query |
 **till** | **datetime**| the end time of the query |

### Return type

[**BfieldcurrentSetDto**](BfieldcurrentSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tsdata_history**
> TsensorSetDto get_tsdata_history(since, till)

Retrieves T sensor measurement history

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import dcs_api
from openapi_client.model.tsensor_set_dto import TsensorSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dcs_api.DcsApi(api_client)
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the query
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the query
    chamber_name = "chamberName_example" # str | a search pattern for the chamber name (optional) (optional)
    tsensor_pattern = "tsensorPattern_example" # str | a search pattern for the sensor name (optional) (optional)

    # example passing only required values which don't have defaults set
    try:
        # Retrieves T sensor measurement history
        api_response = api_instance.get_tsdata_history(since, till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling DcsApi->get_tsdata_history: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Retrieves T sensor measurement history
        api_response = api_instance.get_tsdata_history(since, till, chamber_name=chamber_name, tsensor_pattern=tsensor_pattern)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling DcsApi->get_tsdata_history: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **since** | **datetime**| the start time of the query |
 **till** | **datetime**| the end time of the query |
 **chamber_name** | **str**| a search pattern for the chamber name (optional) | [optional]
 **tsensor_pattern** | **str**| a search pattern for the sensor name (optional) | [optional]

### Return type

[**TsensorSetDto**](TsensorSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

