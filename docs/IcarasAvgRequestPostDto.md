# IcarasAvgRequestPostDto

A Dto to request icaras average processing.

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**since** | **int** | The since time | 
**until** | **int** | The until time | 
**olname** | **str** | The name of the optical line channel, default is all. | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


