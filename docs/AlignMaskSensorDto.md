# AlignMaskSensorDto

A masked channel in the alignment reconstruction fit, with a time interval of validity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**since_t** | **datetime** | The start time of the mask interval | 
**till_t** | **datetime** | The end time of the mask interval | 
**channel_name** | **str** | The sensor name (can be an optical line or a tsensor) | 
**iov_id** | **int** | Generated primary key | [optional] 
**alignmask_tag** | **str** | The tag name | [optional] 
**mask_comment** | **str** | Reason why it was masked | [optional] 
**error_config** | [**NumberMap**](NumberMap.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


