# AlignRecoOpticalLineOffsetDto

Optical line offset for a reconstruction tag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**element_id** | **str** |  | 
**xvalue** | **float** |  | 
**yvalue** | **float** |  | 
**scale** | **float** |  | 
**rotz** | **float** |  | 
**err_x** | **float** |  | 
**err_y** | **float** |  | 
**err_scale** | **float** |  | 
**err_rotz** | **float** |  | 
**tag** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


