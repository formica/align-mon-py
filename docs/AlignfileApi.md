# openapi_client.AlignfileApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**convert_file_content**](AlignfileApi.md#convert_file_content) | **GET** /alignfile/convert/{id} | 
[**get_file_content**](AlignfileApi.md#get_file_content) | **GET** /alignfile/content/{id} | 
[**get_file_metadata**](AlignfileApi.md#get_file_metadata) | **GET** /alignfile/{id} | 
[**upload_file**](AlignfileApi.md#upload_file) | **POST** /alignfile | 


# **convert_file_content**
> file_type convert_file_content(id, format)



Download an alignment file

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignfile_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignfile_api.AlignfileApi(api_client)
    id = 1 # int | the AlignFile ID
    format = "format_example" # str | the format to convert to (e.g. png, jpg...)

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.convert_file_content(id, format)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignfileApi->convert_file_content: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| the AlignFile ID |
 **format** | **str**| the format to convert to (e.g. png, jpg...) |

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_file_content**
> file_type get_file_content(id)



Download an alignment file

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignfile_api
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignfile_api.AlignfileApi(api_client)
    id = 1 # int | the AlignFile ID

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_file_content(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignfileApi->get_file_content: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| the AlignFile ID |

### Return type

**file_type**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream, application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_file_metadata**
> AlignFileOutputDto get_file_metadata(id)



Get the metadata of a given file

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignfile_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_file_output_dto import AlignFileOutputDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignfile_api.AlignfileApi(api_client)
    id = 1 # int | the AlignFile ID

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.get_file_metadata(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignfileApi->get_file_metadata: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| the AlignFile ID |

### Return type

[**AlignFileOutputDto**](AlignFileOutputDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_file**
> AlignFileOutputDto upload_file(metadata, file)



Upload an alignment file to the database

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignfile_api
from openapi_client.model.align_file_output_dto import AlignFileOutputDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignfile_api.AlignfileApi(api_client)
    metadata = "metadata_example" # str | Should be #/components/schemas/AlignFileDto
    file = open('/path/to/file', 'rb') # file_type | The file content to upload
    compression = False # bool | set this flag to true enable compression of the BLOB (optional) if omitted the server will use the default value of False

    # example passing only required values which don't have defaults set
    try:
        api_response = api_instance.upload_file(metadata, file)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignfileApi->upload_file: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        api_response = api_instance.upload_file(metadata, file, compression=compression)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignfileApi->upload_file: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **metadata** | **str**| Should be #/components/schemas/AlignFileDto |
 **file** | **file_type**| The file content to upload |
 **compression** | **bool**| set this flag to true enable compression of the BLOB | [optional] if omitted the server will use the default value of False

### Return type

[**AlignFileOutputDto**](AlignFileOutputDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

