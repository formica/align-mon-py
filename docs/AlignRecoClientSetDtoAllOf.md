# AlignRecoClientSetDtoAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setformat** | **str** |  | [optional]  if omitted the server will use the default value of "AlignRecoClientSetDto"
**resources** | [**[AlignRecoClientDto]**](AlignRecoClientDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


