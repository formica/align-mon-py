# AlDDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**AlIdDto**](AlIdDto.md) |  | [optional] 
**cycle_number** | **int** |  | [optional] 
**dec** | **float** |  | [optional] 
**pos5v** | **float** |  | [optional] 
**pos5c** | **float** |  | [optional] 
**pos15v** | **float** |  | [optional] 
**pos15c** | **float** |  | [optional] 
**neg15v** | **float** |  | [optional] 
**neg15c** | **float** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


