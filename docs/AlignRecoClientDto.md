# AlignRecoClientDto

Entity gathering overall information about one of the client processes performing alignment reconstruction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**host_name** | **str** |  | 
**user_name** | **str** |  | 
**client_name** | **str** |  | 
**branch** | **str** |  | 
**revision** | **str** |  | 
**client_id** | **int** |  | 
**state** | **str** |  | 
**creation_time** | **datetime** |  | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


