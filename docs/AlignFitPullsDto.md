# AlignFitPullsDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**olname** | **str** |  | 
**x_pull** | **float** |  | 
**y_pull** | **float** |  | 
**z_pull** | **float** |  | 
**iovid** | **int** |  | [optional] 
**aligniov** | [**AligniovBaseDto**](AligniovBaseDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


