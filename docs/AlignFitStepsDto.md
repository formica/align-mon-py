# AlignFitStepsDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chi_square_ov_dof** | **float** |  | 
**chi_square** | **float** |  | 
**dof** | **int** |  | 
**fit_id** | **int** |  | [optional] 
**prob** | **float** |  | [optional] 
**sequence_number** | **int** |  | [optional] 
**fit_description** | **str** |  | [optional] 
**n_iterations** | **int** |  | [optional] 
**n_max_iterations** | **int** |  | [optional] 
**exit_code** | **int** |  | [optional] 
**branch** | **str** |  | [optional] 
**revision** | **str** |  | [optional] 
**reco_client_id** | **int** |  | [optional] 
**aligniov** | [**AligniovBaseDto**](AligniovBaseDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


