# AligniovRecoSummaryDto

A reconstruction summary for an align tag

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iov_tag** | **str** |  | 
**cool_tags** | [**[AlignCoolTagDto]**](AlignCoolTagDto.md) |  | 
**iov_list** | [**[AligniovDto]**](AligniovDto.md) |  | 
**tag_description** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


