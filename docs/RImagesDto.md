# RImagesDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_id** | **int** |  | [optional] 
**image_name** | **str** |  | [optional] 
**camera_crate** | **str** |  | [optional] 
**camera_driver** | **int** |  | [optional] 
**camera_mux** | **int** |  | [optional] 
**source_crate** | **str** |  | [optional] 
**source_driver** | **int** |  | [optional] 
**source_mux** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


