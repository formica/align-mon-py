# AligntagDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iov_tag** | **str** |  | 
**tag_type** | **str** |  | 
**tag_id** | **int** |  | [optional] 
**tag_description** | **str** |  | [optional] 
**tag_insertion** | **datetime** |  | [optional] 
**prev_tag** | **int, none_type** |  | [optional] 
**tag_author** | **str** |  | [optional] 
**status** | **str** |  | [optional]  if omitted the server will use the default value of "OPEN"
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


