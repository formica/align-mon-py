# AlignCoolIovViewDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schema** | **str** |  | 
**db** | **str** |  | 
**folder** | **str** |  | 
**channel_id** | **int** |  | 
**iov_since_nanoseconds** | **int** |  | 
**iov_until_nanoseconds** | **int** |  | 
**iov_since** | **datetime** |  | 
**iov_until** | **datetime** |  | 
**pk** | **str** |  | [optional] 
**tag_id** | **int** |  | [optional] 
**tag_name** | **str** |  | [optional] 
**iov_id** | **int** |  | [optional] 
**payload_id** | **int** |  | [optional] 
**file** | **str** |  | [optional] 
**align_iov_id** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


