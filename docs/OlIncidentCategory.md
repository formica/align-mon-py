# OlIncidentCategory


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **str** |  |  must be one of ["PIT_TODO", "PIT_WONTFIX", "CALIB", "RESOLVED", ]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


