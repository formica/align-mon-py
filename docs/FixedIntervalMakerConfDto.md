# FixedIntervalMakerConfDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **str** |  | 
**save_to_db** | **bool** |  | 
**period_minutes** | **int** |  | [optional]  if omitted the server will use the default value of -1
**imtype** | **str** |  | [optional] 
**tag_description** | **str** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


