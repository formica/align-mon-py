# AligniovBaseSetDtoAllOf


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**resources** | [**[AligniovBaseDto]**](AligniovBaseDto.md) |  | [optional] 
**preceding_iov** | **int** | the ID of the IOV preceding the selection at the origin of this IOV set (optional) | [optional] 
**following_iov** | **int** | the ID of the IOV following the selection at the origin of this IOV set (optional) | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


