# openapi_client.AlignmaskApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**find_align_mask_tags**](AlignmaskApi.md#find_align_mask_tags) | **GET** /alignmask | Lists the configure AlignMask tags
[**get_align_mask_tag**](AlignmaskApi.md#get_align_mask_tag) | **GET** /alignmask/{tag} | Fetch an AlignMask tag
[**get_masked_channel**](AlignmaskApi.md#get_masked_channel) | **GET** /alignmask/channel/{id} | Fetch a masked channels by its primary key
[**get_masked_channels**](AlignmaskApi.md#get_masked_channels) | **GET** /alignmask/{tag}/channels | Fetch the masked channels for this tag
[**post_align_mask_sensor**](AlignmaskApi.md#post_align_mask_sensor) | **POST** /alignmask/{tag}/channels | Add an AlignMask sensor
[**save_align_mask_tag**](AlignmaskApi.md#save_align_mask_tag) | **PUT** /alignmask/{tag} | Add an AlignMask tag
[**update_masked_channel**](AlignmaskApi.md#update_masked_channel) | **PUT** /alignmask/channel/{id} | Updates a masked channel identified by its primary key


# **find_align_mask_tags**
> AlignMaskTagSetDto find_align_mask_tags()

Lists the configure AlignMask tags

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.align_mask_tag_set_dto import AlignMaskTagSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Lists the configure AlignMask tags
        api_response = api_instance.find_align_mask_tags()
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->find_align_mask_tags: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

[**AlignMaskTagSetDto**](AlignMaskTagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_align_mask_tag**
> AlignMaskTagDto get_align_mask_tag(tag)

Fetch an AlignMask tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_mask_tag_dto import AlignMaskTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    tag = "tag_example" # str | the tag name

    # example passing only required values which don't have defaults set
    try:
        # Fetch an AlignMask tag
        api_response = api_instance.get_align_mask_tag(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->get_align_mask_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |

### Return type

[**AlignMaskTagDto**](AlignMaskTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The requested mask tag |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_masked_channel**
> AlignMaskSensorDto get_masked_channel(id)

Fetch a masked channels by its primary key

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.align_mask_sensor_dto import AlignMaskSensorDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    id = 1 # int | the masked channel IOV ID

    # example passing only required values which don't have defaults set
    try:
        # Fetch a masked channels by its primary key
        api_response = api_instance.get_masked_channel(id)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->get_masked_channel: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| the masked channel IOV ID |

### Return type

[**AlignMaskSensorDto**](AlignMaskSensorDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The queried masked channel |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_masked_channels**
> AlignMaskSensorSetDto get_masked_channels(tag)

Fetch the masked channels for this tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.align_mask_sensor_set_dto import AlignMaskSensorSetDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    tag = "tag_example" # str | the tag name
    since = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the start time of the interval to query (optional)
    till = dateutil_parser('1970-01-01T00:00:00.00Z') # datetime | the end time of the interval to query (optional)

    # example passing only required values which don't have defaults set
    try:
        # Fetch the masked channels for this tag
        api_response = api_instance.get_masked_channels(tag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->get_masked_channels: %s\n" % e)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Fetch the masked channels for this tag
        api_response = api_instance.get_masked_channels(tag, since=since, till=till)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->get_masked_channels: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |
 **since** | **datetime**| the start time of the interval to query | [optional]
 **till** | **datetime**| the end time of the interval to query | [optional]

### Return type

[**AlignMaskSensorSetDto**](AlignMaskSensorSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The list of masked channels for tag |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_align_mask_sensor**
> AlignMaskSensorDto post_align_mask_sensor(tag, align_mask_sensor_dto)

Add an AlignMask sensor

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.align_mask_sensor_dto import AlignMaskSensorDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    tag = "tag_example" # str | the tag name
    align_mask_sensor_dto = AlignMaskSensorDto(
        iov_id=1,
        alignmask_tag="alignmask_tag_example",
        since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
        till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
        channel_name="channel_name_example",
        mask_comment="mask_comment_example",
        error_config=NumberMap(
            key=3.14,
        ),
    ) # AlignMaskSensorDto | 

    # example passing only required values which don't have defaults set
    try:
        # Add an AlignMask sensor
        api_response = api_instance.post_align_mask_sensor(tag, align_mask_sensor_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->post_align_mask_sensor: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |
 **align_mask_sensor_dto** | [**AlignMaskSensorDto**](AlignMaskSensorDto.md)|  |

### Return type

[**AlignMaskSensorDto**](AlignMaskSensorDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The masked channel was successfully saved |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **save_align_mask_tag**
> AlignMaskTagDto save_align_mask_tag(tag, align_mask_tag_dto)

Add an AlignMask tag

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.align_mask_tag_dto import AlignMaskTagDto
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    tag = "tag_example" # str | the tag name
    align_mask_tag_dto = AlignMaskTagDto(
        tag_id=1,
        mask_tag="mask_tag_example",
        tag_comment="tag_comment_example",
    ) # AlignMaskTagDto | 

    # example passing only required values which don't have defaults set
    try:
        # Add an AlignMask tag
        api_response = api_instance.save_align_mask_tag(tag, align_mask_tag_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->save_align_mask_tag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tag** | **str**| the tag name |
 **align_mask_tag_dto** | [**AlignMaskTagDto**](AlignMaskTagDto.md)|  |

### Return type

[**AlignMaskTagDto**](AlignMaskTagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The mask tag was successfully saved |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_masked_channel**
> AlignMaskSensorDto update_masked_channel(id, align_mask_sensor_dto)

Updates a masked channel identified by its primary key

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import alignmask_api
from openapi_client.model.align_mask_sensor_dto import AlignMaskSensorDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = alignmask_api.AlignmaskApi(api_client)
    id = 1 # int | the masked channel IOV ID
    align_mask_sensor_dto = AlignMaskSensorDto(
        iov_id=1,
        alignmask_tag="alignmask_tag_example",
        since_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
        till_t=dateutil_parser('1970-01-01T00:00:00.00Z'),
        channel_name="channel_name_example",
        mask_comment="mask_comment_example",
        error_config=NumberMap(
            key=3.14,
        ),
    ) # AlignMaskSensorDto | 

    # example passing only required values which don't have defaults set
    try:
        # Updates a masked channel identified by its primary key
        api_response = api_instance.update_masked_channel(id, align_mask_sensor_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AlignmaskApi->update_masked_channel: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| the masked channel IOV ID |
 **align_mask_sensor_dto** | [**AlignMaskSensorDto**](AlignMaskSensorDto.md)|  |

### Return type

[**AlignMaskSensorDto**](AlignMaskSensorDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The modified masked channel |  -  |
**404** | Not found |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

