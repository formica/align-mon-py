# openapi_client.AligntagApi

All URIs are relative to *https://atlas-muon-align-mon.web.cern.ch/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_aligntag**](AligntagApi.md#create_aligntag) | **POST** /aligntags | Create a new aligntag to database.
[**find_aligntags**](AligntagApi.md#find_aligntags) | **GET** /aligntags | Finds an AligntagSetDto by tag name
[**find_aligntags_with_summary**](AligntagApi.md#find_aligntags_with_summary) | **GET** /aligntags/withsummary | Finds an AligntagSetDto by tag name
[**get_aligntag**](AligntagApi.md#get_aligntag) | **GET** /aligntags/{iovtag} | Get an aligntag existing in the database.
[**update_aligntag**](AligntagApi.md#update_aligntag) | **PUT** /aligntags/{iovtag} | Update an aligntag existing in the database.


# **create_aligntag**
> AligntagDto create_aligntag()

Create a new aligntag to database.

This method allows to insert Aligntag.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntag_api
from openapi_client.model.aligntag_dto import AligntagDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntag_api.AligntagApi(api_client)
    aligntag_dto = AligntagDto(
        tag_id=1,
        iov_tag="iov_tag_example",
        tag_description="tag_description_example",
        tag_insertion=dateutil_parser('1970-01-01T00:00:00.00Z'),
        prev_tag=1,
        tag_author="tag_author_example",
        tag_type="tag_type_example",
        status="OPEN",
    ) # AligntagDto |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Create a new aligntag to database.
        api_response = api_instance.create_aligntag(aligntag_dto=aligntag_dto)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntagApi->create_aligntag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aligntag_dto** | [**AligntagDto**](AligntagDto.md)|  | [optional]

### Return type

[**AligntagDto**](AligntagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | successful operation |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_aligntags**
> AligntagSetDto find_aligntags()

Finds an AligntagSetDto by tag name

Finds an AligntagSetDto by tag name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntag_api
from openapi_client.model.aligntag_set_dto import AligntagSetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntag_api.AligntagApi(api_client)
    type = "PRODUCTION" # str | Optional parameter to restrict query to type (PRODUCTION, TESTS or MONTECARLO) (optional)
    tag_pattern = "tagPattern_example" # str | Optional parameter to filter on tag name (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an AligntagSetDto by tag name
        api_response = api_instance.find_aligntags(type=type, tag_pattern=tag_pattern)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntagApi->find_aligntags: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| Optional parameter to restrict query to type (PRODUCTION, TESTS or MONTECARLO) | [optional]
 **tag_pattern** | **str**| Optional parameter to filter on tag name | [optional]

### Return type

[**AligntagSetDto**](AligntagSetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_aligntags_with_summary**
> AligntagWithSummarySetDto find_aligntags_with_summary()

Finds an AligntagSetDto by tag name

Finds an AligntagSetDto by tag name

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntag_api
from openapi_client.model.aligntag_with_summary_set_dto import AligntagWithSummarySetDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntag_api.AligntagApi(api_client)
    type = "PRODUCTION" # str | Optional parameter to restrict query to type (PRODUCTION, TESTS or MONTECARLO) (optional)
    tag_pattern = "tagPattern_example" # str | Optional parameter to filter on tag name (optional)
    active = True # bool | Optional parameter to filter on flag activatedOnline (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Finds an AligntagSetDto by tag name
        api_response = api_instance.find_aligntags_with_summary(type=type, tag_pattern=tag_pattern, active=active)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntagApi->find_aligntags_with_summary: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | **str**| Optional parameter to restrict query to type (PRODUCTION, TESTS or MONTECARLO) | [optional]
 **tag_pattern** | **str**| Optional parameter to filter on tag name | [optional]
 **active** | **bool**| Optional parameter to filter on flag activatedOnline | [optional]

### Return type

[**AligntagWithSummarySetDto**](AligntagWithSummarySetDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, application/xml


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_aligntag**
> AligntagDto get_aligntag(iovtag)

Get an aligntag existing in the database.

This method allows to get an Aligntag designed by the path argument {name}.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntag_api
from openapi_client.model.aligntag_dto import AligntagDto
from openapi_client.model.http_response import HTTPResponse
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntag_api.AligntagApi(api_client)
    iovtag = "iovtag_example" # str | The Aligntag name

    # example passing only required values which don't have defaults set
    try:
        # Get an aligntag existing in the database.
        api_response = api_instance.get_aligntag(iovtag)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntagApi->get_aligntag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovtag** | **str**| The Aligntag name |

### Return type

[**AligntagDto**](AligntagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_aligntag**
> AligntagDto update_aligntag(iovtag, generic_map)

Update an aligntag existing in the database.

This method allows to update Aligntag designed by the path argument {name}.

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import openapi_client
from openapi_client.api import aligntag_api
from openapi_client.model.aligntag_dto import AligntagDto
from openapi_client.model.http_response import HTTPResponse
from openapi_client.model.generic_map import GenericMap
from pprint import pprint
# Defining the host is optional and defaults to https://atlas-muon-align-mon.web.cern.ch/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://atlas-muon-align-mon.web.cern.ch/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = aligntag_api.AligntagApi(api_client)
    iovtag = "iovtag_example" # str | The Aligntag name
    generic_map = GenericMap(
        key="key_example",
    ) # GenericMap | A JSON object containing generic map used for updates

    # example passing only required values which don't have defaults set
    try:
        # Update an aligntag existing in the database.
        api_response = api_instance.update_aligntag(iovtag, generic_map)
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AligntagApi->update_aligntag: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iovtag** | **str**| The Aligntag name |
 **generic_map** | [**GenericMap**](GenericMap.md)| A JSON object containing generic map used for updates |

### Return type

[**AligntagDto**](AligntagDto.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | successful operation |  -  |
**404** | Not found |  -  |
**0** | Generic error response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

