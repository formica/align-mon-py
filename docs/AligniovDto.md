# AligniovDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**iov_id** | **int** |  | 
**since_t** | **datetime** |  | 
**till_t** | **datetime** |  | 
**reco_status** | [**AligniovRecoStatusEnum**](AligniovRecoStatusEnum.md) |  | 
**fit_status** | **int** |  | 
**tag_id** | **str** |  | [optional] 
**iov_comment** | **str** |  | [optional] 
**reco_client_id** | **int** |  | [optional] 
**last_modification_time** | **datetime** |  | [optional] 
**alignfitsteps** | [**[AlignFitStepsDto]**](AlignFitStepsDto.md) |  | [optional] 
**alignfitbadsensors** | [**[AlignFitBadSensorsDto]**](AlignFitBadSensorsDto.md) |  | [optional] 
**cool_iov_count** | [**IntegerMap**](IntegerMap.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


