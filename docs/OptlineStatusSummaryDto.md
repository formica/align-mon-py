# OptlineStatusSummaryDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**n_active_opticallines** | **int** |  | [optional] 
**n_bad_active_opticallines** | **int** |  | [optional] 
**n_active_chambers** | **int** |  | [optional] 
**n_positionned_active_chambers** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


