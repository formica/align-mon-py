# AlignCoolTagDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cool_tag** | **str** |  | [optional] 
**align_tag** | **str** |  | [optional] 
**tag_comment** | **str** |  | [optional] 
**coollock** | **str** |  | [optional] 
**do_upload_on_reco** | **bool** |  | [optional] 
**cherrypy_config** | **str** |  | [optional] 
**tag_type** | **str** |  | [optional] 
**cool_config** | [**CoolConfigDto**](CoolConfigDto.md) |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


