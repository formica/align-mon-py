# OlIncidentPostDto


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ol_name** | **str** |  | 
**incident_category** | [**OlIncidentCategory**](OlIncidentCategory.md) |  | 
**comment_text** | **str** |  | 
**incident_date** | **datetime** |  | [optional] 
**test_code** | **int** |  | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


